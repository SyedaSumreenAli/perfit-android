import {createStore, combineReducers} from 'redux'
import gameReducer from './reducers/gameReducer'
import personReducer from './reducers/personRedeucer'


const AllReducer = combineReducers({person: personReducer, game:gameReducer})
const InitialStates = {
    game:{
        name:'FootBall'
    },
    person:{
        name:'Sumreen'
    }
}
const store = createStore(AllReducer,InitialStates)

export default store