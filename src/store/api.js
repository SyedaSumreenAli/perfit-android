export const BASE_URL = `http://system.perfitapp.com/api/`

export const LOGIN_API = `${BASE_URL}/login`
export const REGISTER_API = `${BASE_URL}/register`
export const CHECK_API = `${BASE_URL}/check`