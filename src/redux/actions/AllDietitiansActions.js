import { ALL_DIETITIANS_REQUEST, ALL_DIETITIANS_FAILURE, ALL_DIETITIANS_SUCCESS } from './types'
import Axios from 'axios';

export const fetchAllDietitiansRequest = () => ({
    type: ALL_DIETITIANS_REQUEST
});

export const fetchAllDietitiansSuccess = json => ({
    type: ALL_DIETITIANS_SUCCESS,
    payload: json
});

export const fetchAllDietitiansFailure = error => ({
    type: ALL_DIETITIANS_FAILURE,
    payload: error
});


export const fetchAllDietitians = () => {

    return async dispatch => {


        try {
            let response = await fetch('http://system.perfitapp.com/api/dietitians/all')
            let json = await response.json()

            // console.log("sasaas")
            //  console.log(json.dietitians)

            dispatch(fetchAllDietitiansSuccess(json.dietitians));
        }
        catch{
            dispatch(fetchAllDietitiansFailure(json.error));
        }
    }
}