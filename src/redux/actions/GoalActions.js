import {SET_GOALS} from './types'

import axios from 'axios'




export const setGoal=(goals,navigate,loading)=>{


    return(dispatch)=>{

        let user_id=goals.user_id;
        let goal=goals.goal;
        let workout=goals.workout;
        let exercise_level=goals.option
        
        axios
        .post(`http://system.perfitapp.com/api/user/${user_id}/profile/goals?goals=${goal}&workout=${workout}&exercise_level=${exercise_level}`)
        .then((res)=>{

            console.log(res.data)

            if(res.data){

                dispatch({
                    type:SET_GOALS,
                    payload:res.data.profile
                })

                loading()
                
                navigate()
            }
         

           
        }
        ).catch((error)=>console.log(error))
    

    }


  





} 