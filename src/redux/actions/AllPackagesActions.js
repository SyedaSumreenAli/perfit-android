import {ALL_PACKAGES_REQUEST , ALL_PACKAGES_SUCCESS, ALL_PACKAGES_FAILURE } from './types'
import {BASE_URI, GET_ALL_PACKAGES_URI } from '../api'
export const getALLPackagesRequest = () => ({
    type: ALL_PACKAGES_REQUEST
});

export const getALLPackagesSuccess = (json) => ({
    type: ALL_PACKAGES_SUCCESS,
    payload: json 
});

export const getALLPackagesFailure = (error) => ({
    type: ALL_PACKAGES_FAILURE,
    payload: error
});

export const getAllPackages = () =>{
    return async dispatch => {
        dispatch(getALLPackagesRequest());
        try{
             let response = await fetch(BASE_URI + GET_ALL_PACKAGES_URI)
            let json = await  response.json()
            dispatch(getALLPackagesSuccess(json))
        }catch{
            dispatch(getALLPackagesFailure(json))
        }
    }
}