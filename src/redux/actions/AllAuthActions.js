import { SIGN_UP } from './types'

import axios from 'axios'


const _storeData = async (id) => {
    try {
        await AsyncStorage.setItem('user_id', id);
    } catch (error) {
        // Error saving data
    }
};


export const signUp = (user,toggleModel,loading) => {
 
//save nhi kia tha

    return (dispatch) => {

        // console.log("user------>")
        //console.log(user)
       
        axios.post('http://system.perfitapp.com/api/register', {

            name: user.name,
            email: user.email,
            username: user.username,
            password: user.password,


        }).then(res => res.data)
        .then((result)=>{
            
            console.log(result)    
        
            

            if(result.error){

                loading()
            }

            if(result.user){


                      console.log(".......")


                _storeData(result.user.id)



                dispatch({

                    type: SIGN_UP,
                    payload: result.user

                })

                loading()
                 toggleModel()
                

            }
            
            
        })
        .catch(err => console.log(err))

              


    }


}