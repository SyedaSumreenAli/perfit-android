import {ALL_EXERCISES_REQUEST , ALL_EXERCISES_SUCCESS, ALL_EXERCISES_FAILURE } from './types'
import {BASE_URI, GET_ALL_EXERCISES_URI } from '../api'
export const getAllExercisesRequest = () => ({
    type: ALL_EXERCISES_REQUEST
});

export const getAllExercisesSuccess = (json) => ({
    type: ALL_EXERCISES_SUCCESS,
    payload: json ,

});

export const getAllExercisesFailure = (error) => ({
    type: ALL_EXERCISES_FAILURE,
    payload: error
});

export const getAllExercises = () =>{
    return async dispatch => {
        dispatch(getAllExercisesRequest());
        try{
             let response = await fetch(BASE_URI+GET_ALL_EXERCISES_URI)
             console.log(BASE_URI+GET_ALL_EXERCISES_URI)
             console.log(response)
            let json = await  response.json()
            console.log(json)
            // debugger;
          console.log(  dispatch(getAllExercisesSuccess(json)))

        }catch{
            console.log(dispatch(getAllExercisesFailure(json)))
        }
    }
}