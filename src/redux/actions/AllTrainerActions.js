import {ALL_TRAINER_REQUEST, ALL_TRAINER_SUCCESS, ALL_TRAINER_FAILURE} from './types'
import Axios from 'axios';

export const fetchAllTrainerRequest = () => ({
        type: ALL_TRAINER_REQUEST
});

export const fetchAllTrainerSuccess = json => ({
        type: ALL_TRAINER_SUCCESS,
        payload: json
});

export const fetchALlTrainerFailure = error => ({
    type: ALL_TRAINER_FAILURE,
    payload: error
});

export  const fetchAllTrainer = () => {
    return async dispatch => {
        dispatch(fetchAllTrainerRequest());
        try{
            let response = await fetch('http://system.perfitapp.com/api/trainers/all')
            let json = await response.json()
            dispatch(fetchAllTrainerSuccess(json));
        }
        catch{
            dispatch(fetchAllTrainerFailure(json.error));
        }
    }
}