import { POST_DETAILS_REQUEST, POST_DETAILS_SUCCESS, POST_DETAILS_FAILURE } from './types'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage';

const postDetailSuccess = details => ({
  type: POST_DETAILS_SUCCESS,
  payload: {
    ...details
  }
});

const postDetailRequest = () => ({
  type: POST_DETAILS_REQUEST
});

const postDetailFailure = error => ({
  type: POST_DETAILS_FAILURE,
  payload: {
    error
  }
});

export const postDetail = (gender, weight, height, date_of_birth,navigate) => {

  return async (dispatch, getState) => {

    let user_id = await AsyncStorage.getItem("user_id")

    console.log("in action " + user_id)

    dispatch(postDetailRequest());

    axios
  .post(`http://system.perfitapp.com/api/user/${user_id}/profile/details?gender=${gender}&weight=${weight}&height=${height}&date_of_birth=${date_of_birth}`)
      .then(res => {

        console.log(res.data)
        dispatch(postDetailSuccess(res.data));
        navigate()
      })
      
      .catch(err => {
        dispatch(postDetailFailure(err.message));
      });
   // console.log('current state:', getState());
  };
};  