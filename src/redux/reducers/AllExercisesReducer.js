import {ALL_EXERCISES_REQUEST , ALL_EXERCISES_SUCCESS, ALL_EXERCISES_FAILURE } from '../actions/types'

const initialState ={
    loading: true,
    errorMessage: '',
    workouts: []
}

const AllExercisesReducer = ( state=initialState, { type, payload }) => {
    console.log(payload)
    switch(type){
        case ALL_EXERCISES_REQUEST:
            return{
                ...state,
                loading: true
            }
        case ALL_EXERCISES_SUCCESS:
            console.log(payload)
            return{
                ...state,
               workouts: payload,
                loading: false,
               
            }
        case ALL_EXERCISES_FAILURE:
            return{
                ...state,
                loading: false,
                errorMessage: payload
            }
        default:
            return state;
    }
      
}

export default AllExercisesReducer