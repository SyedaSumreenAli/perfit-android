import {ALL_DIETITIANS_REQUEST, ALL_DIETITIANS_SUCCESS, ALL_DIETITIANS_FAILURE} from '../actions/types'

const initialState = {
    loading : true,
    errorMessage: '',
    dietitions: []
}

const AllDietitiansReducer = (state= initialState, {type, payload}) =>{
    switch(type){
        case ALL_DIETITIANS_REQUEST:
            return{
                ...state,
                loading: true
            }
        case ALL_DIETITIANS_FAILURE:
            return{
                ...state,
                errorMessage: payload,
                loading: false
            }
        case ALL_DIETITIANS_SUCCESS:
            return{
                ...state,
                dietitions:payload,
                loading:false
               
            }
        default:
            return state;
    }
}

export default AllDietitiansReducer;