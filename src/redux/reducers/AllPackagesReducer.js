import  {ALL_PACKAGES_REQUEST, ALL_PACKAGES_SUCCESS, ALL_PACKAGES_FAILURE} from '../actions/types'

const initialState ={
    loading: false,
    errorMessage: '',
    packages: []
}

const AllPackagesReducer = ( state=initialState, { type, payload }) => {
    switch(type){
        case ALL_PACKAGES_REQUEST:
            return{
                ...state,
                loading: true
            }
        case ALL_PACKAGES_SUCCESS:
            return{
                ...state,
                loading: false,
                packages: payload
            }
        case ALL_PACKAGES_FAILURE:
            return{
                ...state,
                loading: false,
                errorMessage: payload
            }
        default:
            return state;
    }
      
}

export default AllPackagesReducer