import { combineReducers } from 'redux'
import AllTrainerReducer from './AllTrainerReducer'
import AllPackagesReducer from './AllPackagesReducer'
import AllExercisesReducer from './AllExercisesReducer'
import DetailReducer from './DetailsReducer'
import AuthReducer from './AuthReducer'
import GoalReducer from './GoalReducer'
import AllDietitiansReducer from './AllDietitiansReducer'

const AllReducers = combineReducers({
    AllTrainerReducer,
    AllPackagesReducer,
    AllExercisesReducer,
    Diet: AllDietitiansReducer,
    DetailReducer,
    AuthReducer,
    GoalReducer
})

export default AllReducers;