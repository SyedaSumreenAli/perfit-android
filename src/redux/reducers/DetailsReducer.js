import { POST_DETAILS_REQUEST, POST_DETAILS_SUCCESS, POST_DETAILS_FAILURE} from '../actions/types';
  
  const initialState = {
    loading: false,
    details: [],
    error: null
  };
  
  export default function todosReducer(state = initialState, {type, payload }) {
    switch (type) {
      case POST_DETAILS_REQUEST:
        return {
          ...state,
          loading: true
        };
      case POST_DETAILS_SUCCESS:
        return {
          ...state,
          loading: false,
          error: null,
          details: [...state.details, payload]
        };
      case POST_DETAILS_FAILURE:
        return {
          ...state,
          loading: false,
          error: payload.error
        };
      default:
        return state;
    }
  }