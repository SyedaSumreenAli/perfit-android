import {ALL_TRAINER_REQUEST, ALL_TRAINER_SUCCESS, ALL_TRAINER_FAILURE} from '../actions/types'

const initialState = {
    loading : true,
    errorMessage: '',
    trainers: []
}

const AllTrainerReducer = (state= initialState, {type, payload}) =>{
    switch(type){
        case ALL_TRAINER_REQUEST:
            return{
                ...state,
                loading: true
            }
        case ALL_TRAINER_FAILURE:
            return{
                ...state,
                errorMessage: payload,
                loading: false
            }
        case ALL_TRAINER_SUCCESS:
            return{
                ...state,
                trainers : payload,
                loading:false
               
            }
        default:
            return state;
    }
}

export default AllTrainerReducer;