/************* BASE_URI *************/
export const BASE_URI = 'http://system.perfitapp.com/'


/************* GET ALL PACKAGES *************/
export const GET_ALL_PACKAGES_URI = 'api/packages/all'


/************* GET ALL EXERCISES *************/
export const GET_ALL_EXERCISES_URI = 'api/workouts/all'