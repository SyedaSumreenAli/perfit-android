import { StyleSheet } from "react-native";
import { width, height } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    flex: 2,
    // backgroundColor:"red",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  logoContainer: {
    flex: 1,
    paddingTop: height(10),
    paddingBottom: height(8),
    zIndex: 2,
    // backgroundColor:"pink",
  },

  logo: {
    width: 130,
    height: 200,
  },
  textContainer: {
    // backgroundColor:"yellow",
    flex: 3,
  },
  linksContainer: {
    // backgroundColor:"green",
    flex: 2,
  },
  textCenter: {
    textAlign: "center",
  },
  h1: {
    fontSize: 26.52,
    paddingTop: height(14),
    paddingBottom: height(4),
    color: "#222222",
    fontFamily: "Montserrat-Regular",
    fontWeight: "300",
  },
  subText: {
    fontFamily: "Montserrat-Light",
    color: "#928E8E",
    fontSize: 14.52,
    fontWeight: "300",
    paddingHorizontal: width(8),
  },
  bgImage: {
    position: "absolute",
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 45,
    paddingRight: 45,
    opacity: 0.9,
    borderRadius: 25,
  },
  buttonText: {
    fontSize: 19.23,
    fontFamily: "Montserrat-Light",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
  },
  trainerScreenLinkContainer: {
    marginVertical: height(6),
  },

  trainerScreenLink: {
    marginVertical: height(8),
    fontFamily: "Montserrat-Regular",
    color: "#BDBDBD",
  },
  LinkContainer: {
    flex: 1,
    // backgroundColor:'orange',
    alignItems: "center",
  },
  SignupLinkContainer: {
    marginBottom: height(2),
  },
  flexRow: {
    flexDirection: "row",
  },
  SignupText: {
    color: "#888888",
    fontFamily: "Montserrat-Regular",
    fontSize: 15.58,
  },
  SignupLink: {
    color: "#888888",
    fontFamily: "Montserrat-Bold",
    fontSize: 15.58,
  },
});

export default style;
