import React from "react";
import { View, Text, Image, AsyncStorage } from "react-native";
import {width, height} from 'react-native-dimension'
import axios from 'axios';

class Splash extends React.Component {
  // const Splash = ({ navigation }) => {
  // }
    timeout = 0;
  _handleNextPage = async(user_id)=>{
    console.log(user_id)
    try{
      const response = await axios.get(`http://system.perfitapp.com/api/user/${user_id}/get/profile`);
      console.log('--------------- handle next page api try block -------------')
      console.log(response.data)
      if(response.data.message == 'Profile Not Found'){
        this.props.navigation.navigate('YourDetails')
      }
      if(response.data.success === true){
        this.props.navigation.navigate('Tab')
      }

    }catch(error){
      console.log('--------------- handle next page api catch block -------------')
      console.log(error)

    }
  }
  componentDidMount = () => {
    const { navigation } = this.props;
    
    this.timeout = setTimeout(async() => {
    
      const user_id = await AsyncStorage.getItem("user_id")
     
      if(user_id !== null){
       
        console.log('############ USER EXIST ##########')
        console.log(user_id)
        this._handleNextPage(user_id)
       
      }else{
       
        console.log('############ USER NOT EXIST ##########')
        console.log(user_id)
        this.props.navigation.replace("Login"); 
     
      }
    
    },2500)
}

componentWillUnmount(){
  
  console.log(`Unmounting.. clearing interval ${this.timeout}`);

  clearTimeout(this.timeout)

}

render(){
  return (
    <View style = {{flex:1}}>
        <Image source={require('../../assets/images/data/Perfit.gif')}
         style={{
            flex:1,
            justifyContent:'center',
            alignSelf:'center',   
            width:width(100)
        }} />
      </View>

       );
}
}
export default Splash;
