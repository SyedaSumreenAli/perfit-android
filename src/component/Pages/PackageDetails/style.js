import { StyleSheet } from "react-native";
import { width, height, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
     backgroundColor:"white",
    // height:height(100),
    // backgroundColor:"white",
    flex: 1,
  },
  headerContainer1: {
    position: "absolute",
    zIndex: 4,
  },
  flexRow: { flexDirection: "row" },
  iconContainer: {
    flex: 6,
      // backgroundColor:"orange",
    height: height(14),
    justifyContent: "center",
  },
  icon: {
    // backgroundColor:"green",
    // width: width(40),
    marginLeft:-width(12),
    alignSelf: "center",
    fontSize: 35,
  },
  flex4: {
    flex: 7,
    alignItems: "flex-start",
    justifyContent: "center",
    //  backgroundColor:"green",
  },
  headerText: {
    // backgroundColor:"red",
    flexDirection: "column",
    color: "#424242",
    fontSize: 20,
    fontFamily: "Montserrat-SemiBold",
  },
  coverImg: {
    position: "absolute",
    height: height(45),
    width:width(100),
    flex: 2,
  },
  mainContainer: {
    //  backgroundColor:"white",
    flex: 1,
    justifyContent: "flex-end",
  },
  ImgContainer: {
    backgroundColor: "pink",
    alignItems: "center",
  },
  main: {
    // backgroundColor:"skyblue",
    backgroundColor: "white",
    //  backgroundColor:"transparent",
    paddingTop: height(12),
    borderWidth:1,
    borderColor:'#d8d8d8',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingBottom: height(8),
    width: width(85),
    alignSelf: "center",
    alignItems: "center",
    zIndex: 4,
  },
  h2: {
    fontSize: totalSize(3.1),
    fontFamily: "Montserrat-Bold",
    color: "#000000",
    marginBottom:height(2)
  },
  flexRow: {
    flexDirection: "row",
  },
  starIconContainer: {
    flexDirection: "row",
  },
  starIcon: {
    fontSize: totalSize(1.3),
    color: "#FDC900",
    paddingLeft: width(1),
    paddingTop: width(1),
  },
  reviewText: {
    color: "#323131",
    fontSize: totalSize(1.8),
    fontFamily: "Montserrat-Medium",
    paddingLeft: width(2),
  },
  contactButton: {
    backgroundColor: "#FCB344",
    borderRadius: 20,
    height: height(4),
    paddingHorizontal: width(4),
    justifyContent: "center",
    marginTop: height(1),
  },
  contactButtonText: {
    color: "#ffffff",
  },
  lgLightButton: {
    backgroundColor: "#e6e6e6",
    width: width(75),
    justifyContent: "center",
    alignItems:"center",
    height:height(7),
    marginVertical: height(4),
  },
  h3: {
    fontSize: totalSize(2.6),
    fontFamily: "Montserrat-Bold",
  },
  descriptionText: {
    fontSize: totalSize(2),
    // textAlign: "justify",
    color: "#585858",
    paddingHorizontal: width(3),
    fontFamily: "Montserrat-Regular",
  },
  buttomContainer: {
    width: width(100),
    // backgroundColor:"orange",
    height: height(10),
    // justifyContent:"center",
    // alignItems:"center"
  },
  amountBox: {
    backgroundColor: "#F7C400",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  payBox: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EBA70F",
    height: height(10),
  },
  h2White: {
    fontFamily: "Montserrat-Bold",
    color: "#ffffff",
    fontSize: totalSize(3),
  },
  avatarBorder: {
    borderWidth: 4,
    borderColor: "#D8D8D8",
    width: width(24),
    height: width(26),
    borderRadius: 50,
    // borderBottomLeftColor:"transparent",
    borderBottomColor: "transparent",
    // backgroundColor:"#D8D8D8",
    // zIndex:4,
    overflow: "hidden",
    alignItems: "stretch",
    justifyContent: "center",
    alignSelf: "center",
    position: "absolute",
  },
  avatarContainer: {
    position: "absolute",
    justifyContent: "flex-end",
    alignItems: "center",
    alignSelf: "center",
    height: height(32),
    width: width(100),
    //  backgroundColor:"yellow",
    //  opacity:0.6,
    zIndex: 5,
  },
  avatar: {
    height: width(29),
    width:width(25),
    alignSelf: "center",
    zIndex: 6,
  },
  categoryText: {
    backgroundColor: "#000000",
    color: "white",
    borderRadius: 20,
    zIndex: 6,
    paddingHorizontal: width(2),
    fontFamily: "Montserrat-Medium",
    minWidth:width(24),
    textAlign:"center"
  },

  space: {
    // flex:1,
    // backgroundColor:"green",
    width: width(100),
    height: height(25),
    justifyContent: "flex-end",
    alignItems: "center",
    // justifyContent:"center"
  },
  reviewButton: {
    paddingTop: -10,
    // backgroundColor:"red",
    height: null,
  },
});

export default style;
