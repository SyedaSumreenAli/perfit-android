import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Icon, Button } from "native-base";
// import Image from 'react-native-scalable-image'
import style from "./style";
import { width, height } from "react-native-dimension";
import { ScrollView } from "react-native-gesture-handler";
import { withNavigation } from "react-navigation";
import axios from 'axios'
import {BASE_URI} from '../../../redux/api'

class PackageDetails extends Component {
  static navigationOptions = {
    headerShown: false,
  };
  state = {   
    packageName:'',
    description:'',
    price:0,
    duration:0.0,
    review:20,
    image:null,
    trainerName:'John Doe',
    trainerImage:null,
    review:20,
    category:'weight lose'
}   

componentDidMount= async()=>{
   const {user, packageDetails, userType} = this.props.navigation.state.params
    console.log('============== Inside Package Detail =================')
    console.log(user)
    console.log(packageDetails)
    console.log(user.user.username)
  

    this.setState({
      packageName: packageDetails.name,
      price:       packageDetails.price,
      description: packageDetails.description,
      duration:    packageDetails.duration,
      image:       packageDetails.image,
      userName:    user.user.username,
      userImage:   user.user.photo,
      category:    user.user.tags[0].name
    })
    
}
  render() {
    const {userName, userImage, packageName, description, price, duration, review,image,category} = this.state
    // console.log(image)
    return (
      <ScrollView>
        <View style={style.container}>
          <View style={[style.flexRow, style.headerContainer1]}>
            <View style={style.iconContainer}>
              <TouchableOpacity onPress={()=>this.props.navigation.replace('SelectionMain').bind(this)}>
                <Image style={style.icon} source={require('../../../assets/images/icons/arrow.png')}/>
              </TouchableOpacity>
            </View>
            <View style={style.flex4}>
              <View style={style.headerContainer}>
                <Text style={style.headerText}>PT</Text>
              </View>
            </View>
          </View>

          <View>
            <Image
      
              resizeMethod='resize'
              resizeMode='cover'
              style={style.coverImg}
              source={image !==null ?{uri:"http://system.perfitapp.com/images/packages/"+image }
              : require("../../../assets/images/data/pt1.png")}
            />
          </View>
   
          <View style={style.avatarContainer}>
            <View style={style.avatarBorder}>
              <Image
            
              
                source={userImage ? {uri: "http://system.perfitapp.com/images/profile_images/"+userImage}
                : {uri:"https://image.flaticon.com/icons/png/512/21/21294.png"}}
                style={style.avatar}
                resizeMethod="resize"
                resizeMode="cover"
              />
            </View>
            <Text style={style.categoryText}>{category}</Text>
          </View>

          <View style={style.mainContainer}>
            <View style={style.space} />

            <View style={style.main}>
              <Text style={style.h2}>{userName}</Text>
              <View style={style.flexRow}>
                <View style={style.starIconContainer}>
                  <Icon style={style.starIcon} name="star" type="AntDesign" />
                  <Icon style={style.starIcon} name="star" type="AntDesign" />
                  <Icon style={style.starIcon} name="star" type="AntDesign" />
                  <Icon style={style.starIcon} name="star" type="AntDesign" />
                  <Icon style={style.starIcon} name="star" type="AntDesign" />
                </View>
                <View>
                  <Button
                    transparent
                    style={style.reviewButton}
                    onPress={() => this.props.navigation.navigate("Review")}
                  >
                    <Text style={style.reviewText}>Review({review})</Text>
                  </Button>
                </View>
              </View>
              <Button style={style.contactButton}>
                <Text style={style.contactButtonText}> Contact me</Text>
              </Button>
              <View>
                <TouchableOpacity style={style.lgLightButton}>
                  <Text style={style.h3}>{duration} Month</Text>
                </TouchableOpacity>
              </View>
              <View style={style.descriptionContainer}>
                <Text style={style.descriptionText}>
                 {description}
                </Text>
              </View>
            </View>
          </View>
          <View style={[style.flexRow, style.buttomContainer]}>
            <View style={style.amountBox}>
              <Text style={style.h2White}>${price}/Month</Text>
            </View>
            <Button style={style.payBox}>
              <Text style={style.h2White}>PAY</Text>
            </Button>
          </View>
        </View>
     
        </ScrollView>
    );
  }
}

export default withNavigation(PackageDetails);
