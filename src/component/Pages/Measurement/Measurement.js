import React, { Component } from 'react'
import {View, Text} from 'react-native'
import {Content, Form,Picker, Item, Input,  Button} from 'native-base'
import Image from 'react-native-scalable-image'
import {width, height, totalSize} from 'react-native-dimension'
import style from './style'
class Measurement extends Component{
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //       selected: "key1"
    //     };
    //   }
    //   onValueChange(value) {
    //     this.setState({
    //       selected: value
    //     });
    //   }

    render(){
        return(
            <View style={style.container}>
                <Button style={style.buttonMedium}>
                    <Text style={style.buttonText}>update weight</Text>
                </Button>
                <View style={style.h1Container}>
                    <Text style={style.h1}>Body Measurements</Text>
                </View>
                <View style={style.imgContainer}>
                    <Image source={require('../../../assets/images/measurement/body.png')}
                        height={height(64)}
                    />
                </View>
                <View style={style.form}>
                <Content>
                        <Form>

                         <View style={style.flexRow}>
                             <View style={style.headContainer}>
                                <View style={style.headIndicator}>
                                    <Text style={style.line}> - - - - - - - -</Text>
                                </View>
                             </View>
                            <View style={style.r1}>
                            
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="head" />
                                </Item> 
                            </View>
                        </View>   
                       
                        <View style={style.flexRow}>
                            <View style={style.r2}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="shoulders" />
                                </Item>  
                            </View>
                            <View  style={style.shoulderIndicator}>
                            <View style={style.shoulderContainer} >
                                    <Text style={style.line}> - - - - - - - -</Text>
                                </View>

                            </View>
                            </View>     

                        <View style={style.flexRow}>
                            <View style={style.r3}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="chest" />
                                </Item> 
                            </View>
                            <View style={style.chestIndicator}>
                               <View style={style.chestContainer}>
                                    <Text style={style.line}>- - - - - - - - - - - -   - - - - - - - - - - -</Text>
                                </View>
                            </View>
                        </View>
                       
                        <View style={[style.flexRow, style.bisep]}>
                            <View style={style.flexRow}>
                                <View style={style.r4}>
                                    <Item style={style.picker}>
                                        <Input  style={style.pickerItem} placeholder="right bisep" />
                                    </Item> 
                                </View> 
                                <View style={style.bisepIndicator}>
                                    <View style={style.BisepContainer}>
                                            <Text style={style.line}>- - - - - - - - - -</Text>
                                        </View> 
                                </View>
                            </View>
                        
                        <View style={style.flexRow}>    
                            <View style={style.bisepIndicator}>
                                <View style={style.BisepContainer}>
                                        <Text style={style.line}>- - - - - - - - - -</Text>
                                </View> 
                            </View>
                            <View style={style.r5}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="left bisep" />
                                </Item> 
                            </View>
                        </View>
                        </View>

                        <View style={[style.flexRow, style.forearm]}>
                        <View style={style.flexRow}>
                            <View style={style.r6}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="left forearm" />
                                </Item> 
                            </View>
                            <View style={style.leftForearmIndicator}>
                                    <View style={style.leftForearmContainer}>
                                            <Text style={style.line}>- - - - - - - - - -</Text>
                                    </View> 
                                </View>

                        </View>
                            <View style={style.flexRow}>
                                <View style={style.rightForearmIndicator}>
                                    <View style={style.rightForearmContainer}>
                                            <Text style={style.line}>- - - - - - - - - -</Text>
                                    </View> 
                                </View>
                            <View style={style.r7}>
                            <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="right forearm" />
                                </Item> 
                            </View>
                        </View>

                        </View>
                        <View style={style.flexRow}>   
                            <View style={style.r8}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="waist" />
                                </Item> 
                            </View>
                            <View style={style.waistIndicator}>
                                <View style={style.waistContainer}>
                                        <Text style={style.line}>- - - - - - - - - -</Text>
                                </View> 
                             </View>
                            </View>   

                        <View style={style.flexRow}>
                        <View style={style.hipIndicator}>
                                <View style={style.hipContainer}>
                                        <Text style={style.line}>- - - - - - - - - -</Text>
                                </View> 
                             </View>
                            <View style={style.r9}>
                            <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="hips" />
                                </Item> 
                            </View>
                        </View>
                        <View style={[style.flexRow, style.thigh]}>
                        <View style={style.flexRow} >
                            <View style={style.r10}>
                            <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="left thigh" />
                                </Item> 
                            </View>
                            <View style={style.leftThighIndicator}>
                                <View style={style.leftThighContainer}>
                                        <Text style={style.line}>- - - - - - - - - -</Text>
                                </View> 
                             </View>

                        </View>

                        <View style={style.flexRow}>
                            <View style={style.rightThighIndicator}>
                                <View style={style.rightThighContainer}>
                                        <Text style={style.line}>- - - - - - - - - -</Text>
                                </View> 
                             </View>

                            <View style={style.r11}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="right thigh" />
                                </Item> 
                            </View>
                        </View>

                        </View>
                        <View style={[style.flexRow, style.calf]}>
                        <View style={style.flexRow}>
                            <View style={style.r12}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="left calf" />
                                </Item> 
                            </View>
                            <View style={style.leftCalfIndicator}>
                                <View style={style.leftCalfContainer}>
                                        <Text style={style.line}>- - - - - - - - - -</Text>
                                </View> 
                             </View>

                        </View>
                        <View style={style.flexRow}>
                            <View style={style.rightCalfIndicator}>
                                <View style={style.rightCalfContainer}>
                                        <Text style={style.line}>- - - - - - - - - -</Text>
                                </View> 
                             </View>
                            <View style={style.r13}>
                                <Item style={style.picker}>
                                    <Input  style={style.pickerItem} placeholder="right thigh" />
                                </Item> 
                            </View>
                        </View>

                        </View>
                        <View style={style.r14}>
                             <Item style={style.picker}>
                                <Input  style={style.pickerItem} placeholder="weight" />
                            </Item>   
                        </View>

                        <View>
                            <Button style={style.button}>
                                <Text style={style.buttonText}> Update Measurement</Text>
                            </Button>
                        </View>
                        </Form>
                        </Content>
                       
                </View>
                        
            </View>
        )
    }
}

export default Measurement