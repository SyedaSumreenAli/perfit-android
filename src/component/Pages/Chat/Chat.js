import React, { Component } from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import {Button,Icon,Content,Form,Item, Input} from 'native-base'
import style from './style'
import SendMessage from './SendMessage'
import ReceiveMessage from './ReceiveMessage'


class Chat extends Component{
    static navigationOptions = {
        headerShown: false,
      };

      state={
          send:{
              msg:"Can you please let me know the price?",
              time:"10 : 32 AM"
          },
          receive:{
            msg:"Hey Ahmed. Sure, Just Login with your credintials and you can see suggested trainers based on reviews 🤓",
            time:"10 : 35 AM"
        }
      }
    
    render(){
   const {send , receive} = this.state
    return(
     <View style={style.container}>
        <View style={style.flexRow}>
          <View style={[style.flex2, style.pb5]}>
            <Text style={style.uppercaseH1}>Marie Sutton</Text>
          </View>
          <View style={style.crossIconContainer}>
            <Button
              transparent
              onPress={() => this.props.navigation.replace("Inbox")}
            >
              <Icon
                name="circle-with-cross"
                type="Entypo"
                style={style.crossIcon}
              />
            </Button>
          </View>
        </View>

        <View style={style.messageContainer}>
            <SendMessage message={send.msg} time ={send.time}/>
            <ReceiveMessage message={receive.msg} time ={receive.time}/>
        </View>
                
        <View style={[style.inputContainer]}>
            <View style={style.addIconContainer}>
                <Icon name="plus" type="AntDesign" style={style.addIcon}/>
            </View>
            <View style={style.formContainer}>
            <Content>
                    <Form> 
                        <Item style={style.inputBox} underline={false} >
                            <Input  style={style.input}/>
                        </Item>
                    </Form>
                </Content>
            </View>
               
            <View >
            <TouchableOpacity style={style.sendButtonContainer}  transparent>
                <View style={style.iconbg}>
                    <Icon name="send-circle" type="MaterialCommunityIcons" style={style.sendIcon}/>
                </View>
                </TouchableOpacity>
            </View>
            
             
          
        </View>
    </View>
        )
    }
}

export default Chat