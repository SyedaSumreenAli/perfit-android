import React,{Component} from 'react'
import {ScrollView,View, Text, TouchableOpacity, BackHandler} from 'react-native'
import Image from 'react-native-scalable-image'
import {Icon, Button} from 'native-base'
import style from './style'
import { height, width } from 'react-native-dimension'


class PersonalTraining extends Component{
    render(){
        return(
            <ScrollView>
            <View style={style.container}>  
               <TouchableOpacity style={[style.card, {backgroundColor:"#B1B7BF"}]}>
                    <View style={style.imgContainer}>
                        <Image 
                        height={height(20)}
                        style={style.img}
                        source={require('../../../assets/images/personalTraining/training.png')}/>
                    </View>

                    <View style={style.textContainer}>
                        <View style={style.nameContainer}>
                            <Text style={[style.h1Medium, {color:"white"}]}>Strength Training</Text>
                        </View>

                        <View style={style.flexRow}>
                            <View  style={[style.borderRight, style.p3nl]}>
                                <Text style={[style.smallMedium , {color:"white"}]}>Start Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={[style.dateIcon ,  {color:"white"}]}/>
                                    <Text style={[style.dateText,  {color:"white"}]}>20/0/2020</Text>
                                </View>
                            </View>
                            <View style={ style.p3} >
                                <Text style={[style.smallMedium,  {color:"white"}]}>End Date</Text>
                                <View style={style.flexRow}>
                                <Icon name="date" type="Fontisto" style={[style.dateIcon ,  {color:"white"}]}/>
                                    <Text style={[style.dateText, {color:"white"}]}>20/0/2020</Text>
                                </View>
                            </View>
                        </View>

                        <View style={style.progressContainers}>
                            <Text style={[style.h4Medium , { color:"white"}]}> My Progress</Text>
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(20)}]}></View>
                            </View>
                        </View>

                            </View>
               </TouchableOpacity>

               {/* <TouchableOpacity style={[style.card2,style.pt2]}>
                    <View style={style.imgContainer2}>
                        <Image 
                        height={height(20)}
                        style={style.img}
                        source={require('../../../assets/images/personalTraining/training.png')}/>
                    </View>

                    <View style={style.textContainer2}>
                        <View style={style.nameContainer2}>
                            <Text style={style.h1Medium}>Strength Training</Text>
                        </View>

                        <View style={[style.flexRow, style.dateContainer2]}>
                            <View  style={[style.borderRight,  style.p3nl]}>
                                <Text style={style.smallMedium}>Start Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>20/0/2020</Text>
                                </View>
                            </View>
                            <View style={ style.p3} >
                                <Text style={style.smallMedium}>End Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>20/0/2020</Text>
                                </View>
                            </View>
                        </View>

                        <View style={style.progressContainers2}>
                            <Text style={style.h4Medium}> My Progress</Text>
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(20)}]}></View>
                            </View>
                        </View>

                        <View style={style.reviewButtonContainer2}>
                            <Button style={style.reviewButton}>
                                <Text style={style.reviewButtonText}> Add Review </Text>
                            </Button>
                        </View>


                    </View>
               </TouchableOpacity> */}
               <TouchableOpacity style={[style.card]}>
                    <View style={style.imgContainer}>
                        <Image 
                        height={height(20)}
                        style={style.img}
                        source={require('../../../assets/images/personalTraining/training.png')}/>
                    </View>

                    <View style={style.textContainer}>
                        <View style={style.nameContainer}>
                            <Text style={style.h1Medium}>Strength Training </Text>
                        </View>

                        <View style={style.flexRow}>
                            <View style={[style.borderRight ,  style.p3nl]}>
                                <Text>Start Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>20/0/2020</Text>
                                </View>
                            </View>
                            <View style={ style.p3}>
                                <Text>End Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>20/0/2020</Text>
                                </View>
                            </View>
                        </View>

                        <View style={style.progressContainers}>
                            <Text style={style.h4Medium}> My Progress</Text>
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(20)}]}></View>
                            </View>
                        </View>

                      
                       
                    </View>
               </TouchableOpacity>

               <TouchableOpacity style={[style.card]}>
                    <View style={style.imgContainer}>
                        <Image 
                        height={height(20)}
                        style={style.img}
                        source={require('../../../assets/images/personalTraining/training.png')}/>
                    </View>

                    <View style={style.textContainer}>
                        <View style={style.nameContainer}>
                            <Text style={style.h1Medium}>Strength Training </Text>
                        </View>

                        <View style={style.flexRow}>
                            <View style={[style.borderRight ,  style.p3nl]}>
                                <Text>Start Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>20/0/2020</Text>
                                </View>
                            </View>
                            <View style={ style.p3}>
                                <Text>End Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>20/0/2020</Text>
                                </View>
                            </View>
                        </View>

                        <View style={style.progressContainers}>
                            <Text style={style.h4Medium}> My Progress</Text>
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(20)}]}></View>
                            </View>
                        </View>

                      
                       
                    </View>
               </TouchableOpacity>
               <View style={style.flexbox}></View>
            </View>
            </ScrollView>
        )
    }
}


export default PersonalTraining