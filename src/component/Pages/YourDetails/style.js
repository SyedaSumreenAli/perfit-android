import { StyleSheet } from "react-native";
import { width, height } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    flex: 2,
    // backgroundColor:"red",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    paddingBottom: height(13.45),
  },
  logoContainer: {
    flex: 1,
    // paddingTop: height(7.7),
    // paddingBottom: height(8),
    zIndex: 2,
    // backgroundColor:"pink",
  },
  logo: {
    marginTop: height(3),
    width: width(40),
    height: height(23),
  },
  textContainer: {
    // backgroundColor:"yellow",
    flex: 3,
  },
  linksContainer: {
    // backgroundColor:"green",
    flex: 2,
    alignSelf:"center"
  },
  textCenter: {
    textAlign: "center",
  },
  errMsg:{
    
      paddingTop:height(2),
      paddingLeft:height(5),
        color: "#EB1717",
        fontFamily: "Montserrat-Medium",
        fontSize: 14
  
  },
  h1: {
    fontSize: 26.52,
    paddingTop: height(5),
    paddingBottom: height(1),
    color: "#222222",
    fontFamily: "Montserrat-Regular",
    fontWeight: "300",
  },
  subText: {
    fontFamily: "Montserrat-Light",
    color: "#928E8E",
    fontSize: 14.52,
    fontWeight: "300",
    paddingHorizontal: width(4),
  },
  bgImage: {
    position: "absolute",
    //   backgroundColor:'red',
    marginTop: -height(5),
    marginLeft: -width(15),
  },
  linearGradient: {
    paddingLeft: 45,
    paddingRight: 45,
    opacity: 0.9,
    borderRadius: 25,
  },
  buttonText: {
    fontSize: 19.23,
    fontFamily: "Montserrat-Light",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
  },
  flexRow: {
    flexDirection: "row",
  },
  flex2: {
    flex: 2,
  },
  spaceBetween: {
    justifyContent: "space-between",
  },
  genderContainer: {
    marginHorizontal: width(4),
    marginVertical: height(4),
    // backgroundColor:'red'
  },
  btnLg: {
    width: width(30),
    justifyContent: "center",
    borderColor: "#D8D8D8",
    borderRadius: 8,
  },
  btnLgSelected: {
    width: width(30),
    justifyContent: "center",
    borderColor: "#ffd700",
    borderRadius: 8,
  },
  h2Regular: {
    fontFamily: "Montserrat-Regular",
    color: "#222222",
    fontSize: 19,
  },
  h2RegularSelected: {
    fontFamily: "Montserrat-Regular",
    color: "#ffd700",
    fontSize: 19,
  },
  formContainer: {
    //  backgroundColor:"yellow",
    width: width(90),
    marginBottom: height(5),
    flex: 3,
  },
  formIcon: {
    color: "#FDBE51",
    fontSize: 17.58,
    marginRight: width(3),
  },
  formText: {
    color: "#C0C0C0",
    fontFamily: "Montserrat-Medium",
    fontSize: 16,
  },
  errMsg: {
    color: "#EB1717",
    fontFamily: "Montserrat-Medium",
    fontSize: 15,
    marginLeft: width(2),
  },
});

export default style;