import React, { Component } from "react";
import { ScrollView, View, Text, Image, DatePickerAndroid } from "react-native";
import logo from "../../../assets/images/logo.png";
import bgCity from "../../../assets/images/splash-city-bg.png";
import { Button, Content, Form, Item, Input, Icon, Spinner } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import style from "./style";
// import{width, height} from 'react-native-dimension'

import { validateAll } from 'indicative/validator';

import { postDetail } from '../../../redux/actions/detialsAction'

import { connect } from 'react-redux'





class YourDetails extends Component {
  constructor(props) {
    super(props);
    
      this.state = {
        weight: "",
        height: "",
        gender: "",
        date: "",
        datepicker: false,
        error:'',
        validation:false,
        isLoading: false
      };
    
  }


  navigate = () => {
    this.setState({isLoading:false})
    this.props.navigation.navigate("SetYourGoals")

  }




  datepicker = async () => {
    try {
      const {
        action,
        year,
        month,
        day
      } = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: new Date(2020, 4, 25)
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ date: `${year}-${month + 1}-${day}` });
        console.log("date start" + " " + this.state.date)
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  }

  handleSubmit = async() =>{
    console.log('click')
    const { weight, height, gender, date, error, validation }= this.state

    console.log('weight'+ weight)
    console.log('height'+ height)
    console.log('date'+ date)
    console.log('gender'+ gender)
   try{

      if( gender === "" ){
        let errorMessage = 'Please select gender'
          // const updatedError = [errorMessage, ...error]
          // console.log(updatedError);
  
          this.setState({
            error: errorMessage,
            validation:false
          })
      }
      else{
        if( weight === "" ){
          let errorMessage = 'Please select weight'
            // const updatedError = [errorMessage, ...error]
            // console.log(updatedError);
    
            this.setState({
              error: errorMessage,
              validation:false
            })
        }
        else if( parseFloat(weight) <= 0 ){
          let errorMessage = 'Invalid weight, please enter again'
            // const updatedError = [errorMessage, ...error]
            // console.log(updatedError);
    
            this.setState({
              error: errorMessage,
              validation:false
            })
        } else{
          if(height === "" ){
            let errorMessage = 'Please select height'
              // const updatedError = [errorMessage, ...error]
              // console.log(updatedError);
      
              this.setState({
                error: errorMessage,
                validation:false
              })
          }

          else if( parseFloat(height) <= 0 || height > 272 ){
        
            // 272 cm is the highest value of heigh in the modern human age 
      
            let errorMessage = 'Invalid height, please enter again'
              // const updatedError = [errorMessage, ...error]
              // console.log(updatedError);
      
              this.setState({
                error: errorMessage,
                validation:false
              })
          } else{
             if( date === "" ){
              let errorMessage = 'Please select date'
                // const updatedError = [errorMessage, ...error]
                // console.log(updatedError);
        
                this.setState({
                  error: errorMessage,
                  validation:false
                })
            }else{
              this.setState({
                isLoading: true,
                validation:true
              })
              this.props.postDetail(gender, weight, height, date, this.navigate)
            }
          }

        } 
      }
    
      

        
    }catch(error){
      console.log(error)
    } 
    }
    


  render() {
    const { weight, height, gender, date, error, isLoading } = this.state;

    // console.log(this.props)



    return (
      <ScrollView>
        <View style={style.container}>
          <View style={style.logoContainer}>
            <Image source={logo} style={style.logo} />
          </View>

         

          
              

            
                <View style={style.textContainer}>
                  
                    <View>
                      <Image source={bgCity} style={style.bgImage} />
                    </View>
                    <View>
                      <Text style={[style.textCenter, style.h1]}>Your Details</Text>
                    </View>

                    <View>
                      <Text style={[style.textCenter, style.subText]}>
                        To get the most of our app, Help us {"\n"}
                setup suitable program for you
              </Text>
                    </View>
                    <View>
                    {error.length > 0 ? (
                        <Text style={style.errMsg}>{error}</Text>
                      ) : null}  
                    </View>

                    <View
                      style={[
                        style.flexRow,
                        style.flex2,
                        style.spaceBetween,
                        style.genderContainer,
                      ]}
                    >



                      {gender == "female" ? <Button bordered light style={[style.btnLgSelected]} onPress={() => {
                        console.log("female");
                       
                        // this.setState({ gender: "" })
                      }}>
                        <Text style={style.h2RegularSelected}>Female</Text>
                      </Button> :
                        <Button bordered light style={[style.btnLg]} onPress={() => {
                          console.log("female");
                          this.setState({ gender: "female" })
                        }}>
                          <Text style={style.h2Regular}>Female</Text>
                        </Button>
                      }

                      {gender == "male" ? <Button bordered light style={[style.btnLgSelected]} onPress={() => {
                        console.log("male");
                        // this.setState({ gender: "" })
                      }}>
                        <Text style={style.h2RegularSelected}>Male</Text>
                      </Button> :
                        <Button bordered light style={[style.btnLg]} onPress={() => {
                          console.log("male");
                          this.setState({ gender: "male" })
                        }}>
                          <Text style={style.h2Regular}>Male</Text>
                        </Button>
                      }
                    </View>
                

                  <View style={style.formContainer}>
                    <Content>
                      <Form>
                        {/* <Text style={style.errMsg}>{errMsg}</Text> */}
                        <View style={[style.flexRow]}>
                          <Item style={style.flex2}>
                            <Input
                              style={[style.formText]}
                              placeholder="Your weight (kg)"

                              onChangeText={(text)=>{
                                this.setState({
                                  weight:text
                                })

                                console.log(text)
                                
                              }}

                              value={weight}
                             
                              keyboardType="name-phone-pad"
                              blurOnSubmit={false}
                              returnKeyType="next"
                              onSubmitEditing={() => {
                                this.Height._root.focus();
                              }}                     
       
                              keyboardType="numeric"

                            />
                           
                          </Item>
                          <Item style={style.flex2}>
                            <Input
                              style={[style.formText]}
                              placeholder="Your height (cm)s "
                           
                              onChangeText={(text)=>{
                                this.setState({
                                 height:text
                                })

                                console.log(text)
                              }}

                              value={height}
                              ref={input => {
                                this.Height = input;
                              }}
                              blurOnSubmit={false}
                              returnKeyType="next"
                              onSubmitEditing={() => {
                                this.Date._root.focus();
                              }}
                              keyboardType="numeric"
                            />
                          </Item>
                        </View>

                        <Item>
                          <Input
                            style={[style.formText]}
                            placeholder="Date of birth"
                            onFocus={this.datepicker}
                              
                            onChangeText={(text)=>{
                              this.setState({
                                date:text
                              })

                              console.log(text)
                            }}
                            ref={input => {
                              this.Date = input;
                            }}
                            blurOnSubmit={false}
                            returnKeyType="OK"
                            onSubmitEditing={() => {
                              this.handleSubmit
                            }}
                         
                          
                            value={date}
                          />
                        </Item>
                      </Form>
                    </Content>
                  </View>

                  <View style={style.linksContainer}>
                
                  {isLoading===true ? <Spinner large color='#FFAA2F'/> :  <Button
                      transparent
                      onPress={this.handleSubmit}
                    >
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 4 }}
                        colors={["#efcc64", "#FFAA2F"]}
                        style={style.linearGradient}
                      >
                        <Text style={style.buttonText}>Continue</Text>
                      </LinearGradient>
                    </Button>}
                  </View>
                </View>
          
             

         


        </View>
      </ScrollView>
    );
  }
}

const dispatchStateToProps = (dispatch) => {


  return {

    postDetail: (gender, weight, height, date_of_birth, navigate) => dispatch(postDetail(gender, weight, height, date_of_birth, navigate))
  }


}


export default connect(null, dispatchStateToProps)(YourDetails);