import {StyleSheet} from 'react-native'
import {width , height, totalSize} from 'react-native-dimension'


const style = StyleSheet.create({
tabStyle:{
      backgroundColor:"white",
      height:height(6)
  }  ,

  activeTabStyle:{
      backgroundColor:"white",
      height:height(6)
  },
  activeTabTextStyle:{
      color:"#000",
      fontFamily:"Montserrat-SemiBold",
      fontSize: totalSize(2)
  },
  tabTextStyle:{
      color:"#000",
      fontFamily:"Montserrat-SemiBold",
      fontSize: totalSize(2)
  },
  tabBarUnderlineStyle:{
    height:5,backgroundColor:"#FCB345"
  },
  tabContainerStyle:{
    height:height(8),
    justifyContent:"flex-end",
    alignItems:"flex-end",
    backgroundColor:"white",
    marginBottom:height(3)
  }

})

export default style