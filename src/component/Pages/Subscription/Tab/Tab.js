import React, { Component } from 'react';
import {View, Text, ScrollView, ToucableOpacity} from 'react-native';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import PersonalTraining from '../../PersonalTraining/PersonalTraining'
import Dietitian from '../../Dietitian/Dietitian'
import style from './style'


 class TabBar extends Component {
    static navigationOptions = {
        headerShown: false,
      };
   
     

  render() {
    return (
      <Container style={style.tabContainer}>
        
          <Tabs 
          tabContainerStyle={style.tabContainerStyle}
           tabBarUnderlineStyle={style.tabBarUnderlineStyle} 
           >
            <Tab heading="PERSONAL TRAINING"
              tabStyle={style.tabStyle}
               activeTabStyle={style.activeTabStyle}  
               textStyle={style.tabTextStyle}
                activeTextStyle={style.activeTabTextStyle }
                >

                <PersonalTraining />
            </Tab>
          
            <Tab heading="DIETITIAN" 
              tabStyle={style.tabStyle}
               activeTabStyle={style.activeTabStyle} 
                textStyle={style.tabTextStyle} activeTextStyle={style.activeTabTextStyle}>
                {/* <Dietitian/> */}
                <PersonalTraining />
            </Tab>
          </Tabs>
       </Container>
   
    
    );
  }
}

export default TabBar