import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import style from "./style";
import Image from "react-native-scalable-image";
import { width } from "react-native-dimension";
class ImageCard extends Component {
  
  
  state = {
    active: false,
  };


 
  handleClick = () => {
   





    

    console.log(this.props)

  this.props.workoutHandler(this.props.name)

  
 

 
};

  render() {
    const { active } = this.state;
    return (
      <TouchableOpacity
        style={[
          style.categoryCard,
          {
            borderColor: active == true ? "#FDAB42" : "#979797",
            marginRight: this.props.lastImage == true ? width(4) : width(0),
          },
        ]}
        onPress={this.handleClick}
      >
        <View style={style.imageContainer}>
          <Image
            source={this.props.workout==this.props.title ? this.props.imgUri1 : this.props.imgUri0}
            style={{flex: 1,
              width: null,
              height: null,
              resizeMode: 'contain'}}
          />
        </View>

        <Text
          style={[
            style.categoryTitle,
            { color: active == true ? "#FDAB42" : "#222222" },
          ]}
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}
export default ImageCard;