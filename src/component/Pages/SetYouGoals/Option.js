import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import style from "./style";
import Image from "react-native-scalable-image";
import { width } from "react-native-dimension";
class Option extends Component {
  state = {
    active: false,
  };



  handleClick = () => {
   
      

    this.props.optionHandler(this.props.title)

   

   
  

  };
  render() {
    const { active } = this.state;
    return (
      <TouchableOpacity onPress={this.handleClick}>
        <Text
          style={[
            style.h4,
            {
              color: this.props.title==this.props.option ? "#FDAB42" : "#222222",
              textDecorationLine: this.props.title==this.props.option ? "underline" : "none",
            },
          ]}
        >
          {" "}
          {this.props.title}{" "}
        </Text>
      </TouchableOpacity>
    );
  }
}
export default Option;