import React, { Component } from "react";
import { ScrollView, View, Text, AsyncStorage, ProgressBarAndroid,Alert } from "react-native";
import { Button, Spinner } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import { Icon } from "@up-shared/components";
import logo from "../../../assets/images/logo.png";
import bgCity from "../../../assets/images/splash-city-bg.png";
import Image from "react-native-scalable-image";
import style from "./style";
import GoalCard from "./goalCard";
import WorkoutCard from './workoutCard'
import Option from "./Option";
import axios from "axios";
import { height } from "react-native-dimension";
import { TouchableWithoutFeedback, TouchableOpacity } from "react-native-gesture-handler";
// import GetMuscles from '../../../assets/images/gain-muscles.svg';

import { setGoal } from '../../../redux/actions/GoalActions'
import { connect } from 'react-redux'

class SetYourGoals extends Component {
  
    

    state = {
      errMsg: false,
      active: false,

      user_id: "",
      isLoading: false,

      goal: "",
      workout: "",
      option: "",

    };



  async componentDidMount() {


    this._retrieveData();



  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        // We have data!!
        // console.log(value);
        // console.log("user_id");
        this.setState({ user_id: value });
      }
    } catch (error) {
      // console.log(error);
    }
  };


  handleClick = () => {
    const { active } = this.state;
    this.setState({
      active: !active,
    });
  };


  navigate=()=>{

    this.props.navigation.navigate("Tab");
  }


  loading=()=>{

        this.setState({

          isLoading:false
        })
  }

  submit = () => {


    console.log(this.state.user_id);


    if (this.state.workout && this.state.goal && this.state.option) {


      console.log(this.state)

      this.setState({
        errMsg:false,
        isLoading:true
      })

      let goals = {

        user_id: this.state.user_id,
        goal: this.state.goal,
        workout: this.state.workout,
        exercise_level: this.state.option
      }

      this.props.setGoal(goals,this.navigate,this.loading)


    } else {

      this.setState({
        errMsg:true
      })
    }


  }

  goalHandler = (text) => {

    this.setState({

      goal: text

    })

    console.log("fired")

  }


  workoutHandler = (text) => {

    this.setState({

      workout: text,
    })

  }

  optionHandler = (text) => {

    this.setState({

      option: text,
    })

    console.log("fired")

    console.log(this.state.option)


  }

  render() {
    const { errMsg, active } = this.state;

    return (

      <View style={style.container}>
        <View style={style.logoContainer}>
          <Image source={logo} height={height(20)} />
        </View>

        <View style={style.h1Container}>
          <View>
            <Image source={bgCity} style={style.bgImage} />
          </View>
         
          <View style={[style.h2Container1,
            // { backgroundColor:"pink"}
            ]}>
          {this.state.errMsg ?
           <Text style={{ 
          //  backgroundColor:"yellow",
            // marginTop:height(6),
            paddingLeft:height(5),
              color: "#EB1717",
              fontFamily: "Montserrat-Medium",
              fontSize: 14,
              // zIndex:5
          }}
          >Please Select All Fields</Text>:null} 
            <Text style={[style.textCenter, style.h2Dark, style.mt_3]}>
              Set your goal
              </Text>
          </View>
        </View>

        <ScrollView
          scrollEventThrottle={16}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
        >
          <View style={style.categoryContainer}>



            <GoalCard

              imgUri0={require("../../../assets/images/icons/Muscle_gain_black.png")}
              imgUri1={require("../../../assets/images/icons/Muscle_gain_yellow.png")}
              title="Muscle Gain"

              name="Muscle Gain"
              goalHandler={this.goalHandler}
              goal={this.state.goal}


            />



            <GoalCard
              imgUri0={require("../../../assets/images/icons/Weight_loss_black2.png")}
              imgUri1={require("../../../assets/images/icons/Weight_loss_yellow2.png")}
              title="Lose Weight"

              name="Lose Weight"
              goalHandler={this.goalHandler}
              goal={this.state.goal}


            />



            <GoalCard
              imgUri0={require("../../../assets/images/icons/Weight_gain_black.png")}
              imgUri1={require("../../../assets/images/icons/Weight_gain_yellow.png")}
              title="Weight Gain"
              lastImage={true}
              name="Weight Gain"
              goalHandler={this.goalHandler}
              goal={this.state.goal}
            />

            <GoalCard
              imgUri0={require("../../../assets/images/icons/Diet_plan_black.png")}
              imgUri1={require("../../../assets/images/icons/Diet_plan_yellow.png")}
              title="Diet Plan"
              name="Diet Plan"
              goalHandler={this.goalHandler}
              goal={this.state.goal}
            />
          </View>
        </ScrollView>

        <View style={style.textContainer2}>
          <View>
            <Text style={style.h2Dark}> Workout Spot</Text>
          </View>
        </View>

        <View style={style.workOutContainer}>

          <WorkoutCard
            imgUri0={require("../../../assets/images/icons/Exercise_at_home_black.png")}
            imgUri1={require("../../../assets/images/icons/Exercise_at_home_yellow.png")}
            title="Home"

            name="Home"
            workoutHandler={this.workoutHandler}
            workout={this.state.workout}


          />

          <WorkoutCard
            imgUri0={require("../../../assets/images/icons/Exercise_at_gym_black.png")}
            imgUri1={require("../../../assets/images/icons/Exercise_at_gym_yellow.png")}
            title="Gym"
            name="Gym"
            workoutHandler={this.workoutHandler}
            workout={this.state.workout}

            lastImage={true}
          />


        </View>


        <View style={style.textContainer}>
          <View>
            <Text style={style.h2Dark}> Exercise Level</Text>
          </View>
          <View style={[style.flexRow, style.optionContainer]}>
            <Option title="Beginner" optionHandler={this.optionHandler} option={this.state.option} />
            <Option title="Intermediate" optionHandler={this.optionHandler} option={this.state.option} />
            <Option title="Advance" optionHandler={this.optionHandler} option={this.state.option} />
          </View>
        </View>

        <View style={style.linksContainer}>

         


          {this.state.isLoading ? <Spinner  large color='#FFAA2F' /> :
            <Button
              transparent
              onPress={this.submit}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 4 }}
                colors={["#efcc64", "#FFAA2F"]}
                style={style.linearGradient}
              >
                <Text style={style.buttonText}>Start Training</Text>
              </LinearGradient>
            </Button>
          }
          
        </View>
      </View>

    );
  }
}


const dispatchStateToProps = (dispatch) => {


  return {

    setGoal: (goals,navigate,loading) => dispatch(setGoal(goals,navigate,loading))
  }



}

export default connect(null,dispatchStateToProps)(SetYourGoals);