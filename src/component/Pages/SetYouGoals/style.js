import { StyleSheet } from "react-native";
import { width, height } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor:"pink",
    margin: 0,
    backgroundColor: "white",
  },
  logoContainer: {
  //  backgroundColor:"blue",
    //  flex: 4,
     height: height(23),
    justifyContent: "flex-end",
    alignItems: "center",
    zIndex: 2,
    marginBottom:-height(10)
  },
  logo:{
    width: width(40),
    height: height(10),
    
  },
  h1Container: {
    flex: 4,
    height: height(10),
    // backgroundColor:'green',
    justifyContent: "flex-end",
  },
  textContainer: {
    flex: 4,
    //  backgroundColor:'green',
    justifyContent: "center",
    alignItems: "center",
    height: height(18),
  },
  textContainer1: {
    flex: 3,
    //  backgroundColor:'green',

    justifyContent: "flex-end",
    alignItems: "center",
    height: height(15),
  },
  textContainer2: {
    flex: 3,
    marginTop:-height(12),
    marginBottom:height(1),
      // backgroundColor:'green',
    // height:height(10),
    justifyContent: "center",
    alignItems: "center",
  },
  h2Dark: {
    fontFamily: "Montserrat-Bold",
    fontSize: 22,
    color: "#222222",
    // backgroundColor:"yellow",
    justifyContent: "center",
    alignSelf: "center",
    // marginTop:-height(3),
    // height: height(7),

    // paddingTop:height(2)
  },
  optionContainer: {
    justifyContent: "flex-end",
  },
  h1: {
    fontSize: 26.52,
    paddingTop: height(14),
    paddingBottom: height(1),
    color: "#222222",
    fontFamily: "Montserrat-Regular",
    fontWeight: "300",
  },
  subText: {
    fontFamily: "Montserrat-Light",
    color: "#928E8E",
    fontSize: 14.52,
    fontWeight: "300",
    paddingHorizontal: width(4),
  },
  bgImage: {
    position: "absolute",
    marginTop: -height(7),
  },
  textCenter: {
    alignItems: "center",
    textAlign: "center",
  },
  linksContainer: {
    // backgroundColor:"gray",
    flex: 2,
    height: height(15),
    justifyContent: "flex-start",
    alignItems: "center",
  },
  linearGradient: {
    paddingLeft: 45,
    paddingRight: 45,
    opacity: 0.9,
    borderRadius: 25,
    marginBottom:height(3)
  },
  buttonText: {
    fontSize: 19,
    fontFamily: "Montserrat-Light",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
  },
  flexRow: {
    flexDirection: "row",
  },
  h4: {
    fontFamily: "Montserrat-Medium",
    color: "#222222",
    fontSize: 15,
  },

  mt_3:{
    // paddingTop:-height(3),
    // marginTop:-height(8),
    //  marginBottom:-height(),
    // position:"absolute",
      // backgroundColor:"yellow"
  },
  h2Container1:{
    // backgroundColor:"green",
    // height:height(6),
 //   position:"absolute",

  },
  /************* Category Card Style ****************/

  categoryContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    position:"relative",

    //  flex:9,
       marginTop: height(2),
     zIndex:3,
     height: height(22),
      // backgroundColor:"red"
  },

  categoryCard: {
    borderColor: "#F29C1F",
    //  backgroundColor:"yellow",
    width: 150,
    justifyContent: "center",
    borderColor: "#FDAB42",
    borderRadius: width(8),
    alignItems: "center",
    // height: 180,
    height:120,
    borderWidth: width(0.4),
    marginLeft: width(4),
  },
  imageContainer: {
    flex: 12,
    // backgroundColor:"pink",
    padding:12,
    justifyContent: "center",
    alignItems: "center",
  },
  categoryImage: {
    width:100,
    height: 100,
  },

  categoryTitle: {
    alignSelf: "center",
    justifyContent: "flex-end",
    // backgroundColor:"red",
    height: height(1),
    flex: 3,
    fontFamily: "Montserrat-Medium",
    color: "#FDAB42",
  },
  lastImage: {
    paddingRight: width(4),
  },

  /************* Category Card Style ****************/

  workOutContainer:{
    flexDirection: "row",
     justifyContent:"center",
    //  backgroundColor:"red",
    marginTop:-height(3),
    marginBottom:-height(3)
  }
});

export default style;
