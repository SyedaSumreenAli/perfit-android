import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    container:{
        backgroundColor:"white",
        flex:1
    },
    flexRow: { flexDirection: "row" },

    header:{
      height:height(10),
      flexDirection:'row',
      
    },
    headerTextContainer:{
      flex:6,
      // backgroundColor:"yellow",
      justifyContent:'center',
      alignItems:'center'
    },
    menuContainer:{
      flex:2,
      // backgroundColor:'pink',
      justifyContent:'center',
      alignItems:'center'
    },







    arrowContainer: {
      flex: 2,
      //  backgroundColor:"orange",
       alignItems:'center',
      justifyContent: "center",
      zIndex:3
    },
  
    icon: {
    //    backgroundColor:"green",
      alignSelf: "flex-start",
      justifyContent:"flex-end",
      fontSize: 35,
      paddingLeft:width(5)
    },
    flex4: {
     
        position:"absolute",
        width:width(100),
        height:height(12),
        alignItems:"center",
      justifyContent: "center",
    //  backgroundColor:"green",
    },
    headerText: {
      // backgroundColor:"red",
      flexDirection: "column",
      color: "#424242",
      fontSize: totalSize(2.2),
      textTransform:"capitalize",
      fontFamily: "Montserrat-SemiBold",
    },
    tabContainer:{
        backgroundColor:"white",
        flexDirection:"row",
        justifyContent:"space-around",
        // alignSelf:"center",
        shadowColor:"#f2eded",
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
        elevation:6,
        // marginBottom: height(8),
    },
    tabButton:{
        backgroundColor:"#FCB345",
        marginVertical:height(1),
        width:width(28),
        justifyContent:"center",
        alignItems:'center',
        borderRadius:25,
        height:height(5)
    },
    tabButtonText:{
        color:"#fff",
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(1.8)
    }
})

export default style