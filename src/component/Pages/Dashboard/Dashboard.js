import React, { Component } from 'react';
import {View, Text, ScrollView, ToucableOpacity, Image, AsyncStorage} from 'react-native';
import { Icon, Button } from 'native-base';
import style from "./style"
import LinearGradient from "react-native-linear-gradient";
import Subscription from '../Subscription/Subscription';
import Profile from '../Profile/Profile';
import Payment from '../Payment/Payment';
import SelectionMain from '../SelectionPage/Selection';
import Modal from 'react-native-modal';
import { height, width, totalSize } from 'react-native-dimension';

 class Dashboard extends Component {
    static navigationOptions = {
        headerShown: false,
      };
      state={
          pageName:"profile",
          isModalVisible: false,
      }

      toggleModal = () => {
        this.setState({isModalVisible: !this.state.isModalVisible});
        console.log(this.state.isModalVisible)
      };

     handlePage = (name)=>{
        this.setState({
            pageName:name
        })
      }
     
      _logout = async () => {
        try {
          this.setState({
            isModalVisible: false,
          })
          await AsyncStorage.setItem("user_id", null);
          this.navigate('Login')
        } catch (error) {
          console.log(error);
        }
      };
    

    renderPage =()=> {
        const {pageName} = this.state
        if (pageName=='profile') {
            return <Profile/>;
          }

        if (pageName=='subscription') {
            return <Subscription/>;
          }

        if (pageName=='payment') {
            return <Payment/>;
          }
        
    }  

  render() {
      const {pageName} = this.state
    return (
        <ScrollView>
            <View style={style.container}>
          
           <View style={style.header}>
                    <View style={style.arrowContainer}>
                        <Button transparent onPress={()=>this.props.navigation.navigate('Home')}>
                        <Image source={require('../../../assets/images/icons/arrow.png')} />

                        </Button>
                        {/* <Icon name="arrowleft" type="AntDesign" style={style.icon}/> */}
                    </View>
                       
                            <View style={style.headerTextContainer}>          
                                <Text style={style.headerText}>{pageName}</Text>              
                            </View>
                            

                       
                  
                    <View style={style.menuContainer}>
                    <Button transparent onPress={()=>this.toggleModal()}>
                        {/* <Icon name="arrowleft" type="AntDesign" style={style.icon}/> */}
                        <Image source={require('../../../assets/images/icons/menu.png')} style={{zIndex:5}} />
                    </Button>
                    </View>
                      {this.state.isModalVisible?  <View 
                            style={{
                                backgroundColor:'white',
                                borderRadius:5,
                                borderWidth:1,
                                borderColor:'#c0c0c0',
                                flex:1,
                                position:'absolute',
                                // height:height(40),
                                width:width(35),
                                // marginLeft:width(55),
                                right:width(15),
                                marginTop:height(1),
                                elevation:5
                            }}
                    >
                       <Button
                       onPress={() => {
                     
                          this.setState({
                            isModalVisible: false,
                          })
                          AsyncStorage.clear();
                          this.props.navigation.navigate('Login')
                    
                    }}
                       style={{
                         backgroundColor:'white', 
                         alignItems:'center', 
                         justifyContent:'center',
                        paddingRight:width(2)
                        }}>
                       <Icon name="logout" type="SimpleLineIcons" style={{color:'#333942'}}/>
                         <Text  style={{
                           fontFamily:'Montserrat-Medium',
                           fontSize:totalSize(2),
                           color:'#333942'
                         }}
                         >Logout</Text>
                        
                       </Button>
                    </View>: null}

                   
                    </View>
                      
                

                <View style={style.tabContainer}>
                    <Button transparent
                        onPress={()=>{this.handlePage('subscription')}}
                    >
                        <LinearGradient
                             style={[style.tabButton]}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 4 }}
                            colors={ pageName =='subscription'? ["#F9DF02", "#F29C1F"] : ["#fff", "#fff"]}> 
                                <Text style={[style.tabButtonText,
                                  { color: pageName =='subscription'? "white" :"#000"}]}>
                                Subscription</Text>
                      </LinearGradient>
                    </Button>
                    <Button transparent
                        onPress={()=>{this.handlePage('profile')}}
                    >
                        <LinearGradient
                             style={style.tabButton}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 4 }}
                            colors={ pageName =='profile'? ["#F9DF02", "#F29C1F"] : ["#fff", "#fff"]}> 
                                <Text style={[style.tabButtonText,
                                 { color: pageName =='profile'? "white" :"#000"}]}>Profile</Text>
                        </LinearGradient>
                    </Button>
                    <Button transparent
                        onPress={()=>{this.handlePage('payment')}}
                    >
                        <LinearGradient
                             style={style.tabButton}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 4 }}
                            colors={ pageName =='payment'? ["#F9DF02", "#F29C1F"] : ["#fff", "#fff"]}> 
                                <Text style={[style.tabButtonText,
                                  { color: pageName =='payment'? "white" :"#000"}]}>Payment</Text>
                        </LinearGradient>
                    </Button>
                </View>
                    <View>
                        {this.renderPage()}
                    </View>
            </View>
   
            </ScrollView>
    );
  }
}

export default Dashboard