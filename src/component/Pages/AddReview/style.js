import { StyleSheet } from "react-native";
import { height, width, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    paddingTop: height(4),
    // paddingLeft: width(6),

    // height:height(140)
  },
  flexRow: {
    flexDirection: "row",
  },
  flexEnd:{
      alignSelf:"flex-end",
     
  },
  pb5: {
    paddingBottom: height(5),
  },
  flex2: {
    flex: 2,
  },
  crossIconContainer: {
    flex:1,
    // backgroundColor:"yellow",
   
    alignItems:"flex-end"
 
   

  },
  uppercaseH1: {
    textTransform: "capitalize",
    fontFamily: "Montserrat-SemiBold",
    fontSize: 20,
    color: "#424242",
    textAlign: "right",
    //   paddingTop:height(1)
  },
  crossIcon: {
    color: "#3C3C3C",
    
    // backgroundColor:"red",
    justifyContent:"flex-end",
    alignSelf:"flex-end",
    alignItems:"flex-end",
   
    
    fontSize: 26,
  },
  sendMessageOuterContainer:{
      width:width(80),
      justifyContent:"space-between",    
  },
  sendMessageInnerContainer:{
    backgroundColor:"#f2f2f2",
    justifyContent:"center",
    padding:width(2),
    borderRadius:4,
    marginBottom:height(1)
  },

  sendMessageText:{
      color:"#161F3D",
      textAlign:"left",
      fontFamily:"Montserrat-Regular",
      fontSize:totalSize(1.6)
  },
  sendMessageTime:{
      color:"#161F3D",
      fontSize:totalSize(1.4)
  },

  receiveMessageOuterContainer:{
    width:width(60),
    justifyContent:"space-between",
    alignSelf:"flex-end"    
},

  receiveMessageInnerContainer:{
    backgroundColor:"#3C3C3C",
    alignSelf:"flex-start",
    justifyContent:"center",
    padding:width(2),
    borderRadius:4,
    marginBottom:height(1)
  },

  receiveMessageText:{
      color:"#ffffff",
      textAlign:"left",
      fontFamily:"Montserrat-Regular",
      fontSize:totalSize(1.6),
     
  },
  receiveMessageTime:{
    color:"#161F3D",
    fontSize:totalSize(1.4),
    alignSelf:"flex-end",
   
},
  messageContainer:{
    width:width(90),
    alignSelf:"center",
    flex:1,
    // backgroundColor:"yellow"     
  },
  inputContainer:{
      flexDirection:"row",
    //   flex:1,
    //    width:width(100),
       height:height(10),
       backgroundColor:"#BEC4D9", 
    //   alignItems:"flex-end"
        justifyContent:"center"
  },
  formContainer:{
   
    //    width:width(80),
        flex:9,
      height:height(7),
     justifyContent:"center",
     alignSelf:"center"
    //  alignItems:"flex-start"
   
   
     
  },
  input:{
    backgroundColor:"white",
    borderRadius:25,
   
    // borderColor:"#161F3D",
    // borderWidth:0.2,
  },
  inputBox:{
    alignSelf:"center",
    alignSelf:"center",
    borderRadius:25,
    borderColor:"transparent",
   
  },

  addIconContainer:{
    //    backgroundColor:"orange",
      flex:1,
      height:height(10),
      justifyContent:"center",
      alignItems:"flex-end"
    //   width:width(25)
  },
  addIcon:{
      color:"#8C8C8C",
      fontSize:totalSize(3.5)
  },

  sendButtonContainer:{   
    flex:1,
    justifyContent:"center",
   alignSelf:"center",
   margin:width(3)
//    backgroundColor:"#fff",
//    alignItems:"flex-end
  },
  iconbg:{
      backgroundColor:"white",
      borderRadius:50,
     
  },
  sendIcon:{
      color:"#000",
      fontSize:totalSize(6),
      margin:-5,
      borderRadius:50,
  },
  seen:{
      color:"#27D500",
      fontSize:totalSize(2),
      marginLeft:width(1.5)
  },
  starIconOn:{
    color:"#FDC900",
    fontSize:totalSize(2),
    paddingRight:width(1)
  },
  starIcon:{
    color:"#AEAEAE",
    fontSize:totalSize(2),
    paddingRight:width(1)
  },
  starContainer:{
    marginTop:height(2)
  },
  submitBtnContainer:{
    // backgroundColor:"yellow",
    flex:1,
    alignItems:"flex-end"
  },
  submitBtn:{
    width:width(26),
    height:height(5),
    marginRight:width(4),
    justifyContent:"center",
    backgroundColor:"#D8D8D8",
  },
  submitBtnText:{
    fontFamily:"Montserrat-Medium",
    textTransform:"uppercase",
    fontSize:totalSize(1.6)
  }
});

export default style;
