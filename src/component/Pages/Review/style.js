import { StyleSheet } from "react-native";
import { width, height, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    // backgroundColor:"yellow"
    backgroundColor: "white",
    flex: 1,
    // height:height(100)
  },
  flexRow: { flexDirection: "row" },
  iconContainer: {
    flex: 3,
    //  backgroundColor:"orange",
    height: height(13),
    justifyContent: "center",
  },
  icon: {
    // backgroundColor:"green",
    width: width(30),
    alignSelf: "center",
    fontSize: 35,
  },
  flex5: {
    flex: 5,
    alignItems: "flex-start",
    justifyContent: "center",
  },

  headerText: {
    // backgroundColor:"red",
    flexDirection: "column",
    color: "#424242",
    fontSize: 20,
    textTransform: "uppercase",
    fontFamily: "Montserrat-SemiBold",
  },

  listItem: {
    borderBottomColor: "#D8D8D8",
    borderWidth: width(0.1),
    borderColor: "transparent",
    //   justifyContent:"center",
    paddingTop: height(3),
    width: width(90),
    alignSelf: "center",
  },

  imgOuterContainer: {
    //  backgroundColor:"yellow",
    flex: 2,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  textOuterContainer: {
    //  backgroundColor:"skyblue",
    flex: 9,
    justifyContent: "center",
  },

  imageContainer: {
    position: "absolute",
    borderColor: "#d8d8d8",
    borderWidth: width(0.1),
    borderRadius: 50,
  },

  nameText: {
    fontFamily: "Montserrat-Medium",
    color: "#161F3D",
    textTransform: "capitalize",
    fontSize: totalSize(2),
  },
  starIcon: {
    fontSize: 11,
    color: "#FDC900",
    padding: width(0.2),
  },
  arrowLeft: {
    fontSize: 24,
    zIndex: 4,
    // backgroundColor:"red",
    // flex:1,
    width: width(6),
    alignSelf: "center",
  },
  description: {
    color: "#777777",
    fontFamily: "Montserrat-Regular",
    fontSize: totalSize(1.6),
    textAlign: "justify",
  },
  descriptionContainer: {
    // flex:4,
    width: width(70),
    // height:height(10),
    paddingTop: height(1),
    paddingBottom: height(2),
    // justifyContent:"center"
  },
});

export default style;
