import React, {Component} from 'react'
import {View,Text } from 'react-native'
import style from './style'

function Description(props){
    return(
        <Text  style={style.description} numberOfLines={props.showLines}  >{props.description}</Text>
    );
};
export default Description