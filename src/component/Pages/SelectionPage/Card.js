import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Button, Icon, Badge } from "native-base";
import style from "./style";
import Image from "react-native-scalable-image";
import { width, height } from "react-native-dimension";
import { withNavigation } from "react-navigation";


const Card = (props) => {
   let trainerId= props.id
  
  console.log('coming from selection card'+ trainerId)
  return (
    <TouchableOpacity style={style.card}
       onPress={()=>props.navigation.navigate("PTPage", {trainerId})}
    >
      <View style={style.imgContainer}>
        <Image source={props.imgUri} height={height(22)} style={style.image} />
        <Badge style={style.imgCaptionContainer}>
          <Text style={style.imgCaption}>{props.category}</Text>
        </Badge>
      </View>
      <View style={style.contentContainer}>
        <View style={style.cardTitleContainer}>
          <Text style={style.cardTitle}>{props.name}</Text>
        </View>
        <View style={[style.flexRow, style.reviewContainer]}>
          <View style={[style.flexRow, style.reviewStarContainer]}>
            <Icon
              style={style.reviewStarIcon}
              name="star"
              type="AntDesign"
            ></Icon>
            <Icon
              style={style.reviewStarIcon}
              name="star"
              type="AntDesign"
            ></Icon>
            <Icon
              style={style.reviewStarIcon}
              name="star"
              type="AntDesign"
            ></Icon>
            <Icon
              style={style.reviewStarIcon}
              name="star"
              type="AntDesign"
            ></Icon>
            <Icon
              style={style.reviewStarIcon}
              name="star"
              type="AntDesign"
            ></Icon>
          </View>
          <TouchableOpacity style={style.reviewTextContainer}
              onPress={()=>props.navigation.navigate("Review")}
          >
            <Text style={style.reviewText}>Review ({props.review}) </Text>
          </TouchableOpacity>
        </View>
        <View style={style.cardButtonContainer}>
          <Button style={style.cardButton} rounded>
            <Text style={style.cardButtonText}> {props.buttonText}</Text>
          </Button>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default withNavigation(Card);
