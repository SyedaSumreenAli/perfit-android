import React, { Component } from "react";
import { View, Text } from "react-native";
import { Button, Icon, Item } from "native-base";
import Image from "react-native-scalable-image";
import Card from "./Card";
import SignUpCard from "./SignUpCard";
import style from "./style";
import { TouchableOpacity } from "react-native-gesture-handler";
import { ScrollView } from "react-native";
import { width, height } from "react-native-dimension";
import axios from 'axios';
import Card2 from './Card2'
class SelectionMain extends Component {
  static navigationOptions = ({ navigation }) => {
    return { headerShown: false };
  };

  constructor(props) {
    super(props);
    this.state = {
      trainers: [],
      dietician: [],

    };
  }

  async componentDidMount() {
    this.getAllTrainers();
    this.getAllDieticians();
  }

  getAllTrainers() {
    var self = this;
    axios.get("http://system.perfitapp.com/api/trainers/all")
      .then(function (response) {
        // console.log(response.data);
        self.setState({ trainers: response.data.trainers })
      })
      .catch(function (error) {
        // console.log(error);
      })
  }
  getAllDieticians() {
    var self = this;
    axios.get("http://system.perfitapp.com/api/dietitians/all")
      .then(function (response) {
        // console.log(response.data);
        self.setState({ dietician: response.data.dietitians })
      })
      .catch(function (error) {
        // console.log(error);
      })
  }

  render() {


  

    return (
      <View style={style.container}>
        <ScrollView>
          <View style={style.flexRow}>
            <View style={style.arrowContainer}>
              <TouchableOpacity
              // onPress={()=> this.props.navigation.replace("SetYourGoals")}
              >
                <Image source={require('../../../assets/images/icons/arrow.png')} />
              </TouchableOpacity>
            </View>

            <View style={style.logoContainer}>
              <Image
                source={require("../../../assets/images/logo.png")}
                height={height(23)}

              />
            </View>
          </View>
          <View>
            <View style={style.subContainer}>
              <Text style={style.h2Light}>Perfect choice for state</Text>
              <View style={style.flexRow}>
                <View>
                  <Text style={style.h1}>Personal Trainers</Text>
                </View>
                <View style={style.showAllContainer}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("TrainerList")
                    }
                  >
                    <Text style={style.showAllText}>Show All</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={style.fullWidth}>
                <ScrollView
                  scrollEventThrottle={16}
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                >
                  <View style={style.CardListContainer}>
                    {this.state.trainers.length > 0 ? this.state.trainers.map(item => (

                      <Card
                        imgUri={item.user.photo ? { uri: "http://system.perfitapp.com/images/profile_images/" + item.user.photo }
                          : require("../../../assets/images/Selection/trainer1.png")}
                        name={item.user.name}
                        id={item.id}
                        review={20}
                        category="Weight lose"
                        buttonText="Trainer Details"
                      />
                    )) : null}
                    <SignUpCard
                      imgUri={require("../../../assets/images/Selection/trainer2.png")}
                      name="Karl Dixon"
                      caption="Strength calistenics"
                      trainingLeft={5}
                    />
                  </View>
                </ScrollView>
              </View>
            </View>

            <View style={style.subContainer}>
              <Text style={style.h2Light}>Perfect choice for state</Text>
              <View style={style.flexRow}>
                <View>
                  <Text style={style.h1}>Personal Dietitians</Text>
                </View>
                <View style={style.showAllContainer}>

                  <TouchableOpacity
                  
                  onPress={() =>
                   
                   this.props.navigation.navigate("DietitiansList")
                  }
                  >
                    <Text style={style.showAllText}>Show All</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={style.fullWidth}>
                <ScrollView
                  scrollEventThrottle={16}
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                >
                  <View style={style.CardListContainer}>
                    {this.state.dietician.length > 0 ? this.state.dietician.map(item => (
                      <Card2
                        imgUri={item.user.photo ? { uri: "http://system.perfitapp.com/images/profile_images/" + item.user.photo }
                          : require("../../../assets/images/Selection/Dietitian1.png")}
                        id= {item.id}
                        name={item.user.name}
                        user={item.user}
                        review={20}
                        buttonText="Dietitain Details"
                        category="Weight lose"
                      />
                    )) : null}
                    <SignUpCard
                      imgUri={require("../../../assets/images/Selection/Dietitian2.png")}
                      name="Karl Dixon"
                      caption="Strength calistenics"
                      trainingLeft={5}
                    />
                  </View>
                </ScrollView>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default SelectionMain;