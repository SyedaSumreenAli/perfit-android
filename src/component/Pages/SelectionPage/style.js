import { StyleSheet } from "react-native";
import { width, height, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  arrowContainer: {
    flex: 2,
    //  backgroundColor:"red",
    justifyContent: "center",
    alignItems:"center",
    height: height(14),
  },
  arrowleft: {
    //  backgroundColor:"yellow",
    alignSelf: "center",
    width: width(15),
    fontSize: totalSize(4),
    fontFamily:"Montserrat-Light"
  },
  image: {
    overflow: "hidden",
  },
  logoContainer: {
    flex: 5,
    height: height(30),
    alignItems: "flex-start",
    justifyContent: "center",
  },
  subContainer: {
    justifyContent: "space-between",
    width: width(90),
    //  backgroundColor:"gray",
    alignSelf: "center",
  },
  flexRow: {
    flexDirection: "row",
  },
  h1: {
    fontSize: totalSize(2.8),
    fontFamily: "Montserrat-Bold",
    color: "#000000",
    marginBottom:-height(2)
  },
  h2Light: {
    color: "#000000",
    fontSize: 14.68,
    fontFamily: "Montserrat-Regular",
    // marginTop:-height(2)
  },
  showAllContainer: {
    // backgroundColor:"purple",
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
  },
  showAllText: {
    fontFamily: "Montserrat-Bold",
    fontSize: totalSize(1.6),
    color: "#000000",
  },

  fullWidth: {
    width: width(100),
    // backgroundColor:"red",
    marginLeft: -width(5),
  },
  CardListContainer: {
    flex: 1,
    height: height(30),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  /********** TRAINER/DIETITIAN CARD STYLE *************/
  card: {
    height: height(22),
    backgroundColor: "white",
    width: width(75),
    marginRight: width(4),
    // alignSelf:"center",
    marginLeft: width(5),
    //  overflow:"hidden",
    resizeMode: "contain",
    // flex:1,
    // borderWidth:width(0.1),
    borderRadius: 12,
    flexDirection: "row",
    shadowColor: "#000",
    overflow:"hidden",
    shadowOffset: { width: 15, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 12,
    elevation: 15,
  },
  imgContainer: {
    width: width(30),
    overflow:"hidden",
     backgroundColor:"white",
    justifyContent: "flex-start",
    borderBottomLeftRadius:4
  },
  imgCaptionContainer: {
     position: "absolute",
    //  alignSelf: "center",
    marginLeft:width(3),
    justifyContent: "flex-end",
      backgroundColor:"transparent",
    // flex: 1,
    height: height(20),
  },
  imgCaption: {
    color: "#ffffff",
    fontSize: totalSize(1.4),
    fontFamily: "Montserrat-Medium",
    backgroundColor: "#6D7278",
    paddingHorizontal: width(2),
    borderRadius: 20,
  },
  contentContainer: {
      backgroundColor:"white",
    width: width(40),
    paddingLeft:width(6),
    zIndex:2
  },

  cardTitleContainer: {
    //    backgroundColor:"red",
    width: null,
    flex: 2,
    justifyContent: "center",
    // height:height(6)
    alignItems: "stretch",
  },
  cardTitle: {
    fontFamily: "Montserrat-Medium",
    fontSize: 19,
    color: "#222222",
    //  backgroundColor:"yellow",
    //  padding:width(2)
  },
  reviewContainer: {
    flex: 3,
    // backgroundColor:"red"
  },
  reviewStarContainer: {
    flex: 1,
    height: height(3),
    //  backgroundColor:"green",
    justifyContent: "space-around",
    paddingRight:width(2),
    alignItems: "center",
  },
  reviewStarIcon: {
    fontSize: 10,
    color: "#F9DF02",
  },
  reviewTextContainer: {
    height: height(3),
    // backgroundColor:"green",
    justifyContent: "center",
    alignItems: "center",
  },
  reviewText: {
    fontFamily: "Montserrat-Medium",
    color: "#000000",
    fontSize: 11,
    zIndex: 2,
  },
  cardButtonContainer: {
    flex: 3,
    // backgroundColor:"red",
    // marginLeft:width(2),
    width: width(28),
    justifyContent: "center",
  },
  cardButton: {
    backgroundColor: "#FCB345",
    borderRadius: 20,
    justifyContent: "center",
    alignItems:"center",
    height: height(4.5),
    width:width(30)

  },
  cardButtonText: {
    color: "#ffffff",
    fontFamily: "Helvetica",
    fontSize: totalSize(1.7),
  },
  /********** TRAINER/DIETITIAN CARD STYLE END  *************/

  /********** TRAINER/DIETITIAN SIGN UP CARD STYLE *************/

  linearGradient: {
    // flex: 1,
    // paddingLeft: 45,
    // paddingRight: 45,
    // opacity: 0.9,
    width: width(33),
    borderRadius: 20,
  },
  buttonText: {
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
  },
  flex2: {
    flex: 2,
    marginTop: -height(2),
    //   backgroundColor:"yellow"
  },
  h3Light: {
    fontFamily: "Montserrat-Medium",
    color: "#688893",
    fontSize: 15,
    marginLeft: -3,
  },
  h3Yellow: {
    color: "#F9DF02",
    fontFamily: "Montserrat-Medium",
    fontSize: 15,
  },
  caption: {
    fontSize: 12.0,
    fontFamily: "Montserrat-Medium",
    color: "#688893",
  },
  /********** TRAINER/DIETITIAN SIGN UP CARD STYLE END *************/
});

export default style;
