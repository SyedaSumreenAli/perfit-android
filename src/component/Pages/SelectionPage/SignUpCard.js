import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Button, Icon } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import style from "./style";
import Image from "react-native-scalable-image";
import { width, height } from "react-native-dimension";

const SignUpCard = (props) => {
  return (
    <TouchableOpacity style={style.card}>
      <View style={style.imgContainer}>
        <Image source={props.imgUri} height={height(25)} style={style.image} />
      </View>
      <View style={style.contentContainer}>
        <View style={style.cardTitleContainer}>
          <Text style={style.cardTitle}>{props.name}</Text>
        </View>
        <View style={style.flex2}>
          <View>
            <Text style={style.caption}>{props.caption}</Text>
          </View>
          <View style={style.flexRow}>
            <Text style={style.h3Light}> Training left: </Text>
            <Text style={style.h3Yellow}>{props.trainingLeft}</Text>
          </View>
        </View>

        <View style={style.cardButtonContainer}>
          <Button
            transparent
            //  onPress={this.handleSubmit}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 4 }}
              colors={["#F9DF02", "#F29C1F"]}
              style={style.linearGradient}
            >
              <Text style={style.buttonText}>Sign me up</Text>
            </LinearGradient>
          </Button>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default SignUpCard;
