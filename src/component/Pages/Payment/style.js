import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style= StyleSheet.create({
    container:{
        width:width(100),
        alignItems:"center" 
    },
    bannerTextContainer:{
        // backgroundColor:"yellow",
        height:height(25),
        position:"absolute",
        justifyContent:"flex-end",
        alignSelf:"flex-start",
        paddingLeft:width(8)
    },
    bannerText:{
        color:"white",
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(2.9)
    },
    formContainer:{
        width:width(90),
        alignSelf:"center",
        marginTop:height(4),
        // height:height(60)
    },
    flexRow:{
         flexDirection:"row",
         justifyContent:"space-between"
        // backgroundColor:"orange"
    },
    formItem:{
         width:width(90),
         flex:1,
         borderRadius:width(2),
         borderColor:"#E5E5EA"
    },
    formItemMd:{
        flex:1,
        width:width(30)
    },
    h4:{
        fontFamily:"Montserrat-Bold",
        color:"#000",
        fontSize:totalSize(2),
        marginVertical:height(2)
    },
    h4Light:{
        fontSize:totalSize(1.9),
        color:"#636363",
        fontFamily:"Montserrat-Medium",
        //  width:200,
        // backgroundColor:"yellow",
          flex:1,
         height:height(3),
       
    },
    pl4:{
        paddingLeft:width(4)
    },
    ml3:{
        marginLeft:width(3)
    },
    ml1:{
        marginLeft:width(1)
    },
    mr1:{
        marginRight:width(1)
    },
    ml_2:{
        marginLeft:-width(0.6)
    },
    btn:{
        backgroundColor:"#FCB345",
        width:width(90),
        justifyContent:"center",
        alignSelf:"center",
        alignItems:"center",
        borderRadius:width(20),
        height:height(6),
        marginTop:height(3),
        flex:1
    },
    mb:{
         marginBottom:width(10),
    },
    btnText:{
        color:"#fff",
        textTransform:"uppercase",
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.2)
    },
    /************* check card ************** */

    greenContainer:{
        borderColor:"#10CA88",
        height:height(10),
        width:width(90),
        // marginTop:height(4),
        borderWidth:width(0.4),
        borderRadius:width(2),
        alignItems:"center",
        justifyContent:"space-around",
        backgroundColor:"rgba(16, 202, 136,0.1)"
    },
    checkCardIcon:{
        color:"rgb(16, 202, 136)"
    },
    checkCardNumberContainer:{
        justifyContent:"flex-start",
        alignItems:"flex-start",
        // backgroundColor:"yellow",
        width:width(55)
    },
    h3:{
        
        color:"#26315F",
        fontSize:totalSize(1.8),
        fontFamily:"Montserrat-Medium"
    },
    visaCard:{
        backgroundColor:"#fff",
        width:width(10),
        justifyContent:"center",
        alignItems:"center",
        height:height(4),
        borderRadius:width(1),
        borderTopColor:"#059BBF",
        borderBottomColor:"#F79F1A",
        borderWidth:width(1.5),
        borderLeftWidth:0,
        borderRightWidth:0
    },
    visaText:{
        color:"#059BBF",
        fontFamily:"Montserrat-Bold",
        // fontSize:totalSize(1.5),
        textTransform:"uppercase",
        // fontStyle:"italic"
    },
    addNewCardContainer:{
        // borderColor:"#10CA88",
        height:height(10),
        width:width(90),
        marginTop:height(4),
        // borderWidth:width(0.4),
        // borderRadius:width(2),
        alignItems:"flex-start",
         justifyContent:"center",
        backgroundColor:"#F5F8FB"
    },
    h5:{
        color:"#030100",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.8)
    },
    pLight:{
        color:"#9B9B9B",
        textDecorationLine:"underline",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.7)
    },
    termsContainer:{
        alignItems:"center",
        height:height(10),
        justifyContent:"flex-end"
    },
/*********** payment done ************ */
    h3Green:{
        color:"#10CA88",   
    },
    checkIconO:{
        color:"#10CA88",
        fontSize:totalSize(13),
        marginTop:height(5.5)
    },
    h2:{
        fontSize:totalSize(3),
        fontFamily:"Montserrat-Bold",
        color:"#000",
        textAlign:"center",
        marginTop:height(4)
    },
    p2Light:{
        color:"#AFAFAF",
        fontFamily:"Montserrat-SemiBold",
        marginTop:height(5.5),
        marginBottom:height(12)
    },
    center:{
        alignItems:"center",
     
    }
})

export default style