import React,{Component} from 'react'
import {View, Text} from 'react-native'
import Image from 'react-native-scalable-image'
import { Container, Header, Content, Form, Item, Input, Label, Picker, Icon } from 'native-base';
import style from './style'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {withNavigation} from 'react-navigation'
class Payment extends Component{
    
    state={
        showForm: true,
        checkCard: false,
        paymentDone:false,
        showButton:true
    }
    renderForm = () =>{
        return(
           <View style={style.formContainer}>
               <TouchableOpacity onPress={()=>{this.handleForm()}}>     
                    <Content>
                    <Form>
                        <Text style={style.h4}>Card holder's name</Text>
                        <Item rounded style={[style.formItem]} >
                        <Label style={style.pl4}>
                            <Text style={style.h4Light}>Type the card holder's name</Text></Label>
                        <Input />
                        </Item>
                        <Text style={style.h4}>Card Number</Text>
                        <Item rounded style={style.formItem} last>
                        <Label style={[style.flexRow]}>
                            <Icon  style={[style.h4Light]} name="creditcard" type="AntDesign"/>
                            <Text style={[style.h4Light]}>  Type the card holder's number</Text>
                        </Label>
                        <Input />
                        </Item>
                        <Text style={style.h4}>cvv</Text>
                        <Item rounded style={style.formItem} fixedLabel last>
                        <Label style={style.h4Light}>Type the cvv</Label>
                        <Input />
                        </Item>
                        <Text style={style.h4}>Expiration Date</Text>
                        <View style={style.flexRow}>
                        
                            <Item rounded style={[style.formItemMd, style.mr1]} picker>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down" />}
                                    style={{ width: undefined }}
                                    // placeholder="01"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    // selectedValue={this.state.selected2}
                                    // onValueChange={this.onValueChange2.bind(this)}
                                >
                                    <Picker.Item label="01" value="key0" />
                                    <Picker.Item label="02" value="key1" />
                                    <Picker.Item label="03" value="key2" />
                                </Picker>
                            </Item>

                            <Item rounded style={[style.formItemMd, style.ml1]} picker>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down" />}
                                    style={{ width: undefined }}
                                    placeholder="Select your SIM"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    // selectedValue={this.state.selected2}
                                    // onValueChange={this.onValueChange2.bind(this)}
                                >
                                    <Picker.Item label="2020" value="key0" />
                                    <Picker.Item label="2021" value="key1" />
                                    <Picker.Item label="2022" value="key2" />
                                   
                                </Picker>
                            </Item>
                        </View>
                    </Form>
                    </Content>
              
               </TouchableOpacity>
               
           </View>
        )
    }
    renderCheckCard = () =>{
        return(
           <View>
                    <Text style={style.h4}>Registered Card </Text>
                 <View style={[style.flexRow, style.greenContainer]}>
                    <View style={style.visaCard}>
                        <Text style={style.visaText}>Visa</Text>
                    </View>
                    <View style={style.checkCardNumberContainer}>
                        <Text style={style.h3}>**** **** **** 1234 </Text>
                    </View>
                    <View>
                        <Icon style={style.checkCardIcon} name="checkcircle" type="AntDesign"/>
                    </View>
                 </View>
                 <View style={style.addNewCardContainer}>
                     <Text style={[style.h5, style.ml3]}>Add New Card</Text>
                 </View>
                 <View style={style.termsContainer}>
                     <Text style={style.pLight}>View terms and conditions</Text>
                 </View>
           </View>
        )
    }
    renderPaymentDone = () =>{
        return(
           <View>
               <View style={style.center}>
                  <Icon name="check-circle" type="FontAwesome5" style={style.checkIconO} />
                  <Text style={style.h2}>Your Payment is {'\n'} successfully.</Text>
                  <Text style={style.p2Light}>A confirmation Email has been sent to you </Text>
               </View>
                  <TouchableOpacity 
                  style={[style.flexRow, style.greenContainer]}
                  onPress={()=>{this.props.navigation.navigate("StrengthTraining")}}
                  >

                    <View style={style.checkCardNumberContainer}>
                        <Text style={style.h3Green}>Go to my subscription</Text>
                    </View>
                    <View>
                        <Icon style={style.checkCardIcon} name="checkcircle" type="AntDesign"/>
                    </View>
                 </TouchableOpacity>
           </View>
        )
    }
    renderPayButton = () => {
       return(
           <View>
            <TouchableOpacity style={style.btn} 
              onPress={()=>{this.handlePay()}}
            >
            <Text style={style.btnText}>Pay</Text>
        </TouchableOpacity>

           </View>
       )
    }
    handlePay = () =>{
        const { checkCard, paymentDone, showButton } = this.state
        this.setState({
            checkCard: false,
            showForm:false,
            paymentDone:true,
            showButton:false
        })
    }
    handleForm=()=>{
        const { showForm, checkCard} = this.state
        this.setState({
            showForm:!showForm,
            checkCard:!checkCard
        })
    }
    render(){
        const {showForm, checkCard, paymentDone, showButton} = this.state
        let _render =   showForm ? this.renderForm() : 
                        checkCard ? this.renderCheckCard() :
                        this.renderPaymentDone()
        let _renderButton = showButton ? this.renderPayButton(): null
        return(
            <View style={style.container}>
               <Image source={require('../../../assets/images/payment/1.png')}/>
               <View  style={style.bannerTextContainer}>
                   <Text style={style.bannerText}>Payment Detials</Text>
               </View>
                {_render}
                <View style={style.mb}>

                {_renderButton}
                </View>
                   
            </View>
        )

    }
}
export default withNavigation(Payment)