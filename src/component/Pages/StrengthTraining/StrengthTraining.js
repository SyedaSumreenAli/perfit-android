import React, {Component} from 'react'
import {View, Text, TouchableOpacity,Image} from 'react-native'
import {Icon} from 'native-base'
//import Image from 'react-native-scalable-image'
import Tab from './Tab/Tab'
import style from './style'
import { height, width } from 'react-native-dimension'

class StrengthTraining extends Component{

    static navigationOptions = {
        headerShown: false,
      };

    render(){
  
    return(
        <View>
             <View style={style.flexRow}>
                    <TouchableOpacity style={style.iconContainer} 
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Icon name="arrowleft" type="AntDesign" style={style.icon}/>
                    </TouchableOpacity>
                    <View style={style.flex4} >
                        <View style={style.headerContainer}>
                            <Text style={style.headerText}>PT</Text>
                            </View>
                        </View>
             </View>
            <View>
                <View style={style.imgContainer}>
                <Image 
                // height={height(30)}
                
                // width={width(100)}
                source={require('../../../assets/images/personalTraining/training-rectangle.png')}/>
                </View>
               
                <View style={style.maskContainer}>

                    <Image 
                       // height={height(30)}
               
                // width={width(100)+width(10)}
                // width={width(100)}
                style={style.mask}
                source={require('../../../assets/images/personalTraining/mask.png')}/>
                </View>
                    
               
            </View>
            <View style={style.bannerTextContainer}>
              <View>
                  <Text style={style.h1}>Strength Training </Text>
            </View>
              <View style={style.flexRow}>
              <View style={[style.flexRow, style.dateContainer]}>
                    <View  style={[style.borderRight,  style.p3nl]}>
                        <View style={style.flexRow}>
                            <Icon name="date" type="Fontisto" style={[style.dateIcon ,style.whiteText]}/>
                            <Text style={[style.dateText ,style.whiteText]}>12/04/2020</Text>
                        </View>
                    </View>          
                        <View style={style.flexRow}>
                            <Icon name="date" type="Fontisto" style={[style.dateIcon, 
                                style.whiteText]}/>
                            <Text style={[style.dateText, style.whiteText]}>10:30 - 12:00 AM</Text>
                        </View>
                    
                </View>
              </View>
            </View>


        
            <View style={style.tabContainer}>
                <Tab/>
            </View>
        </View>
    )
          
    }
}

export default StrengthTraining