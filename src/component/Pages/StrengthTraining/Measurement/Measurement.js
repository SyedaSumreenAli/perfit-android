import React ,{Component} from 'react'
import {View, Text, ScrollView} from 'react-native'
import {Button} from 'native-base'
import LinearGradient from "react-native-linear-gradient";
import style from './style'
import Image from 'react-native-scalable-image'
import {withNavigation } from 'react-navigation'
import { TouchableOpacity } from 'react-native-gesture-handler';

class Measurement extends Component{
    render(){
        return(
            <ScrollView>
                <View style={style.container}>
                    <View style={style.imgContainer}>
                        <Image source={require('../../../../assets/images/measurement/body2.png')}/>    
                    </View>
                    <TouchableOpacity transparent 
                    onPress={()=>{this.props.navigation.navigate("UpdateMeasurement")}}
                    >
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 4 }}
                        colors={["#efcc64", "#FFAA2F"]}
                        style={style.linearGradient}
                    >
                        <Text style={style.buttonText}>Add Measurement</Text>
                    </LinearGradient>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        )
    }
}

export default withNavigation(Measurement) 