import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    flexRow: { flexDirection: "row" },
    iconContainer: {
      flex: 6,
      //  backgroundColor:"orange",
      height: height(10),
      justifyContent: "center",
    },
    icon: {
      // backgroundColor:"green",
      width: width(30),
      alignSelf: "center",
      fontSize: 35,
    },
    flex4: {
      flex: 7,
      alignItems: "flex-start",
      justifyContent: "center",
      //  backgroundColor:"green",
    },
    headerText: {
      // backgroundColor:"red",
      flexDirection: "column",
      color: "#424242",
      fontSize: 20,
      fontFamily: "Montserrat-SemiBold",
    },
    absolute:{
      position:"absolute",
    },
    mask:{
      // opacity:0.5,
      // // flex:1,
      // width:width(100)+width(40),
      //  flex:1,
      // flex:1,
       width:width(100)+100,
       height:height(30),
       marginLeft:-width(20),
      justifyContent:"flex-start",
      // alignSelf:"flex-start"

    },
    imgContainer:{
      flex:1,
      position:"absolute",
      width:width(100),
      height:height(32)
    },
    maskContainer:{
      // flex:1,
      width:width(100),
       opacity:1,
      // height:height(30),
     overflow:"hidden",
     position:"absolute"
    },
   h1:{
     color:"#FCB345",
     fontSize:totalSize(3.2),
     fontFamily:"Montserrat-Bold"
   },
   bannerTextContainer:{
      // backgroundColor:"blue",
      // opacity:0.2,
     height:height(26),
     paddingBottom:height(4),
     width:width(96),
     alignSelf:"flex-end",
     justifyContent:"flex-end"
    },
    whiteText:{
      color:"white",
      fontFamily:"Montserrat-Regular",
      alignSelf:"center"
    },
    dateIcon:{
      fontSize:totalSize(1.5),
      marginRight:width(2),
      // justifyContent:"center",
     
    },
    dateContainer:{
      //  backgroundColor:"green",
      // flex:1,
      // width:width(40),
      justifyContent:"space-between"
    },
    borderRight:{
      borderRightColor:"white",
      borderWidth:width(0.5),
      marginRight:width(2),
      paddingRight:width(1),
      borderTopColor:"transparent",
      borderLeftColor:"transparent",
      borderBottomColor:"transparent"
    }
    
});

export default style