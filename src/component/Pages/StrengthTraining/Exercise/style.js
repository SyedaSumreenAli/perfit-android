import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    container:{
         flex:1,
          marginTop:height(2),
         height:height(100),
        //  marginBottom:height(10)
        // backgroundColor:"skyblue"
      },
    weekSlider:{
        backgroundColor:"#FDC14A",
        flexDirection:"row",
        justifyContent:"space-around",
        alignItems:"center",
        height:height(6)
    },
    whiteText:{
        color:"white",
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.5)
    },
    daysContainer:{
        backgroundColor:"#F7F5F5",
        height:height(10),
        paddingHorizontal:width(12),
        flexDirection:"row",
        justifyContent:"space-around",
        alignItems:"center"
    },
    dayButton:{
        backgroundColor:"transparent",
        borderRadius:50,
        width:width(10),
        height:width(10),
        justifyContent:"center",
        alignItems:"center"
    },
    dayText:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.6)
    },
    currentDay:{
        alignSelf:"center",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.4)
    },
    currentDayContainer:{
        //   flex:1,
    //  
    
    // backgroundColor:"yellow",
        justifyContent:"center",
        alignItems:"center",
        height:height(6),
        marginBottom:-height(10)
    },

    h2Container:{
        // backgroundColor:"yellow",
        //   flex:1,
        //   height:height(10),
         
        //   flex:1,
        marginTop:-height(4),
        marginBottom:height(4),
         justifyContent:"flex-end",
         paddingBottom:height(1.5),
         borderWidth:width(0.2),
         borderBottomColor:"#D8D8D8",
         borderLeftColor:"transparent",
         borderRightColor:"transparent",
         borderTopColor:"transparent"
    },
    exerciseContainer:{
        //    backgroundColor:"red",
           marginBottom:height(2),
        //  height:height(12),
        //  flex:4,
        flexDirection:"row",
        // position:"absolute"
         alignItems:"center"
       
    },
    exerciseContainer1:{
        //    position:"absolute",
            // backgroundColor:"orange",
           height:height(100),
        //    marginBottom:height(20),
        //  justifyContent:"flex-start",
        width:width(90),
        alignSelf:"center",
         flex:1,
         marginTop:-height(75)
    },
    exImgContainer:{
          flex:2,
        // backgroundColor:"red",   
        alignItems:"center",
        // backgroundColor:"yellow",
    },
    youtubeIconContainer:{
        position:"absolute",
        height:width(15),
        overflow:"hidden",
        justifyContent:"center",
        alignItems:"center"
    },
    
    youtubeIcon:{
        color:"red", 
        alignSelf:"center"
    },
    checkIconContainer:{
        position:"absolute",
        height:width(6),
        // backgroundColor:"yellow",
        width:width(10),
        // opacity:0.5,
        overflow:"hidden",
        justifyContent:"center",
        alignItems:"flex-start"
    },
    
    checkIcon:{
        color:"white",
        fontSize:totalSize(2)
         
        
    },
    textContainer:{
        flex:5,
        height:height(8),
        justifyContent:"center",
        flexDirection:"row",
          borderWidth:width(0.2),
         borderColor:"transparent",
         borderBottomColor:"#d8d8d8",
        //  marginLeft:width(5), 
        //  paddingBottom:height(2)
    },
    exDescriptionContainer:{
         flex:3,
        
        // backgroundColor:"yellow"
    },
    exSwtichContainer:{
         flex:2,
         alignItems:"flex-end",
        //  borderWidth:width(0.2),
        //  borderColor:"transparent",
        //  borderBottomColor:"#d8d8d8"
        //  height:height(10),
        //  backgroundColor:"skyblue"
    },
    descriptionText:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.2)
    },
    image:{
        width:width(20),
        height:width(16),
        borderRadius:5,
        // marginRight:width(5),
        paddingRight:width(2)
    },
    h2:{
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(2)
    },
    loaderContainer:{
        // backgroundColor:"skyblue",
        // flex:2,
        height:height(50),
        justifyContent:"center",
        alignItems:"center"
    },
    loader:{
        color:"#FCB344"
    }
})

export default style