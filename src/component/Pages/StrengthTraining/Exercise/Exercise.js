import React ,{Component} from 'react'
import {View, Text, TouchableOpacity, Image} from 'react-native'
// import Image from 'react-native-scalable-image'
import {Icon, Button, Item, Spinner} from "native-base"
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import ToggleSwitch from 'toggle-switch-react-native'
import { width } from 'react-native-dimension'
import {withNavigation} from 'react-navigation'
import {getAllExercises} from '../../../../redux/actions/AllExercisesActions'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {BASE_URI} from '../../../../redux/api'

class Exercise extends Component{
    state={
        days:[],
      
        currentDay:"Monday"
    }
    loadDays = () => {
        let days=[
            {
                day:"Sunday",
                label:"S",
            },
            {
                day:"Monday",
                label:"M",
            },
            {
                day:"Tuesday",
                label:"T",
            },
            {
                day:"Wednesday",
                label:"W",
            },
            {
                day:"thursday",
                label:"T",
            },
            {
                day:"Friday",
                label:"F",
            },
            {
                day:"Saturday",
                label:"S",
            },
           
        ];

        this.setState({ days })
    }

   
    renderDays=()=>{
        const {days, currentDay} = this.state

         let day =  days.map((day, index) =>{
                  return  <TouchableOpacity 
                            key={index}
                            style={[style.dayButton,{
                                backgroundColor: currentDay== day.day ? "#979797" : "transparent",
                            }]}
                            
                            onPress={ () => { this.handleCurrentDay(day.day)}}
                            >
                        <Text  style={[style.dayText, {
                            color: currentDay == day.day ? "white": "#000"
                        }]}>{day.label}</Text>
                    </TouchableOpacity>
            })

            return day
        
    }
    componentDidMount= async()=>{
        return( 
            await  this.props.getAllExercises(),
            await  this.loadDays(),

         //   await console.log(this.props),
           console.log(this.props.loading)
          
        )
     }
    renderExercisePlan = () => {

        const exercisePlan = this.props.workouts.workouts || null
        console.log(this.props.loading)
        const loading = this.props.loading
        console.log('////////////////////////////'+exercisePlan.length)
        let exercises =  exercisePlan.map(item=>{
            let count= exercisePlan.length

            let image = item.media!==null?`${BASE_URI}workouts/images/${item.media.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')}`:'nullcjurl'
            image =image.replace(/\"?/g, "")
            image =image.replace(/\[?/g, "")
            image =image.replace(/\]?/g, "")
            console.clear()
            console.log(image)
            return(
                  <View>
                       
                        <View style={style.exerciseContainer}>
                            <TouchableOpacity 
                            style={style.exImgContainer}
                            onPress={() => {this.props.navigation.navigate("ExerciseVideo")}}
                            >
                        <Image
                            style={style.image}
                            source={ item.image? {uri:image} : require("../../../../assets/images/data/pt1.png") }                   
                         />
                        <View style={style.youtubeIconContainer}>
                            <Icon name="youtube" type="AntDesign" style={style.youtubeIcon}/>

                        </View>
                    </TouchableOpacity>
                    <View style={[
                        style.textContainer, {

                            borderBottomColor: count <=
                             exercisePlan.length-1 ? 'transparent' :  '#d8d8d8'
                        }
                    ]}>
                    <View style={style.exDescriptionContainer}>
                        <Text style={style.descriptionText}
                          numberOfLines={2}>
                               {/* {item.name.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')} */}
                              {item.description.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')}
                              </Text>
                    </View>
                    <View style={style.exSwtichContainer}>
                     
                    <ToggleSwitch
                        isOn={true}
                        onColor="#2FD475"
                        offColor="#B2B2B2"
                        size="medium"
                        style={{width:width(20)}}
                        onToggle={isOn => console.log("changed to : ", isOn)}
                        />
                       <View style={style.checkIconContainer}>
                            <Icon name="check" type="AntDesign" style={style.checkIcon}/>

                        </View>  
                     </View>   
                    
                    </View>
                </View>
                </View> 
           
                )
                count++
                
        })
           
        return exercises

    }

   

    handleCurrentDay= (day) => {
        const {currentDay} = this.state
        this.setState({currentDay: day})
    }
 
    render(){
        const {currentDay} = this.state
        let content =  this.props.loading ? 
        <View  style={style.loaderContainer}>
            <Spinner large color={style.loader.color} />
        </View>:
        <View>
            <View style={style.container}>
                <View style={style.weekSlider}>
                    <View>
                        <Icon style={style.whiteText} name="left" type="AntDesign"/>
                    </View>
                    <View>
                        <Text  style={style.whiteText} > Week 1</Text>
                    </View>
                    <View>
                        <Icon  style={style.whiteText}  name="right" type="AntDesign"/>
                    </View>
                </View>

                <View style={style.daysContainer}>
                    {this.renderDays()}     
                </View>
                <View style={style.currentDayContainer}>
                    <Text style={style.currentDay}>{currentDay}</Text>
                </View>
            </View>
            <View style={style.exerciseContainer1}>
                     <View style={style.h2Container}>
                            <Text style={style.h2}>Morning Plan

                               {/* {item.name.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')} */}
                                {/* {item.tags.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')} */}
                         </Text>
                        </View> 
            {this.renderExercisePlan()}
            </View>
        </View>

        return(
            <ScrollView>
                {content}
            </ScrollView>
        )
    }
}

Exercise.PropTypes= {
    getAllExercises: PropTypes.func.isRequired,
   workouts:PropTypes.object.isRequired
  }
  
   const mapStateToProps = (state) => {
       console.log(state)
    return {
      workouts: state.AllExercisesReducer.workouts,
      loading: state.AllExercisesReducer.loading,
      error: state.AllExercisesReducer.errorMessage
    }
  }

export default withNavigation(connect(mapStateToProps, {getAllExercises})(Exercise));