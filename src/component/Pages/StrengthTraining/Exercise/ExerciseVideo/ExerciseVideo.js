import React, { Component } from 'react'
import { 
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Slider,
  Alert} from 'react-native'

import Image from 'react-native-scalable-image'
import Video from 'react-native-video';

import style from './style'
import { width, height } from 'react-native-dimension'

import MediaControls, {PLAYER_STATES} from 'react-native-media-controls'


class ExerciseVideo extends Component{
  videoPlayer;
   state={
   
    currentTime: 0,
    duration: 0,
    isFullScreen: false,
    isLoading: true,
    paused: false,
    playerState: PLAYER_STATES.PLAYING,
    screenType: "content"
    }


  
static navigationOptions = {
  headerShown: false
}
  
onSeek = () => {

  this.videoplayer.seek(seek)

}


onPaused = (playerState) => {

  this.setState({

    paused: !this.state.paused,
    playerState
  })

}

onReplay = () => {

  this.setState({

    playerState: PLAYER_STATES.PLAYING
  })

  this.videoplayer.seek(0)
}

onProgress = (data) => {


  const { isLoading, playerState } = this.state

  if (!isLoading && playerState !== PLAYER_STATES.ENDED) {

    this.setState({
      currentTime: data.currentTime
    })
  }

}

onLoad = (data) => {

  this.setState({
    duration: data.duration,
    isLoading: false
  })
}

onLoadStart = (data) => {

  this.setState({

    isLoading: true
  })

}

onError = (error) => {

  Alert.alert(error)
}

exitFullScreen = () => {

  Alert.alert("Exit Full Screen")
}

enterFullScreen = () => {

  if (this.state.screenType == "content") {

    this.state({ screenType: "cover" })

  } else {

    this.setState({ screenType: "content" })
  }
}

renderToolbar = () => {

  <View style={styles.toolbar}>
    <Text>Toolbar</Text>
  </View>
}

onSeeking = (currentTime) => {

  this.setState({

    currentTime
  })

}


    render(){
      // let {description, video} = this.props.navigation.state.params
      // console.log(this.props.navigation.state.params)
        return(
            <View style={style.body}>
                <View style={style.header}>
                   <TouchableOpacity>
                       <Image source={require('../../../../../assets/images/exercises/fullscreen-exit.png')}/>
                   </TouchableOpacity>
                   <TouchableOpacity 
                      onPress={()=>{this.props.navigation.goBack()}}
                   >
                       <Image source={require('../../../../../assets/images/exercises/close.png')}/>
                   </TouchableOpacity>
                </View> 
               <View>
             <TouchableOpacity
                onPress={() => this.setState({paused: !this.state.paused})}
                style={style.videoContainer}>
              <View style={{flex:1}}>
              <Video
                  controls={true}
                  paused={false} 
                  source={{uri:"https://www.w3schools.com/html/mov_bbb.mp4"}}
                  //source={require("./assets/test.mp4")}
                  style={styles.mediaPlayer}
                  volume={13}>
              </Video>
            
                </View>
             </TouchableOpacity>
               </View>
              
              <Text style={style.text}>Chest – Barbell Bench Press – 4 sets of 8 reps.</Text>
            </View>
        )
    }
}



  const styles=StyleSheet.create({


    mediaPlayer:{

      flex:1,
    }
  })

export default ExerciseVideo