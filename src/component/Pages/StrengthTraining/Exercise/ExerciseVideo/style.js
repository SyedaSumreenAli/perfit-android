import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    body:{
        backgroundColor:"#000",
        flex:1
    },
    header:{
        flexDirection:"row",
        justifyContent:"space-between",
        marginHorizontal:width(6),
        marginTop:height(4)
    },
    icon:{
        color:"#fff"
    },
    text:{
        color:"#fff",
        fontFamily:"Montserrat-Medium",
        textAlign:"center",
        fontSize:totalSize(1.8)
    },
    videoContainer:{
        // flex:1,
        marginTop:height(4),
        marginBottom:height(4),
        height:height(40)
    },
    mediaPlayer:{
        // flex:1,
        // position:"absolute",
        // backgroundColor:"black",
        // top:0,
        // bottom:0,
        // left:0,
        // right:0 ,
        width:width(100),
        height:height(40)    
    },
    toolbar:{
        marginTop:30,
        backgroundColor:'white',
        padding:10,
        borderRadius:5
    }
    
});

export default style