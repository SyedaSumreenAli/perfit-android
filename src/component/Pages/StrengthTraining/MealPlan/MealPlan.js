import React ,{Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import Image from 'react-native-scalable-image'
import {Icon, Button, Item} from "native-base"
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import ToggleSwitch from 'toggle-switch-react-native'
import {withNavigation} from 'react-navigation'
import { width } from 'react-native-dimension'

class MealPlan extends Component{
    state={
        days:[],
        mealPlan:[],
        currentDay:"Monday"
    }
    loadDays = () => {
        let days=[
            {
                day:"Sunday",
                label:"S",
            },
            {
                day:"Monday",
                label:"M",
            },
            {
                day:"Tuesday",
                label:"T",
            },
            {
                day:"Wednesday",
                label:"W",
            },
            {
                day:"thursday",
                label:"T",
            },
            {
                day:"Friday",
                label:"F",
            },
            {
                day:"Saturday",
                label:"S",
            },
           
        ];

        this.setState({ days })
    }

    loadMealPlan = () => {
        let mealPlan = [
            {
                title:"Break Fast",
                meal:[
                   { name:"Egg Muffins",
                    description:"Research has shown that protein before a workout can help enhance protein synthesis ",
                    imageUri: require('../../../../assets/images/MealPlan/1.png'),
                   
                   },
                   { name:"Greak salad.",
                     description:"Place the cucumber, peppers, tomatoes and red onion in a large bowl. For the vinaigrette.",
                    imageUri:require('../../../../assets/images/MealPlan/2.png'),
                 
                   },
                   
            ]
            },
            {
                title:"Lunch",
                meal:[
                   { name:"Roasted chicken",
                   description:"Silky tomato soup is like the little black dress of soups. Unadorned and paired with a grilled cheese sandwich.",
                    imageUri: require('../../../../assets/images/MealPlan/1.png'),
                   
                   },
            ]
            }
        ]

        this.setState({
           mealPlan
        })
    }


  
    renderDays=()=>{
        const {days, currentDay} = this.state

         let day =  days.map((day, index) =>{
                  return  <TouchableOpacity 
                            key={index}
                            style={[style.dayButton,{
                                backgroundColor: currentDay== day.day ? "#979797" : "transparent",
                            }]}
                            
                            onPress={ () => { this.handleCurrentDay(day.day)}}
                            >
                        <Text  style={[style.dayText, {
                            color: currentDay == day.day ? "white": "#000"
                        }]}>{day.label}</Text>
                    </TouchableOpacity>
            })

            return day
        
    }

    renderMealPlan = () => {
        const {mealPlan} = this.state
        
        console.log(mealPlan)
        let meal = mealPlan.map(item=>{
            console.log(item.title)
            if(item.title ==''||null)
                return ''
            else{ 
                 return <View>
                <View style={style.h2Container}>
                    <Text style={style.h2}>{item.title}</Text>
                </View>
                { item.meal.map((sub, index) => {
                    console.log(item.meal.length)
                   return(
                     <View style={style.mealContainer}>
                        <TouchableOpacity 
                        style={style.exImgContainer}
                        onPress={()=>{this.props.navigation.navigate("Recipe")}}
                        
                        >
                            <Image source={sub.imageUri} width={width(15)}/>
                            
                        </TouchableOpacity>
                        <View style={[
                            style.textContainer,
                            {
                                borderBottomColor: item.meal.length-1 > index ? "#d8d8d8" : "transparent",        
                           }

                        ]}>
                        <View style={style.exDescriptionContainer}>
                            <Text style={style.h3} >{sub.name}</Text>
                            <Text style={style.p}>{sub.description}</Text>
                        </View>
                     
                        
                        </View>
                    </View> )
                }
            )}
               </View>
              
            
        }})

        return meal

    }

   componentDidMount= async()=>{
       return( 
          await  this.loadDays(),
          await  this.loadMealPlan()
       )
    }

    handleCurrentDay= (day) => {
        const {currentDay} = this.state
        this.setState({currentDay: day})
      
    }
 
    render(){
        const {currentDay} = this.state
        return(
            <ScrollView>
            <View style={style.container}>
                <View style={style.weekSlider}>
                    <View>
                        <Icon style={style.whiteText} name="left" type="AntDesign"/>
                    </View>
                    <View>
                        <Text  style={style.whiteText} > Week 1</Text>
                    </View>
                    <View>
                        <Icon  style={style.whiteText}  name="right" type="AntDesign"/>
                    </View>
                </View>

                <View style={style.daysContainer}>
                    {this.renderDays()}     
                </View>
                <View style={style.currentDayContainer}>
                    <Text style={style.currentDay}>{currentDay}</Text>
                </View>
            </View>
            <View style={style.exerciseContainer1}>
                
              {this.renderMealPlan()}
            </View>
            </ScrollView>
        )
    }
}

export default withNavigation(MealPlan)