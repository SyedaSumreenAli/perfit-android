import React ,{Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import Image from 'react-native-scalable-image'
import {Icon, Button, Item} from "native-base"
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import ToggleSwitch from 'toggle-switch-react-native'
import { width } from 'react-native-dimension'

class Exercise extends Component{
    state={
        days:[],
        exercisePlan:[],
        currentDay:"Monday"
    }
    loadDays = () => {
        let days=[
            {
                day:"Sunday",
                label:"S",
            },
            {
                day:"Monday",
                label:"M",
            },
            {
                day:"Tuesday",
                label:"T",
            },
            {
                day:"Wednesday",
                label:"W",
            },
            {
                day:"thursday",
                label:"T",
            },
            {
                day:"Friday",
                label:"F",
            },
            {
                day:"Saturday",
                label:"S",
            },
           
        ];

        this.setState({ days })
    }

    loadExercisePlan = () => {
        let exercisePlan = [
            {
                title:" Morning",
                exercises:[
                   {
                     description:"Ultimate Nutrition Prostar 100% Whey Protein",
                     caption:" This Product Helps in Muscle Building",
                     weight:"5.28lb",
                     imageUri: require('../../../../assets/images/supplements/1.png'),
                   
                   },
                   {
                    description:"MuscleBlaze Super Gainer XXL",
                   caption:" This Product Helps in Muscle Building",
                   weight:"1lb",
                   imageUri: require('../../../../assets/images/supplements/1.png'),
                 
                   },
            ]
            },
            {
                title:"Pre-workout",
                exercises:[
                    {
                        description:"Ultimate Nutrition Prostar 100% Whey Protein",
                        caption:" This Product Helps in Muscle Building",
                        weight:"5.28lb",
                        imageUri: require('../../../../assets/images/supplements/1.png'),    
                       }
            ]
            }
        ]

        this.setState({
           exercisePlan
        })
    }


  
    renderDays=()=>{
        const {days, currentDay} = this.state

         let day =  days.map((day, index) =>{
                  return  <TouchableOpacity 
                            key={index}
                            style={[style.dayButton,{
                                backgroundColor: currentDay== day.day ? "#979797" : "transparent",
                            }]}
                            
                            onPress={ () => { this.handleCurrentDay(day.day)}}
                            >
                        <Text  style={[style.dayText, {
                            color: currentDay == day.day ? "white": "#000"
                        }]}>{day.label}</Text>
                    </TouchableOpacity>
            })

            return day
        
    }

    renderExercisePlan = () => {
        const {exercisePlan} = this.state
        
        console.log(exercisePlan)
        let exercises = exercisePlan.map(item=>{
            console.log(item.title)
            if(item.title ==''||null)
                return ''
            else{ 
                 return <View>
                <View style={style.h2Container}>
                    <Text style={style.h2}>{item.title}</Text>
                </View>
                { item.exercises.map((sub, index) => {
                   return(
                     <View style={style.exerciseContainer}>
                        <View style={style.exImgContainer}>
                            <Image source={sub.imageUri} width={width(15)}/>
                        </View>
                        <View style={[style.textContainer,
                        {
                            borderBottomColor: item.exercises.length-1 > index ? "#d8d8d8" : "transparent",        
                        }]}>
                        <View style={style.exDescriptionContainer}>
                            <Text style={style.h1}>{sub.description}</Text>
                            <Text style={style.p}>{sub.caption}</Text>
                        </View>
                        <View style={style.exSwtichContainer}>
                            <Text style={style.weightText}> Weight: {sub.weight}</Text> 
                         </View>   
                        
                        </View>
                    </View> )
                }
            )}
               </View>
              
            
        }})

        return exercises

    }

   componentDidMount= async()=>{
       return( 
          await  this.loadDays(),
          await  this.loadExercisePlan()
       )
    }

    handleCurrentDay= (day) => {
        const {currentDay} = this.state
        this.setState({currentDay: day})
    }
 
    render(){
        const {currentDay} = this.state
        return(
            <ScrollView>
            <View style={style.container}>
                <View style={style.weekSlider}>
                    <View>
                        <Icon style={style.whiteText} name="left" type="AntDesign"/>
                    </View>
                    <View>
                        <Text  style={style.whiteText} > Week 1</Text>
                    </View>
                    <View>
                        <Icon  style={style.whiteText}  name="right" type="AntDesign"/>
                    </View>
                </View>

                <View style={style.daysContainer}>
                    {this.renderDays()}     
                </View>
                <View style={style.currentDayContainer}>
                    <Text style={style.currentDay}>{currentDay}</Text>
                </View>
            </View>
            <View style={style.exerciseContainer1}>
                
              {this.renderExercisePlan()}
            </View>
            </ScrollView>
        )
    }
}

export default Exercise