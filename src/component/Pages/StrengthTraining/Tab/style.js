import {StyleSheet} from 'react-native'
import {width , height, totalSize} from 'react-native-dimension'


const style = StyleSheet.create({

 
tabStyle:{
      backgroundColor:"white",
      height:height(6),
      borderBottomWidth: 0
  }  ,

  activeTabStyle:{
      backgroundColor:"white",
      height:height(6)
  },
  activeTabTextStyle:{
      color:"#000",
      fontFamily:"Montserrat-SemiBold",
      fontSize: totalSize(2)
  },
  tabTextStyle:{
      color:"#AEAEAE",
      fontFamily:"Montserrat-SemiBold",
      fontSize: totalSize(2)
  },
  underlineStyle:{
   height:height(1),
    backgroundColor:"#FCB345",
    // borderColor:"white",
    // borderWidth: 0
    //  marginTop:height(3)
  },
  tabContainerStyle:{
    height:height(10),
    justifyContent:"flex-end",
    alignItems:"flex-end",
    backgroundColor:"red",
    // marginBottom:height(6),
    // borderBottomColor:"white",
   
  },

  tabContainer:{
    position:"absolute",
    backgroundColor:"white"
   
  }

})

export default style