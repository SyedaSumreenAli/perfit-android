import React, { Component } from 'react'
import {ScrollView,View,Text, TouchableOpacity} from 'react-native'
import Image from 'react-native-scalable-image'
import{Icon, Badge} from 'native-base'
import style from './style'


class Recipe extends Component{
    static navigationOptions={
        headerShown : false
    }
    render(){
        return(
            <ScrollView>
                <View style={style.body}>
                    <Image 
                    source={require('../../../../assets/images/MealPlan/eggmuffins.png')}
                    style={style.bannerImg}
                    />
                    <TouchableOpacity style={style.crossIconContainer}
                     onPress={()=>{this.props.navigation.goBack()}}
                    >
                    <Image source={require("../../../../assets/images/MealPlan/cross.png")}/>
                    </TouchableOpacity>
                    <View style={style.contentContainer}>
                    <View style={style.content}>
                        <View style={style.flexRow}>
                            <Text style={style.h1}> Egg Muffins</Text>
                            <View>
                                <Badge style={style.badge}>
                                    <Text style={style.badgeText}> Breakfast</Text>
                                </Badge>
                            </View>
                        </View>
                        <View>
                            <Text style={style.descriptionText}>Research has shown that protein before a workout can help enhance protein synthesis  </Text>
                        </View>
                        <View>
                            <Text style={style.h2}>You'll need:</Text>
                        </View>
                        <View style={style.card}>
                            <Text style={style.p} >- 8 eggs</Text>
                            <Text style={style.p} >- Cooked ham or bacon (for an extra protien hit)</Text>
                            <Text style={style.p} >- Diced pepper</Text>
                            <Text style={style.p} >- Diced onion </Text>
                            <Text style={style.p} >- Salt </Text>
                            <Text style={style.p} >- Pepper</Text>
                        </View>
                        <View>
                            <Text style={style.h2}>How to do it:</Text>
                        </View>
                        <View style={style.card}>
                            <Text style={style.p}>- The night before your session, pre-heart your oven to 180 and grease your muffin tray.</Text>
                            <Text style={style.p}>- Beat the eggs together, mix your ingredients in and pour into the muffin tray.</Text>
                            <Text style={style.p}>- Bake for around 20 minutes until the muffins are set in the middle.</Text>
                            <Text style={style.p}>- The following morning, heat for 2-3 minutes in the microwave. Voila.</Text>
                        </View>                        
                    </View>
                    </View>

                </View>
            </ScrollView>
        )
    }
}

export default Recipe