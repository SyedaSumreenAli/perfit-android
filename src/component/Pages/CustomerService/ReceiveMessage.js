import React from 'react'
import {View, Text} from 'react-native'
import {Icon} from "native-base"
import style from './style'

const ReceiveMessage = (props) => {
    return(
       <View style={style.receiveMessageOuterContainer}>
           <View style={style.receiveMessageInnerContainer}>
                <Text style={style.receiveMessageText}>{props.message}</Text>
           </View>
           <View style={[style.flexRow, style.flexEnd]}>
                <Text style={style.receiveMessageTime}>{props.time}</Text>
                <Icon name="check-all" type="MaterialCommunityIcons" style={style.seen}/>
           </View>
       </View>
    )
}
export default ReceiveMessage