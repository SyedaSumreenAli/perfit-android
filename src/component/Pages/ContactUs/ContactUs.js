import React from "react";
import { ScrollView, View } from "react-native";
import {
  Text,
  Button,
  Label,
  Icon,
  Content,
  Form,
  Item,
  Input,
  Textarea,
} from "native-base";
import style from "./style";
import LinearGradient from "react-native-linear-gradient";

export const TrainerContact = ({ navigation }) => {
  return (
    <ScrollView>
      <View style={style.container}>
        <View style={style.flexRow}>
          <View style={[style.flex5, style.pb5]}>
            <Text style={style.uppercaseH1}>Contact Us</Text>
          </View>
          <View style={style.flex1}>
            <Button
              transparent
              onPress={() => navigation.navigate("TrainerLogin")}
            >
              <Icon
                name="circle-with-cross"
                type="Entypo"
                style={style.crossIcon}
              />
            </Button>
          </View>
        </View>

        <View>
          <Text style={style.h2}>Question, Comments, or Concerns?</Text>
        </View>

        <View style={style.formContainer}>
          <Content>
            <Form>
              {/* <Text style={style.errMsg}>{errMsg}</Text> */}
              <Item stackedLabel style={style.InputContainer}>
                <Label style={style.formLabel}>Full Name</Label>
                <Input style={[style.formText]} secureTextEntry={true} />
              </Item>
              <Item stackedLabel style={style.InputContainer}>
                <Label style={style.formLabel}>Email</Label>
                <Input style={[style.formText]} secureTextEntry={true} />
              </Item>
              <Item stackedLabel style={style.InputContainer}>
                <Label style={style.formLabel}>Mobile phone</Label>
                <Input style={[style.formText]} secureTextEntry={true} />
              </Item>
              <Item stackedLabel style={style.InputContainer}>
                <Label style={style.formLabel}>Message</Label>
                <Textarea rowSpan={6} style={style.formText} />
              </Item>
            </Form>
          </Content>
          <View style={style.buttonContainer}>
            <Button
              transparent
              //onPress={this.handleSubmit}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 4 }}
                colors={["#FFAA2F", "#FFAA2F"]}
                style={style.linearGradient}
              >
                <Text style={style.buttonText}>Send Message</Text>
              </LinearGradient>
            </Button>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default TrainerContact;
