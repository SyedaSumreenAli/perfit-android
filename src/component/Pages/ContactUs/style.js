import { StyleSheet } from "react-native";
import { height, width } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    paddingTop: height(4),
    paddingLeft: width(6),

    // height:height(140)
  },
  flexRow: {
    flexDirection: "row",
  },
  pb5: {
    paddingBottom: height(5),
  },
  flex5: {
    flex: 5,
  },
  flex1: {
    flex: 1,
  },
  uppercaseH1: {
    textTransform: "uppercase",
    fontFamily: "Montserrat-SemiBold",
    fontSize: 20,
    color: "#424242",
    textAlign: "center",
    //   paddingTop:height(1)
  },
  crossIcon: {
    color: "#3C3C3C",
    flex: 4,
    fontSize: 26,
  },
  h2: {
    color: "#161F3D",
    fontFamily: "Montserrat-Medium",
    fontSize: 25,
  },
  formContainer: {
    flex: 1,
    width: width(85),
    marginLeft: -12,
    marginTop: height(4),
  },
  formText: {
    backgroundColor: "#EFEFEF",
    width: width(81),
    marginLeft: 0,
    // borderColor:"#979797",
    // marginBottom:height(4)
  },
  InputContainer: {
    marginBottom: height(1),
    borderColor: "#EFEFEF",
  },
  formLabel: {
    color: "#161F3D",
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    marginBottom: height(1.5),
  },
  buttonContainer: {
    //  backgroundColor:"yellow",
    // /  flex:4
    marginBottom: height(14),
    marginLeft: width(3),
  },
  linearGradient: {
    // flex: 1,
    // paddingLeft: 15,
    // paddingRight: 15,
    // opacity: 0.9,
    borderRadius: 25,
    //  width:width(50),
    marginTop: height(2),
    // marginBottom:height(14),
  },
  buttonText: {
    fontSize: 17,
    fontFamily: "Montserrat-Medium",
    textAlign: "center",
    marginVertical: 10,
    alignSelf: "center",
    color: "#ffffff",
  },
});

export default style;
