import React, { Component } from 'react'
import {View, Text, Dimensions} from 'react-native'
import {Button} from 'native-base'
import {width, height} from 'react-native-dimension'
import Image from 'react-native-scalable-image'
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import { LineChart, Grid, XAxis, YAxis} from 'react-native-svg-charts'
import Measurement from "../Measurement/Measurement"
import { Circle, Path } from 'react-native-svg'
// import CircularProgress from './ProgressCircle/CircularProgress'
import ProgressCircle from 'react-native-progress-circle'


class Profile extends Component{

    state={
        userImage:require('../../../assets/images/data/9.png'),
        username:"Ahmed Muftah",
        email:"Ahmedmoftah@live.com",
        progressPercent:40,
        weightLoss:98
    }

    render(){
        const{ userImage, username, email, progressPercent, weightLoss}= this.state
        const data = [50, 30, 40 , 45]
        const axesSvg = { fontSize: 10, fill: 'grey' };
        const verticalContentInset = { top: 10, bottom: 10 }
        const xAxisHeight = 30
        
        const Decorator = ({ x, y, data }) => {
            return data.map((value, index) => (
                <Circle
                    key={ index }
                    cx={ x(index) }
                    cy={ y(value) }
                    r={ 4 }
                    stroke={ '#FCB345' }
                    strokeWidth={width(1.6)}
                    fill={ '#FCB345' }
                />
            ))
        }
    return(
        
        <View style={style.container}>
              <View style={style.imageContainer}>
                <Image source={userImage}/>
                <Button transparent style={style.imgEditButton}>
                    <Text style={style.editText}>Edit</Text>
                </Button>
                </View>
                <View>
                    <Text style={style.profileName}>{username}</Text>
                    <Text style={style.email}>{email}</Text>
                    <View style={style.headingContainer}>
                        <Text style={style.h1}>Weight Loss Progress</Text>
                        <Text style={style.h4}>{progressPercent}% Completed</Text>
                    </View>
                </View>
                <View style={style.flexRow}>
                    <View style={style.c1}>
                        <Text style={style.semiBold}>Start</Text>
                        <Text style={style.medium}>110 KG</Text>
                    </View>
                    <View  style={style.c2}>
                    <ProgressCircle
                            percent={40}
                            radius={width(18)}
                            borderWidth={8}
                            color="#FCB345"
                            shadowColor="#D8D8D8"
                            bgColor="#fff"
                        ><View style={style.centerText}>
                            <Text style={style.mediumSmall}>current</Text>
                            <Text style={style.boldLarge}>40 KG</Text>
                        </View>
                            
                        </ProgressCircle>
                    </View>
                    <View style={style.c1}>
                    <Text style={style.semiBold} >Target</Text>
                        <Text style={style.medium}>85 KG</Text>
                    </View>
                </View>
                <View style={{ height: height(50),   width:width(90), padding: 20, flexDirection: 'row' }}>
                <YAxis
                    data={data}
                    style={{ marginBottom: xAxisHeight }}
                    contentInset={verticalContentInset}
                    svg={axesSvg}
                />
                <View style={{ flex: 1, marginLeft: 10,  }}>
                    <LineChart
                        style={{ flex: 1 }}
                        data={data}
                        contentInset={verticalContentInset}
                        svg={{ stroke: '#FCB345', strokeWidth:width(1.2) }}
                    >
                        <Grid/>
                        <Decorator/>
                    </LineChart>
                    <XAxis
                        style={{ marginHorizontal: -10, height: xAxisHeight }}
                        data={data}
                        formatLabel={(value, index) => index}
                        contentInset={{ left: 10, right: 30 }}
                        svg={axesSvg}
                    />
                </View>
            </View>
              
          
           <View>
               <Measurement/>
           </View>
        </View>
       
    )}
        
}
export default Profile