import {StyleSheet} from 'react-native'
import {height, width, totalSize} from 'react-native-dimension'
const style = StyleSheet.create({
    container:{
        paddingTop:height(2),
        // paddingBottom:height(10),
         flex:1,
        alignItems:"center",
        width:width(80),
        // backgroundColor:"skyblue",
        alignSelf:"center",
        // height:height(100),
        paddingBottom:height(10)
    },
    imageContainer:{
        borderRadius:100,
        overflow:"hidden"
     },
     imgEditButton:{
         backgroundColor:"rgba(0,0,0,0.7)",
         position:"absolute",
         bottom:0,
         width:width(26),
        justifyContent:"center"
     },
     editText:{
         color:"#ffffff",
         fontSize:12,
         alignSelf:"center",
         textAlign:"center",
         fontFamily:"Montserrat-SemiBold"
     },
     profileName:{
         color:"#333942",
         fontFamily:"Montserrat-Bold",
         fontSize:totalSize(1.8),
         paddingTop:height(1),
         textAlign:"center"
     },
     email:{
        color:"#333942",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.8),
        paddingTop:height(1),
        textAlign:"center"
    },
    h1:{
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.2),
        color:"#000"
    },
    headingContainer:{
        marginTop:height(4)
    },
    h4:{
        textAlign:"center",
        fontFamily:"Montserrat-SemiBold",
        marginTop:height(1),
        fontSize:totalSize(1.8)
    },
    chartContainer:{
        width:width(95),
        flex:1,
        // backgroundColor:"yellow",
       alignItems:"center"
    },
    flexRow:{
        flexDirection:"row",
        justifyContent:"space-between",
        flex:1,
        paddingVertical:height(5)

    },
    c1:{
        
         alignItems:"center",
         justifyContent:"center",
         flex:1,
    },
    semiBold:{
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(1.5),
        textTransform:"uppercase"
    },
    medium:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.5),
        alignSelf:"center"
    },
    mediumSmall:{
        fontSize:totalSize(1.6),
        fontFamily:"Montserrat-Medium",
        textAlign:"center",
        alignSelf:"center",
        justifyContent:"flex-start",
        marginTop:-height(2)
    },
    boldLarge:{
        alignSelf:"center",
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(3.2)
    }

})

export default style