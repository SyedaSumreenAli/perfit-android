import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    container:{
        //  flex:1,
        width:width(90),
        //  height:height(100),
        // paddingBottom:height(100),
          backgroundColor:"white",
        alignSelf:"center",
        // justifyContent:"space-between"
    },
    card:{
            flex:1,
        backgroundColor:"white",
        width:width(90),
       alignSelf:"center",
//    height:height(50),
      marginVertical:height(2),
        borderRadius:8,
        overflow:"hidden",
        paddingVertical:height(2),
       flexDirection:"row",
       shadowColor: "#000",
       shadowOffset: { width: 15, height: 10 },
       shadowOpacity: 0.5,
       shadowRadius: 12,
       elevation: 8,
    },
    pt2:{
        // paddingVertical:height(10)
    },
    img:{
        alignSelf:"flex-end",
        
    },
    imgContainer:{
        flex:2,
        justifyContent:"center",
        marginTop:-height(4),
        alignItems:"flex-start",
        // backgroundColor:"gray",
       
    },
    textContainer:{
        flex:3,
        // backgroundColor:"pink",
        justifyContent:"center",
        paddingLeft:width(4)
        
    },
    flexRow:{
        flexDirection:"row"
    },
    h1Medium:{
        fontSize:totalSize(2.2),
        fontFamily:"Montserrat-Medium",
        // alignSelf:"center"
    },
    smallMedium:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.4),
        paddingBottom:height(1)
    },
    borderRight:{
       
        borderWidth:width(0.2),
        borderLeftColor:"transparent",
        borderTopColor:"transparent",
        borderBottomColor:"transparent",
        borderRightColor:"#d8d8d8",
    },
    dateIcon:{
        color:"#939495",
        fontSize:totalSize(1.4),
        paddingRight:width(2)
    },
    dateText:{
        color:"#939495",
        fontSize:totalSize(1.4),   
    },
    p3:{
        padding:width(1)
    },
    p3nl:{
        paddingRight:width(4),
        paddingVertical:width(1)
    },

    progressBar:{
        backgroundColor:"#D8D8D8",
        height:height(1),
        borderRadius:4,
        marginRight:width(5),
        marginTop:height(1)
        // width:width(0)
    },
    progressContainer:{
       // backgroundColor:"yellow",
        // flex:2,
        overflow:"hidden"
    },
    progress:{
        backgroundColor:"#FCB345",
        height:height(1), 
        borderRadius:4
    },
    h4Medium:{
        fontFamily:"Montserrat-Medium",
        color:"#26315F",
        fontSize:totalSize(1.6)
    },

    reviewButton:{
        height:height(4),
        width:width(30),
        justifyContent:"center",
        alignSelf:"flex-end",
        borderRadius:25,
        backgroundColor:"#FCB345",
        marginRight:width(5),
        marginBottom:height(1),
        marginTop:height(2)
    },
    reviewButtonText:{
        color:"white",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.4),
       
    },
    reviewButtonContainer:{
        // flex:4,
        //  backgroundColor:"red",
        //  width:width(60),
        //  justifyContent:"center",    
    },
    // nameContainer:{
    //     flex:1
    // },
    flexBox:{
        // flex:1
        // height:height(20)
    },
    dateContainer:{
        // backgroundColor:"gray",
        // flex:
    },
    imgContainer2:{
        flex:2,
        justifyContent:"center",
        // marginTop:-height(4),
        alignItems:"flex-start"
    },
    bgLight:{
        backgroundColor:"#B1B7BF"
    },
    whiteText:{
        color:"white"
    }
})

export default style