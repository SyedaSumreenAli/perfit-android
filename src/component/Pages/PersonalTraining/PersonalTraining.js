import React,{Component} from 'react'
import {ScrollView,View, Text, TouchableOpacity, BackHandler} from 'react-native'
import Image from 'react-native-scalable-image'
import {Icon, Button} from 'native-base'
import style from './style'
import { height, width } from 'react-native-dimension'
import {withNavigation} from 'react-navigation'


class PersonalTraining extends Component{
    render(){
        return(
            <ScrollView>
            <View style={style.container}>  
              <TouchableOpacity style={[style.card,style.pt,style.bgLight]}
                onPress={ ()=>{ this.props.navigation.navigate("StrengthTraining")}}
              >
                    <View style={style.imgContainer2}>
                        <Image 
                        width={width(30)}
                        style={style.img}
                        source={require('../../../assets/images/personalTraining/training.png')}/>
                    </View>

                    <View style={style.textContainer}>
                        <View style={style.nameContainer}>
                            <Text style={[style.h1Medium, style.whiteText]}>Strength Training</Text>
                        </View>

                        <View style={[style.flexRow, style.dateContainer]}>
                            <View  style={[style.borderRight,  style.p3nl]}>
                                <Text style={[style.smallMedium, style.whiteText]}>Start Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={[style.dateIcon ,style.whiteText]}/>
                                    <Text style={[style.dateText ,style.whiteText]}>0/0/00</Text>
                                </View>
                            </View>
                            <View style={ style.p3} >
                                <Text style={[style.smallMedium, style.whiteText]}>End Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={[style.dateIcon, 
                                        style.whiteText]}/>
                                    <Text style={[style.dateText, style.whiteText]}>0/0/00</Text>
                                </View>
                            </View>
                        </View>

                        <View style={style.progressContainers}>
                            <Text style={[style.h4Medium,style.whiteText]}> My Progress</Text>
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(30)}]}></View>
                            </View>
                        </View>

                        <View style={style.reviewButtonContainer}>
                            {/* <Button style={style.reviewButton}>
                                <Text style={style.reviewButtonText}> Add Review </Text>
                            </Button> */}
                        </View>


                    </View>
               </TouchableOpacity>

               <TouchableOpacity style={[style.card,style.pt]}>
                    <View style={style.imgContainer}>
                        <Image 
                        width={width(30)}
                        style={style.img}
                        source={require('../../../assets/images/personalTraining/training.png')}/>
                    </View>

                    <View style={style.textContainer}>
                        <View style={style.nameContainer}>
                            <Text style={style.h1Medium}>Strength Training</Text>
                        </View>

                        <View style={[style.flexRow, style.dateContainer]}>
                            <View  style={[style.borderRight,  style.p3nl]}>
                                <Text style={style.smallMedium}>Start Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>0/0/00</Text>
                                </View>
                            </View>
                            <View style={ style.p3} >
                                <Text style={style.smallMedium}>End Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>0/0/00</Text>
                                </View>
                            </View>
                        </View>

                        <View style={style.progressContainers}>
                            <Text style={style.h4Medium}> My Progress</Text>
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(20)}]}></View>
                            </View>
                        </View>

                        <View style={style.reviewButtonContainer}>
                            <Button style={style.reviewButton}
                              onPress={()=>{this.props.navigation.navigate("AddReview")}}
                            >
                                <Text style={style.reviewButtonText}> Add Review </Text>
                            </Button>
                        </View>


                    </View>
               </TouchableOpacity>

               <TouchableOpacity style={[style.card,style.pt]}>
                    <View style={style.imgContainer2}>
                        <Image 
                        width={width(30)}
                        style={style.img}
                        source={require('../../../assets/images/personalTraining/training.png')}/>
                    </View>

                    <View style={style.textContainer}>
                        <View style={style.nameContainer}>
                            <Text style={style.h1Medium}>Strength Training</Text>
                        </View>

                        <View style={[style.flexRow, style.dateContainer]}>
                            <View  style={[style.borderRight,  style.p3nl]}>
                                <Text style={style.smallMedium}>Start Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>0/0/00</Text>
                                </View>
                            </View>
                            <View style={ style.p3} >
                                <Text style={style.smallMedium}>End Date</Text>
                                <View style={style.flexRow}>
                                    <Icon name="date" type="Fontisto" style={style.dateIcon}/>
                                    <Text style={style.dateText}>0/0/00</Text>
                                </View>
                            </View>
                        </View>

                        <View style={style.progressContainers}>
                            <Text style={style.h4Medium}> My Progress</Text>
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(30)}]}></View>
                            </View>
                        </View>

                        

                    </View>
               </TouchableOpacity>
               <View style={style.flexbox}></View>

            </View>
            </ScrollView>
        )
    }
}


export default withNavigation(PersonalTraining)