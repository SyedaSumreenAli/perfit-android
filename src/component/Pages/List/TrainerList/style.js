import { StyleSheet } from "react-native";
import { width, height } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    // backgroundColor:"yellow"
    backgroundColor: "white",
    flex: 1,
  },
  flexRow: { flexDirection: "row" },
  iconContainer: {
    flex: 3,
    // backgroundColor:"orange",
    height: height(15),
    justifyContent: "center",
  },
  icon: {
    // backgroundColor:"green",
    // width: width(30),
    alignSelf: "center",
    marginLeft:-width(12),
    fontSize: 35,
  },
  flex4: {
    flex: 4,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  headerContainer2: {
    width: width(90),
    //  backgroundColor:"pink",
    alignSelf: "center",
    // flex:2,
    height: height(12),
  },

  headerText: {
    // backgroundColor:"red",
    flexDirection: "column",
    color: "#424242",
    fontSize: 20,
    fontFamily: "Montserrat-SemiBold",
  },
  h3: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    color: "#000000",
  },
  h2: {
    fontFamily: "Montserrat-Bold",
    fontSize: 28,
    color: "#000000",
  },
  list: {
    backgroundColor: "#F6F6F6",
    height: height(12),
  },
 
  textOuterContainer: {
    // backgroundColor:"skyblue",
    flex: 7,
    justifyContent: "center",
  },
  buttonOuterContainer: {
    // backgroundColor:"orange",
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  imgOuterContainer: {
    // backgroundColor:"yellow",
    flex: 3,
    alignItems: "center",
    justifyContent: "center",
  },
  imageContainer: {
    // position: "absolute",
    overflow:"hidden",
    // flex:1,
    // height:height(20),
    // justifyContent:"space-around"
    borderColor: "#6D7278",
    borderWidth: width(0.2),
    borderRadius: 50,
  },
  img:{
    borderColor: "#6D7278",
    borderWidth: width(0.2),
    borderRadius: 50,
    width:80,
    height:80,
    // flex:1
  },
  categoryText: {
    backgroundColor: "#6D7278",
    borderRadius: 20,
    paddingHorizontal: width(2),
    fontSize: 11,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    marginTop:-height(2)
  },
  categoryContainer: {
    // backgroundColor:"red",
    // height: height(10),
    // justifyContent: "flex-end",
    //  flex:1,
  },
  nameText: {
    fontFamily: "Montserrat-SemiBold",
    color: "#000000",
    textTransform: "capitalize",
    fontSize: 20,
  },
  starIcon: {
    fontSize: 11,
    color: "#FDC900",
    padding: width(0.2),
  },
  reviewText: {
    color: "#323131",
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
  },
  buttonContainer: {
    width: width(10),
    height: width(10),
    backgroundColor: "#F7C400",
  },
  arrowLeft: {
    color:"#fff",
    fontSize: 24,
    zIndex: 4,
    // backgroundColor:"red",
    // flex:1,
    width: width(6),
    alignSelf: "center",
  },
});

export default style;
