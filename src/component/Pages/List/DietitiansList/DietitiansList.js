import React, { Component } from "react";
import {  View, Text, TouchableOpacity, Image ,FlatList,RefreshControl} from "react-native";
import { Icon, Button, Spinner } from "native-base";
// import Image from "react-native-scalable-image";
import style from "./style";
import { ScrollView } from "react-native-gesture-handler";


import {fetchAllDietitians} from '../../../../redux/actions/AllDietitiansActions'

import {connect} from 'react-redux'

class DietitiansList extends Component {
  state ={
    dietitions:[],
    loading:true,
    errorMessage:'',
    refreshing: false
  }
  static navigationOptions = {
    headerShown: false,
  };
  handlePTPage = () => {
    let trainer = this.props.trainers.user.id
    this.props.navigation.navigate("PTPage",{trainer});
  };  

  componentDidMount = async () => {
    
    
    await  this.props.fetchAllDietitians()
   
  
   
    // this.setState({
    //  dietitians:this.props.dietitians,
    //   loading: false,
    //   errorMessage: this.props.errorMessage
    // })
   
  
  };

  handleRefresh = () => {

    this.setState({

      refreshing: true
    }, async() => {

      await this.props.fetchAllDietitians()
      

        this.setState({
          refreshing: false
        })
     
    })

  }


  render() {
    let counter = 0;
    let trainerId;

     console.log("props")
   console.log(this.props.dietitians.dietitions)


    return (
      <ScrollView

      refreshControl={
        <RefreshControl
          tintColor={"green"}
          refreshing={this.state.refreshing}
          onRefresh={() => this.handleRefresh()}
        />
      }
      showsVerticalScrollIndicator={true}
      
      >
     { this.props.loading ? 
        <View style={style.loaderContainer}>
          <Spinner style={style.loader}/>
        </View>:  <View style={style.container}>
          <View style={style.flexRow}>
            
            <View style={style.iconContainer}>
            <TouchableOpacity 
              onPress={()=>this.props.navigation.goBack()}
            >
              <Image style={style.icon} source={require('../../../../assets/images/icons/arrow.png')}/>
              </TouchableOpacity>
            </View>

            
            <View style={style.flex4}>
              <View style={style.headerContainer}>
                <Text style={style.headerText}>PT</Text>
              </View>
            </View>
          </View>
          <View style={style.headerContainer2}>
            <Text style={style.h3}>Perfect choice for state </Text>
            <Text style={style.h2}>Personal Dietitians</Text>
          </View>
          <View>


            <View style={style.listContainer}> 





            <FlatList 

              data={this.props.dietitians.dietitions}
              renderItem={({item,index})=>{
               
                console.log(item.id)

                return (

                  <TouchableOpacity
              key={index}
              onPress={()=>this.props.navigation.navigate("DTPage",{dietitianId:item.id,photo:item.photo})}
              style={[
                style.flexRow,
                style.list,
                {
                  backgroundColor: index % 2 ? "white" : "#F6F6F6",
                },
              ]}
            >


              <View style={style.imgOuterContainer}>
                <View style={style.imageContainer}>
                  <Image style={style.img}
                    // height={80}
                    width={80}
                  source={item.photo ? {uri: "http://system.perfitapp.com/images/profile_images/"+item.photo }
                      : require("../../../../assets/images/Selection/trainer1.png")} resizeMode="cover" />
                </View>
                <View style={style.categoryContainer}>
                  <Text style={style.categoryText}>Weight lose</Text>
                </View>
              </View>
              <View style={style.textOuterContainer}>
                <Text style={style.nameText}>{item.user.name}</Text>
                <View style={style.flexRow}>
                  <View style={style.flexRow}>
                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                  </View>
                  <Text style={style.reviewText}> Review (20)</Text>
                </View>
              </View>
              <View style={style.buttonOuterContainer}>
                
                <Button
                  rounded
                  style={style.buttonContainer}
                  onPress={()=>this.props.navigation.navigate("DTPage",{dietitianId:item.id})}
                >
                  <Icon
                    style={style.arrowLeft}
                    name="arrowright"
                    type="AntDesign"
                  />
                </Button>
              </View>
            </TouchableOpacity>

                )
              }}

            />
  


            </View>  
          </View>
        </View>}
      </ScrollView>
    );
  }
}

const dispatchStateToProps=(dispatch)=>{

  return{

      fetchAllDietitians:()=>dispatch(fetchAllDietitians())
  }


}

 const mapStateToProps = (state) => {
  return {
    dietitians: state.Diet
    
  }
}
export default connect(mapStateToProps,dispatchStateToProps)(DietitiansList);