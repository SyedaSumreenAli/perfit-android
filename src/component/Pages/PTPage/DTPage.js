import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Icon, Button } from 'native-base'
// import Image from 'react-native-scalable-image'
import Card from './Card/Card'
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import { width, height } from 'react-native-dimension'
import { withNavigation } from 'react-navigation'
import axios from 'axios'
import { BASE_URI } from '../../../redux/api'


class DTPage extends Component {

    static navigationOptions = {
        headerShown: false
    };
    state = {
        name: '',
        imgUri: '',
        successTransformation: 122,
        review: 20,
        category: "Weight lose",
        description: "",
        tag:""

    }

    componentDidMount = async () => {

        const dietitianId = this.props.navigation.state.params.dietitianId

        console.log(dietitianId)


        await axios.get(`http://system.perfitapp.com/api/dietitian/${dietitianId}/view`)
            .then(response => {


                console.log(response.data)

                // console.log(response.data.dietitian.name)
                // console.log(response.data.trainer.portfolio)

                let portfolio = response.data.dietitian.portfolio

                var text = portfolio.replace(/<[^>]*>/g, '')

                // console.log(text)

                let tags = response.data.dietitian.user.tags

                console.log(tags[0].name)

                this.setState({
                    name: response.data.dietitian.user.username,
                    imgUri: response.data.dietitian.user.photo,
                    description: text,
                    tag: tags[0].name,
                })

            }).catch(err => console.log(err))


    }

    render() {

        const { name, imgUri, successTransformation, review, category, description } = this.state

        const dietitianId = this.props.navigation.state.params.dietitianId
        console.log(dietitianId)

        const imageUri = this.props.navigation.state.params.photo
        return (
            <ScrollView>
                <View />
                <View style={style.container}>
                    <View style={[style.flexRow, style.headerContainer]}>
                        <TouchableOpacity style={style.iconContainer}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Icon name="arrowleft" type="AntDesign" style={style.icon} />
                        </TouchableOpacity>
                        <View style={style.flex4} >
                            <View>
                                <Text style={style.headerText}>DT</Text>
                            </View>
                        </View>
                    </View>
                    <View style={style.flexRow}>
                        <View style={style.imgOuterContainer}>
                            <View style={style.imgContainer}>
                                <Image
                                    // source={imgUri}
                                    source={imgUri ? { uri: "http://system.perfitapp.com/images/profile_images/" + imgUri }
                                        : { uri: "https://image.flaticon.com/icons/png/512/21/21294.png" }}
                                    resizeMode="cover"
                                    resizeMethod="resize" 
                                    style={style.profileImg}
                                />

                                {/* <View style={style.categoryContainer}>   */}
                                <Text style={[style.categoryText]}>{this.state.tag}</Text>
                                {/* </View>   */}
                            </View>
                        </View>

                        <View style={style.reviewContainer}>
                            <View style={[style.nameContainer]}>
                                <Text style={style.h2Dark}>{name}</Text>
                            </View>
                            <View style={[style.flexRow, style.reviewInnerContainer]}>
                                <View style={[style.flexRow, style.reviewIconContainer]}>
                                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                                    <Icon style={style.starIcon} name="star" type="AntDesign" />
                                </View>
                                <TouchableOpacity style={style.reviewTextContainer}
                                    onPress={() => this.props.navigation.navigate("Review")}
                                >

                                    <Text style={style.reviewText}>Reviews({review})</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>

                    <View style={[style.flexRow, style.headerContainer2]}>
                        <View style={style.headerTextContainer}>
                            <Text style={style.textSmall}>Success Transformation</Text>
                            <Text style={style.h2Dark}>{successTransformation}</Text>
                        </View>
                        <View style={style.buttonContainer}

                        >
                            <Button style={style.button} onPress={() => this.props.navigation.navigate("Chat")}>
                                <Text style={style.buttonText}>Contact me</Text>
                            </Button>
                        </View>
                    </View>

                    <View style={style.textContainer}>
                        <Text style={style.description}>{description}</Text>
                    </View>
                    <View
                        style={{
                            //  
                            // marginLeft:-width(10),
                            //  height:height(60)      
                        }}>
                        <Card id={dietitianId} userType='dietitian'/>
                    </View>

                </View>
            </ScrollView>
        ); 
        //roko
    }
}
export default withNavigation(DTPage)