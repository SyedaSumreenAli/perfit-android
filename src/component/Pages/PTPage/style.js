import { StyleSheet } from "react-native";
import { width, height } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    // backgroundColor:"yellow"
    backgroundColor: "white",
    flex: 1,
    // height:height(100)
  },
  flexRow: { flexDirection: "row" },
  iconContainer: {
    flex: 6,
    //  backgroundColor:"orange",
    // height: height(12),
    justifyContent: "center",
  },
  icon: {
    // backgroundColor:"green",
    width: width(30),
    alignSelf: "center",
    fontSize: 35,
  },
  flex4: {
    flex: 7,
    alignItems: "flex-start",
    justifyContent: "center",
    //  backgroundColor:"green",
  },
  headerText: {
    // backgroundColor:"red",
    flexDirection: "column",
    color: "#424242",
    fontSize: 20,
    fontFamily: "Montserrat-SemiBold",
  },
  imgOuterContainer: {
    flex: 2,
    height: height(15),
    // alignItems:"center",
    //  backgroundColor:"yellow",
    justifyContent: "center",
  },
  imgContainer: {
    borderRadius: 50,
    borderWidth: 0.4,
    position: "absolute",
    // overflow:"hidden",
    alignSelf: "center",
    borderColor: "#000000",
    //
    //   backgroundColor:"red"
  },
  categoryContainer: {
    //   backgroundColor:"red",
      height: height(10),
    alignItems: "center",
    alignSelf: "center",
    position: "absolute",
    justifyContent: "flex-end",
    flex: 1,
    // paddingHorizontal: width(1),
  },
  profileImg:{
    marginBottom:-13,
     borderRadius:50,
      height:width(24),
       width:width(24)
      },
  
  categoryText: {
    backgroundColor: "#000000",
    borderRadius: 20,
   
    fontSize: 11,
 
   textAlign:"center",

    color: "#ffffff",
    fontFamily: "Montserrat-Medium",
  },
  nameContainer: {
    //  backgroundColor:"skyblue",
    flex: 1,
    justifyContent: "center",
  },
  reviewInnerContainer: {
    flex: 1,
  },
  reviewContainer: {
    //  backgroundColor:"red",
    flex: 4,
    height: height(15),
    // flexDirection:"row",
  },
  reviewIconContainer: {
    //  backgroundColor:"red",
    flex: 1.5,
  },
  starIcon: {
    fontSize: 13,
    paddingLeft: width(0.6),
    color: "#F7C400",
  },
  reviewTextContainer: {
    flex: 3,
    // justifyContent:"flex-end",
    alignItems: "flex-start",
    //  backgroundColor:"skyblue"
  },
  reviewText: {
    color: "#323131",
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    textDecorationLine: "underline",
  },
  headerContainer2: {
    //  backgroundColor:"red",
    // flex:1,
    height: height(12),
    width: width(90),
    alignSelf: "center",
    borderBottomColor: "#E3E3E3",
    borderWidth: 1,
    borderColor: "transparent",
    marginBottom:-height(2),
    
  },
  headerTextContainer: {
    // backgroundColor:"skyblue",
    justifyContent: "center",
    flex: 2,
  },
  textSmall: {
    fontFamily: "Montserrat-Medium",
    fontSize: 13,
    color: "#000000",
  },
  h2Dark: {
    color: "#000000",
    fontFamily: "Montserrat-Bold",
    fontSize: 26.68,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    width: width(25),
    borderRadius: 50,
    justifyContent: "center",
    height: height(4),
    backgroundColor: "#FCB344",
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 13,
  },
  textContainer: {
    flex: 2,
    // backgroundColor:"skyblue",
    width: width(90),
    alignSelf: "center",
    justifyContent: "center",
    paddingVertical: height(4),
  },
  description: {
    color: "#535050",
    fontFamily: "Montserrat-Medium",
    fontSize: 14,
    textAlign: "justify",
    lineHeight: 20,
  },
  headerContainer:{
    // backgroundColor:"blue",
    height:height(10)
  }
 
});
export default style;
