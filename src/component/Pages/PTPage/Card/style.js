import { StyleSheet } from "react-native";
import { width, height, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    // backgroundColor:"yellow",
    // height:height(120)
  },
  h2Dark: {
    fontFamily: "Montserrat-Bold",
    fontSize: 19,
    color: "#000000",
    height: height(6),
  },
  card: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderWidth: 0.2,
    borderRadius: 12,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: { width: 15, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 12,
    elevation: 15,
    marginBottom: height(8),
    marginRight: width(2),
  },
  image: {
    width: width(80),
    height: height(50),
    position: "absolute",
  },
  shadowImage: {
    flex: 1,
  },
  priceText: {
    color: "#ffffff",
    fontSize: totalSize(2.6),
    fontFamily: "Montserrat-Bold",
    
   
  },
  arrowContainer:{
    flex:3,
    justifyContent:"center",
    alignItems:"center",
    // backgroundColor:"gray"
  },
  priceIcon:{
    color:"#ffffff",
    fontSize:totalSize(4),
    // flex:2,
     justifyContent:"center",
    // backgroundColor:"gray"
  },
  amountContainer:{
    flex:3,
   justifyContent:"center",
  //  backgroundColor:"red",
    alignItems:"flex-end"
  },
  priceContainer: {
    backgroundColor: "#F7C400",
    // backgroundColor:'red',
  //  justifyContent: "center",
    height: height(14),
    alignItems: "center",
    flexDirection:"row",
    flex:1
  },
  list: {
    position: "absolute",
    flex: 1,
    //  backgroundColor:"red",
    height: height(43),
    justifyContent: "flex-end",
    paddingLeft: height(2),
  },
  text: {
    color: "#ffffff",
    fontFamily: "Montserrat-Regular",
    fontSize: totalSize(1.6),
    paddingLeft: width(1),
  },
  bullet: {
    color: "#818181",
    fontSize: totalSize(1.6),
  },
  flexRow: {
    flexDirection: "row",
  },
  spinnerContainer:{
    height:height(30),
     backgroundColor:"white"
  }
});
export default style;
