import React, { Component } from "react";
import { View, StatusBar, TouchableHighlight, Image } from "react-native";
import {  Text,  Icon, Spinner } from "native-base";
import { height, width } from "react-native-dimension";
import Carousel, { Pagination } from "react-native-snap-carousel";
import LinearGradient from "react-native-linear-gradient";
import { withNavigation } from "react-navigation";

import style from "./style";
import axios from "axios";

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardData: [],
      activeSlide: 0,
    };
   
  }
  static navigationOptions = {
    headerShown: false,
  };

  componentDidMount = async () => {
    let id = this.props.id
    console.log('coming from PTPage:'+ id)
       await axios.get(`http://system.perfitapp.com/api/trainer/${id}/view`)
       .then(response =>{
        
        console.log(response.data)
        this.setState({ cardData : response.data.trainer.packages })

      

       }).catch(err => console.log(err))
      
  };

  renderCardData({ item, index }, parallaxProps) {
  
    return (
      <TouchableHighlight
        style={style.container}
        // onPress={() => this.props.navigation.navigate("PackageDetails",{packageId:item.id, trainerId:this.props.trainerId})}
        activeOpacity={0.6}
        underlayColor="transparent"
      >
        <>
          <Text style={style.h2Dark}>{item.name}</Text>
          <View style={style.card}>
            <Image
              style={style.image}
              source={ item.image ? {uri: "http://system.perfitapp.com/images/packages/"+item.image } :
              require("../../../../assets/images/data/pt1.png")}
              resizeMethod="resize"
              resizeMode="cover"
            />
            <Image
              stylw={style.shadowImage}
              source={require("../../../../assets/images/data/Rectangle.png")}
              resizeMethod="resize"
              resizeMode="cover"
            />
            {/* <View style={style.list}>{listItem}</View> */}
              <View  style={style.list}>{ item.highlights.map(text => (
                  <View style={style.flexRow}>
                    <Text style={style.bullet}>{'\u2B24'}</Text>
                    <Text style={style.text} >{text}</Text>
                  </View>
              )) }</View>
            <View style={style.priceContainer}>
              <View style={style.amountContainer}>
                <Text style={style.priceText}>${item.price}</Text>
              </View>
              <View style={style.arrowContainer}>
              <Icon style={style.priceIcon} name="arrowright" type="AntDesign"/>
              </View>
              
            </View>
          </View>
        </>
      </TouchableHighlight>
    );
  }

  render() {
    const submit = this.handleSubmit;
    let content = this.props.loading ?
    <View style={style.spinnerContainer}>
        <Spinner large color="#F7C400"/> 
    </View> :
      <Carousel
        data={this.state.cardData}
        sliderWidth={width(100)}
        itemWidth={width(80)}
        layout={"default"}
        renderItem={this.renderCardData.bind(this)}
        onSnapToItem={(index) => {
          this.setState({ activeSlide: index });
        }}
        hasParallaxImages={true}
      />
    return (
      <View
        style={{
            //  backgroundColor:"red",
          zIndex: 2,
          overflow: "visible",
          width: width(100),
          // flex:1,
          // height:height(100)
        }}
      > 
      {content}
      </View>
    );
  }
}



export default withNavigation(Cards);
