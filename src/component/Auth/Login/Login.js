import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  KeyboardAvoidingView,
  AsyncStorage,
  Platform,
} from "react-native";
import logo from "../../../assets/images/logo.png";
import { Button, Content, Form, Item, Input, Spinner, Toast } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import style from "./loginStyle";
import Icon from "react-native-vector-icons/FontAwesome";
import axios from "axios";
import { LoginManager, AccessToken } from "react-native-fbsdk";
import {
  GoogleSignin,
  statusCodes,
} from "@react-native-community/google-signin";
import {width, height } from "react-native-dimension";
import { validateAll } from 'indicative/validator';


class Login extends Component {

  state = {
    errMsg: "Invalid credentials, please enter again.",
    errors: [],
    email: "",
    password: "",
    username: "",
    user: {},
    isLoading: false,
  };

  handleSubmit = async () => {

    const { email, password, errors } = this.state;
    const rules = {

      email: 'required|email',
      password: 'required|string|min:6',
    }

    const messages = {
      required: (field) => `${field} field is required`,
      'email.email': 'Invalid email address, please enter again',
      'password.min': 'password must be atleast 6 characters long',
    }

    try {
      await validateAll({ email, password }, rules, messages)
      this.setState({ isLoading: true })

      const response = await axios.post('http://system.perfitapp.com/api/login', {
        email,
        password
      })

      console.log("response")
      console.log(response.data)


      if (response.data.success) {

        this.setState({
          isLoading: false,
          email: "",
          password: "",
          isLoading: false,
          user: response.data
        })
        console.log('----------------------------------')

        console.log(response.data.user)
        this._storeData(response.data.user.id)
        // this.props.navigation.navigate("Tab");
        this._handleNextPage(response.data.user.id)
      }

      /************ SERVER SIDE ERRORS ***********/
      if (response.data.error=="Credentials does not match") {
       let errorMessage = 'Invalid credentials, please enter again'
        const updatedError = [errorMessage, ...errors]
        console.log(updatedError);

        this.setState({
          errors: updatedError,
          isLoading: false
        })
      }

    } catch (err) {

      /************ VALIDATION ERRORS ***********/

      console.log("catch----------")
      console.log(err)

      let formatedErrors = []
      err.forEach(errors => formatedErrors.push(errors.message))
      console.log('$$$$$$$$$$$$$$$$$$$' + formatedErrors)
      this.setState({
        isLoading: false,
        errors: formatedErrors
      })

    }
  };

  _storeData = async (id) => {
    try {
      await AsyncStorage.setItem("user_id", id.toString());
    } catch (error) {
      console.log(error);
    }
  };

  _handleNextPage = async(user_id)=>{
    console.log(user_id)
    try{
      const response = await axios.get(`http://system.perfitapp.com/api/user/${user_id}/get/profile`);
      console.log('--------------- handle next page api try block -------------')
      console.log(response.data)
      if(response.data.message == 'Profile Not Found'){
        this.props.navigation.navigate('YourDetails')
      }
      if(response.data.success === true){
        this.props.navigation.navigate('Tab')
      }

    }catch(error){
      console.log('--------------- handle next page api catch block -------------')
      console.log(error)

    }
  }

  _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      const email = userInfo.user.email;
      console.log("Email " + email);

      const name = userInfo.user.name;
      console.log("Name " + name);

      const password = userInfo.user.id + name;
      console.log("Password" + password);

      this.setState({
        email: email,
        username: name,
        password: password,
      });

      axios
        .post("http://system.perfitapp.com/api/google-login", {
          email: email,
          name: name,
        })
        .then((res) => {

          console.log("..............")
          console.log(res)
          this.setState({ isLoading: false });
          this.setState({ user: res.data });

          console.log(res.data.user.id);
          if (this.state.user.success === true) {
            this._storeData(res.data.user.id);
            // this.props.navigation.navigate("Tab");
            this._handleNextPage(res.data.user.id)
          } else {

            errorMessage = 'Invalid credentials, please enter again'
            const updatedError = [errorMessage, ...errors]
            console.log(updatedError);

            this.setState({
              errors: updatedError,
              isLoading: false
            })
          }
        })
        .catch((err) => console.log(err));
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        errorMessage = error.code
        const updatedError = [errorMessage, ...errors]
        console.log(updatedError);

        this.setState({
          errors: updatedError,
          isLoading: false
        })
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        errorMessage = error.code
        const updatedError = [errorMessage, ...errors]
        console.log(updatedError);

        this.setState({
          errors: updatedError,
          isLoading: false
        })
      } else {
        // some other error happened
        console.log(error);
      }
    }
  };

  handleFacebookLogin() {
    // try {
    //   LoginManager.setLoginBehavior(Platform.OS ==='ios' ? 'native': 'NATIVE_ONLY');    
    // } catch (error) {
    //   LoginManager.setLoginBehavior('WEB_ONLY');  
    // }
    // LoginManager.setLoginBehavior(Platform.OS ==='ios' ? 'native': 'NATIVE_ONLY');
    // LoginManager.setLoginBehavior('NATIVE_ONLY');
    const initUser = (token) => {
      fetch(
        "https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,friends&access_token=" +
        token
      )
        .then((response) => {
          response.json().then((json) => {
            const id = json.id;
            console.log("ID " + id);

            const email = json.email;
            console.log("Email " + email);

            const firtName = json.first_name;
            console.log("First Name  " + firtName);
            const lastName = json.last_name;
            console.log("Last Name " + lastName);
            this.setState({
              email: email,
              username: firtName + lastName,
              password: firtName + lastName + id,
            });

            axios
              .post("http://system.perfitapp.com/api/facebook-login", {
                email: email,
                name: firtName + lastName,
              })
              .then((res) => {
                this.setState({ isLoading: false });
                this.setState({ user: res.data });
                console.log(res.data);
                if (this.state.user.success === true) {
                  this._storeData(res.data.user.id);
                  // this.props.navigation.navigate("Tab");
                  this._handleNextPage(res.data.user.id)
                } else {
                  console.log("no");
                  this.setState({ isLoading: false, errMsg: 'Error Occur' });
                  // ToastAndroid.show("Error Occured", ToastAndroid.LONG);
                }
              })
              .catch((err) => console.log(err));
          });
        })
        .catch(() => {
          console.log("ERROR GETTING DATA FROM FACEBOOK");
        });
    };
    
    LoginManager.logInWithPermissions([
      "public_profile",
      "email",
      "user_friends",
    ]).then(
      function (result) {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const { accessToken } = data;
            console.log(accessToken);
            initUser(accessToken);
          });
        }
      },
      function (error) {
        console.log("Login fail with error: " + error);
      }
    );
  }

  componentDidMount() {
    GoogleSignin.configure({
      //  scopes: ["profile", "email"],
      webClientId:
        "616932809388-gbv8pc7ik4v8dt802psdgag2p3rjs6sl.apps.googleusercontent.com",
      offlineAccess: true,
      forceCodeForRefreshToken: true,
    });
  }

  focusTheField = (id) => {
    this.inputs[id]._root.focus();
  }

  render() {
    const { email, password, errors } = this.state;
    console.log('********************' + errors);
    return (
      <View style={style.container}>
        <KeyboardAvoidingView
          keyboardVerticalOffset={-10}
          style={{ flex: 1 }}
          behavior="height"
        >
          <View style={style.container}>
            <View style={style.logoContainer}>
              <Image source={logo} style={style.logo} />
            </View>

            <View style={style.formContainer}>
              {errors.length > 0 ? (
                <Text style={style.errMsg}>{errors[0]}</Text>
              ) : null}
              {/* <Content keyboardShouldPersistTaps={'handled'}> */}
                <Form>
                  <Item>
                    <Icon
                      style={[style.formIcon]}
                      type="FontAwesome"
                      name="envelope-o"
                    />
                    <Input
                      style={[style.formText]}
                      placeholder="Email"
                      onChangeText={(text) => this.setState({ email: text })}
                      value={email}
                      blurOnSubmit={false}
                      returnKeyType="next"
                      onSubmitEditing={() => {
                        this.Password._root.focus();
                      }}
                    // onBlur={this.Validator(email)}
                    />
                  </Item>
                  <Item style={{ marginTop: height(1) }}>
                    <Icon
                      style={[style.formIcon]}
                      type="FontAwesome"
                      name="key"
                    />
                    <Input
                      style={[style.formText]}
                      placeholder="Password"
                      secureTextEntry={true}
                      // onChange={this.handleChange}
                      // onBlur={this.Validator(password)}
                      onChangeText={(text) => this.setState({ password: text })}
                      // getRef={input => { this.inputs['password'] = input }}
                      ref={input => {
                        this.Password = input;
                      }}
                      value={password}
                    />
                  </Item>
                </Form>
              {/* </Content> */}

            </View>


            <View style={style.linksContainer}>
              {this.state.isLoading ? (
                <Spinner color="#FCB044" />
              ) : (
                  <Button transparent onPress={this.handleSubmit}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 4 }}
                      colors={["#efcc64", "#FFAA2F"]}
                      style={style.linearGradient}
                    >
                      <Text style={style.buttonText}>Login</Text>
                    </LinearGradient>
                  </Button>
                )}

              <View style={[style.flexRow, style.socialMediaContainer]}>
                <View>
                  <Button
                    style={{backgroundColor:'#3b5998', width: width(30),
                    borderRadius: 25,
                    paddingVertical: height(1.5),
                    marginTop: height(3),
                   
                    alignItems: "center",
                    justifyContent: "center"}}
                    name="facebook"
                    // backgroundColor="transparent"
                    onPress={() => this.handleFacebookLogin()}
                  >
                    <Icon name="facebook" type='AntDesign' style={style.google} />
                  </Button>
                
                </View>
                <View>
                  <Button
                    style={[
                      style.socialMediaButton,
                      { backgroundColor: "#eb1a43" },
                    ]}
                    onPress={this._signIn}
                  >
                    <Icon name="google" type="AntDesign" style={style.google} />
                  </Button>
                </View>
              </View>

              <View style={[style.LinkContainer, style.mb2]}>
                <View>
                  <Button
                    style={style.forgotPassword}
                    transparent
                    onPress={() => {
                      this.props.navigation.navigate("ForgetPassword");
                    }}
                  >
                    <Text style={[style.lightText]}>
                      {" "}
                      Forgot your password?
                    </Text>
                  </Button>
                </View>

                <View style={[style.flexRow]}>
                  <View>
                    <Text style={[style.lightText]}>
                      Don't have an account yet?{" "}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={style.signupButton}
                    onPress={() => this.props.navigation.navigate("Signup")}
                  >
                    <View>
                      {/* <Button
                    transparent
                    style={[style.SignupLinkContainer]}
                    onPress={() => this.props.navigation.navigate("Signup")}
                  > */}
                      <Text style={[style.SignupLink]}>Sign up</Text>
                      {/* </Button> */}
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    style.LinkContainer,
                    style.trainerScreenLinkContainer,
                  ]}
                >
                  <View style={[style.flexRow]}>
                    <Text style={[style.trainerText]}>If you're a </Text>

                    {/* <Button transparent style={[style.trainerLink]}> */}
                    <Text style={[style.trainerLinkText]}>Trainer </Text>
                    {/* </Button> */}

                    <Text style={[style.trainerText]}>or </Text>

                    {/* <Button transparent style={[style.trainerLink]}> */}
                    <Text style={[style.trainerLinkText]}>Diatiation</Text>
                    {/* </Button> */}
                  </View>
                  <Button
                    transparent
                    onPress={() => {
                      this.props.navigation.navigate("ContactUs");
                    }}
                  >
                    <Text style={[style.contact]}>CONTACT US</Text>
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default Login;
