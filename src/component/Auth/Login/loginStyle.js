import { StyleSheet } from "react-native";
import { width, height, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor:"red",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  logo: {
    
    width: width(40),
    height: height(23),
  },
  logoContainer: {
  //  flex: 0.5,
    paddingTop: height(3.5),
   // paddingBottom: height(5),
  //  zIndex: 2,
    //backgroundColor:"pink",
  },
  formContainer: {
    // backgroundColor:"yellow",
    width: width(80),
    height:height(23),
   // marginBottom: height(3),
   // flex: 0.4,
    paddingTop:height(3),
    marginBottom:height(2)
  },
  linksContainer: {
    // backgroundColor:"green",

  //  flex: 0.6,
  },
  textCenter: {
    textAlign: "center",
  },

  linearGradient: {
    flex: 1,
    paddingLeft: 45,
    paddingRight: 45,
    opacity: 0.9,
    borderRadius: 25,
  },
  buttonText: {
    fontSize: 19.23,
    fontFamily: "Montserrat-Light",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
  },
  trainerScreenLinkContainer: {
  marginTop:-height(6)
  },
  trainerLink: {
    // paddingTop: height(1.8),
    // paddingHorizontal: width(0.7),
    // height: 0,
  },
  trainerLinkText: {
    fontFamily: "Montserrat-Bold",
    color: "#FCB044",
    fontSize: 17.58,
    paddingTop:height(11)
  },
  trainerText: {
    fontFamily: "Montserrat-Regular",
    color: "#FCB044",
    fontSize: 17.5,
    paddingTop:height(11)
  },
  LinkContainer: {
  //  flex: 1,
    // backgroundColor:'orange',
    alignItems: "center",
  },
  SignupLinkContainer: {
    //  backgroundColor:"yellow",
    padding:0,
    height:height(1),
    justifyContent:"flex-start",
    // alignItems:"flex-start"
  },
  flexRow: {
    flexDirection: "row",
  },
  mb2: {
  //  paddingBottom: height(1),
  },
  lightText: {
    color: "#888888",
    fontFamily: "Montserrat-Regular",
    fontSize: 15.58,
    paddingTop:height(4)
    
  },
  SignupLink: {
    color: "#888888",
    fontFamily: "Montserrat-Bold",
    fontSize: 15.58,
    paddingTop:height(4)
   // marginTop: height(1),

    // backgroundColor:'red'
  },

  socialMediaContainer: {
    flex: 0,
  //  backgroundColor:'orange',
    justifyContent: "space-between",
  },
  socialMediaButton: {
  //  flex: 2,
    width: width(30),
    borderRadius: 25,
    paddingVertical: height(1.5),
    marginTop: height(3),
   
    alignItems: "center",
    justifyContent: "center",
  },
  facebook: {
    backgroundColor: "#3b5998",
    height:height(6.8)
  },
  google: {
     color:"#fff",
     fontSize:totalSize(2.2)
  },
  contact: {
   // paddingTop: height(2),
    fontFamily: "Montserrat-Bold",
    color: "#FCB044",
    fontSize: 17,
  },
  formIcon: {
    color: "#FDBE51",
    fontSize: 20,
    marginRight: width(3),
  },
  formText: {
    color: "#C0C0C0",
    fontFamily: "Montserrat-Medium",
    fontSize: 17.5,
  },
  errMsg: {
    paddingTop:height(2),
    color: "#EB1717",
    fontFamily: "Montserrat-Medium",
    fontSize: 14,
    marginLeft: width(2),
  },
  forgotPassword:{
    //  backgroundColor:"yellow",
     marginTop:height(2),
    marginBottom:-height(2.5)
  },
  signupButton:
  {
    // backgroundColor:"red",
    zIndex:3,
    marginTop:height(4),
    height:height(3),
    justifyContent:"flex-end"
  }
});

export default style;
