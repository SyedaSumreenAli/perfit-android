import React, { Component } from "react";
import { Text, View } from "react-native";
import { Button, Content, Form, Item, Input } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import style from "./style";
class ForgetPassword extends Component {
  render() {
    return (
      <View style={style.container}>
        <View>
          <Text style={[style.h1Dark]}>Forget{"\n"}Password ?</Text>
        </View>
        <View>
          <Text style={style.smTextGray}>
            Enter your email to receive your password
          </Text>
        </View>

        <View style={[style.formContainer]}>
          <View style={style.formContainer}>
            <Content>
              <Form>
                {/* <Text style={style.errMsg}>{errMsg}</Text> */}
                <Item>
                  <Icon
                    style={[style.formIcon]}
                    type="FontAwesome"
                    name="envelope-o"
                  />
                  <Input
                    style={[style.formText]}
                    placeholder="Email"
                    // onChange={this.handleChange}
                    // value={email}
                    // onBlur={this.Validator(email)}
                  />
                </Item>
              </Form>
            </Content>
          </View>
          <View style={style.buttonContainer}>
            <Button transparent onPress={this.handleSubmit}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 4 }}
                colors={["#efcc64", "#FFAA2F"]}
                style={style.linearGradient}
              >
                <Text style={style.buttonText}>Resent</Text>
              </LinearGradient>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

export default ForgetPassword;
