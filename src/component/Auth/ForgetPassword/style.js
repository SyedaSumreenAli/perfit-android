import { StyleSheet } from "react-native";
import { height, width } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingHorizontal: height(8),
    paddingVertical: height(13),
  },
  h1Dark: {
    fontFamily: "Montserrat-Bold",
    fontSize: 28.78,
    color: "#131415",
  },
  smTextGray: {
    color: "#888888",
    fontSize: 15.58,
    fontFamily: "Montserrat-Medium",
    paddingTop: height(1),
  },
  formContainer: {
    //  backgroundColor:'orange',
    width: width(70),
    // height:height(10),
    // marginBottom: height(5),
    flex: 1,
  },
  formIcon: {
    color: "#FDBE51",
    fontSize: 17.58,
    marginRight: width(3),
  },
  formText: {
    color: "#C0C0C0",
    fontFamily: "Montserrat-Medium",
    fontSize: 16.9,
  },
  errMsg: {
    color: "#EB1717",
    fontFamily: "Montserrat-Medium",
    fontSize: 15,
    marginLeft: width(2),
  },
  buttonContainer: {
    //  backgroundColor:"yellow",
    flex: 4,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 45,
    paddingRight: 45,
    opacity: 0.9,
    borderRadius: 25,
  },
  buttonText: {
    fontSize: 19.23,
    fontFamily: "Montserrat-Light",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
  },
});

export default style;
