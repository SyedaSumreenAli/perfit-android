import { StyleSheet } from "react-native";
import { height, width } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
  },
  logo: {
    marginTop:height(6),
    width: width(40),
    height: height(23),
    marginBottom:height(3)
    // zIndex:1.2
  },
  logoContainer: {
    flex: 1,
    // backgroundColor:"red",
    width: width(100),
    alignItems: "center",
    justifyContent: "center",
    
    height: height(35),
  },
  headingContainer: {
    // flex: 4,
    // backgroundColor:"orange",
    width: width(100),
    justifyContent: "center",
    alignItems: "center",
    height: height(20),
  },
  formContainer: {
  //  flex: 5,
  // flex:4,
 backgroundColor:"white",
    width: width(100),
    marginTop:height(6),
    // marginBottom:height(12),
    height:height(35),
    // overflow:"hidden",
    // justifyContent:"flex-end",
    alignItems: "center",
  //  height: height(27),
  },
  linkContainer: {
   // flex: 0.3,
  //  flex:2,
    height: height(30),
    // backgroundColor:"pink",
    width: width(100),
    alignItems: "center",
    justifyContent: "center",
  },
  buttomLinkContainer: {
    backgroundColor:"white",
    flex: 4,
    justifyContent: "flex-end",
    alignItems: "center",
    width: width(100),
  },

  h1Dark: {
    color: "#131415",
    fontFamily: "Montserrat-Bold",
    fontSize: 36,
    alignSelf: "flex-start",
  },
  h2Dark: {
    color: "#131415",
    fontFamily: "Montserrat-Bold",
    fontSize: 27,
    alignSelf: "flex-start",
  },
  subTextDark: {
    color: "#040404",
    fontFamily: "Montserrat-Medium",
    fontSize: 17,
    alignSelf: "flex-start",
  },
  width80: {
    width: width(80),
    // backgroundColor:"purple"
  },
  width100: {
    width: width(80),
    // backgroundColor:"gray",
    height: height(25),
  },
  buttonContainer: {
    // backgroundColor:"brown",
    justifyContent: "center",
    alignItems: "center",
    flex: 3,
    width: width(100),
  },
  linearGradient: {
    width: width(70),
    borderRadius: 25,
    alignItems: "center",
    paddingVertical: height(2),
  },
  buttonText: {
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    fontSize: 19,
  },
  formIcon: {
    color: "#FDBE51",
    fontSize: 20,
    marginRight: width(4),
  },
  formItem: {
    //// backgroundColor:"red",
    marginLeft: 0,
    // zIndex:4
    // flex:1,
    // width:width(100),
    // justifyContent:"flex-start",
    // alignSelf:"flex-start",
  },
  formText: {
    fontFamily: "Montserrat-Medium",
    color: "#c0c0c0",
    fontSize: 16,
  },
  subTextLight: {
    color: "#BDBDBD",
    fontFamily: "Montserrat-Medium",
    fontSize: 14.02,
    textAlign: "center",
  },
  emailSent: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14.23,
    color: "#BDBDBD",
    paddingTop: height(2),
    textAlign: "center",
  },
  flexRow: {
    flexDirection: "row",
  },
  lightText: {
    color: "#888888",
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
  },
  SignupLink: {
    color: "#888888",
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    marginTop: -height(1),
  },
  SignupLinkContainer: {
   
    flex: 1,
    alignItems: "flex-start",
    padding: 0,
  },
  flex2: {
    // flex:2,
    // backgroundColor:"yellow",
    height: height(7),
    justifyContent: "space-around",
  },
  errMsg: {
    paddingTop:height(2),
    color: "#EB1717",
    fontFamily: "Montserrat-Medium",
    fontSize: 14,
    marginLeft: width(2),
  },
});

export default style;
