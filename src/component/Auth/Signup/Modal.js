import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { connect } from "react-redux";


import CodeInput from 'react-native-confirmation-code-input';

import axios from 'axios'
import { height } from "react-native-dimension";

class VerifyCode extends Component {


    _onFulfill = (code) => {

        console.log(code)


        if (code == this.props.code.confirmation_code) {

            console.log("Verified")

            let email = this.props.code.email
            let token = this.props.code.confirmation_code

            axios.post(`http://system.perfitapp.com/api/verify/token?token=${token}&email=${email}`)
                .then((res) => {


                    console.log(res.data)

                    console.log(this.props)

                    this.props.toggle()

                    this.props.navigate()



                })


        }

        else {

            console.log("invalid code")
        }

    }

    render() {


        console.log(this.props)


        return (

            <View style={styles.modalContainer} >


                <View >
                    <Text style={{ color: "black",fontSize:22, fontFamily:'Montserrat-Medium' }}>Enter Confirmation Code :  </Text>
                </View>

                <View >





                    <CodeInput
                        ref="codeInputRef1"
                        codeLength={6}
                        
                        space={6}
                        size={45}
                        inputPosition='left'
                        onFulfill={(code) => this._onFulfill(code)}
                        containerStyle={{ marginTop: 30 }}
                        codeInputStyle={{ borderWidth: 1.5 }}
                        activeColor='rgba(218, 218, 47, 1)'
                        inactiveColor='rgba(218, 218, 47, 1)'
                        keyboardType="numeric"
                        className='border-circle'
                        codeInputStyle={{ color:"#c0c0c0" }}
                    />






                </View>
            </View>
        )
    }


}


const mapStateToProps = (state) => {

    return {

        code: state.AuthReducer
    }


}

const styles = StyleSheet.create({


    modalContainer: {

        backgroundColor: "white",
        color:"black",
        height: "40%",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: height(5),
        borderRadius: 10,


    }

})


export default connect(mapStateToProps)(VerifyCode);