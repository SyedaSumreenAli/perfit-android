import React, { Component } from "react";
import { ScrollView, View, Text, Image, ToastAndroid, Toast, ProgressBarAndroid } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import logo from "../../../assets/images/logo.png";
import { Button, Content, Form, Item, Input, Spinner } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import style from "./signupStyle";
import Icon from "react-native-vector-icons/FontAwesome";
import axios from 'axios'
import { height } from "react-native-dimension";
import { validateAll } from 'indicative/validator';

import VerifyCode from './Modal'

import { signUp } from '../../../redux/actions/AllAuthActions'

import { connect } from 'react-redux'

import Modal from 'react-native-modal';



class Signup extends Component {
  state = {
    name: "test",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
    isLoading: false,
    userData: '',
    errors: [],
    emailSent: false,
    modalOpen:false

  }

  navigate = () => {


    this.props.navigation.navigate("YourDetails");

  }

  toggleModel = () => {
    console.log('######### called model #############')
    this.setState({
      modalOpen: !this.state.modalOpen
    })

  }

  isLoading=()=>{

    this.setState({
      isLoading: !this.state.isLoading
    })

  }

  handleSubmit = async () => {
    const { name, email, username, password_confirmation, password, errors } = this.state

    const rules = {
      username: 'required|string|min:3',
      email: 'required|email',
      password: 'required|string|min:6|confirmed',
      password_confirmation: 'required'
    }

    const messages = {
      required: (field) => `${field} field is required`,
      'email.email': 'Invalid email address, please enter again',
      'password.confirmed': 'password does not match, please enter again',
      'password.min': 'password must be atleast 6 characters long',
      'username.min': 'username must be atleast 3 characters long'
    }

    try {
      await validateAll({ name, email, username, password_confirmation, password }, rules, messages)
     
     
      // this.setState({ isLoading: true, })

      this.isLoading()

      let user = {

        name: name,
        email: email,
        username: username,
        password: password,


      }

      this.props.signUp(user, this.toggleModel,this.isLoading)







    }
    catch (errors) {

      /************ VALIDATION ERRORS ***********/
      const formatedErrors = []
      errors.forEach(errors => formatedErrors.push(errors.message))
      console.log('$$$$$$$$$$$$$$$$$$$' + formatedErrors)
      this.setState({
        isLoading: false,
        errors: formatedErrors
      })

    }
  }



  render() {
    const { email, username, password, password_confirmation, errors, emailSent,modalOpen } = this.state

    console.log('####### error ########## ' + errors[0])
    return (
      <View style={style.container}>
        <View style={style.logoContainer}>
          <Modal isVisible={modalOpen} animationType="slide" >

            <VerifyCode
              toggle={this.toggleModel}
              navigate={this.navigate}



            />
          </Modal>
          <Image source={logo} style={style.logo} />
        </View>

        {/* <View style={[style.headingContainer]}>
            <View style={style.width80}>
              <Text style={[style.subTextDark]}>Signup to get</Text>
              <Text style={[style.h2Dark]}>Free Premium </Text>
              <Text style={[style.h1Dark]}>Training Course</Text>
            </View>
          </View> */}

        <View style={style.formContainer}>
          <View style={style.width100}>

            <Form style={{
              // zIndex:6,            
            }}>
              <Text style={style.errMsg}>{errors[0]}</Text>


              <Item style={style.formItem}>
                <Icon
                  style={[style.formIcon]}
                  type="FontAwesome"
                  name="user-o"
                />
                <Input
                  style={[style.formText]}
                  onChangeText={(text) => this.setState({ username: text })}
                  value={username}
                  placeholder="Name"
                  blurOnSubmit={false}
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    this.Email._root.focus();
                  }}
                />
              </Item>
              <Item style={style.formItem}>
                <Icon
                  style={[style.formIcon]}
                  type="FontAwesome"
                  name="envelope-o"
                />
                <Input
                  style={[style.formText]}
                  onChangeText={(text) => this.setState({ email: text })}
                  value={email}
                  placeholder="Email"
                  ref={input => {
                    this.Email = input;
                  }}
                  blurOnSubmit={false}
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    this.Password._root.focus();
                  }}
                />
              </Item>
              <Item style={style.formItem}>
                <Icon
                  style={[style.formIcon]}
                  type="FontAwesome"
                  name="key"
                />
                <Input
                  style={[style.formText]}
                  onChangeText={(text) => this.setState({ password: text })}
                  value={password}
                  placeholder="Password"
                  secureTextEntry={true}
                  ref={input => {
                    this.Password = input;
                  }}
                  blurOnSubmit={false}
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    this.password_confirmation._root.focus();
                  }}
                />
              </Item>
              <Item style={style.formItem}>
                <Icon
                  style={[style.formIcon]}
                  type="FontAwesome"
                  name="key"
                />
                <Input
                  style={[style.formText]}
                  onChangeText={(text) => this.setState({ password_confirmation: text })}
                  value={password_confirmation}
                  placeholder="Confirm Password"
                  secureTextEntry={true}
                  ref={input => {
                    this.password_confirmation = input;
                  }}
                />
              </Item>
            </Form>

          </View>
        </View>

        <View style={style.linkContainer}>
          <View style={style.buttonContainer}>
            {this.state.isLoading ? <Spinner color="#FCB044" /> :
              <Button transparent onPress={() => this.handleSubmit()}>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 4 }}
                  colors={["#efcc64", "#F29C1F"]}
                  style={style.linearGradient}
                >
                  <Text style={style.buttonText}>Sign Up</Text>
                </LinearGradient>
              </Button>
            }
          </View>

          <View
          // style={[style.LinkContainer, style.trainerScreenLinkContainer]}
          >
            <View>
              <Text style={[style.subTextLight]}>
                Information will be send to you in{" "}
              </Text>
            </View>
            <View>
              {this.state.isLoading ? <Text style={[style.emailSent]}>Email Sent</Text> : null}
            </View>
          </View>

          <View style={[style.buttomLinkContainer]}>
            <View style={[style.flex2, style.flexRow]}>
              <View>
                <Text style={[style.lightText]}>
                  Already have an account?{" "}
                </Text>
              </View>
              <View>
                <Button
                  transparent
                  style={[style.SignupLinkContainer]}
                  onPress={() => this.props.navigation.navigate("Login")}
                >
                  <Text style={[style.SignupLink]}>Sign in</Text>
                </Button>
              </View>
            </View>
          </View>
        </View>
      </View>

    );
  }
}


const dispatchStateToProps = (dispatch) => {


  return {

    signUp: (user, toggleModel,isLoading) => dispatch(signUp(user, toggleModel,isLoading))
  }

}


export default connect(null, dispatchStateToProps)(Signup);



