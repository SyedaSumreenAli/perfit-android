import React, { Component } from "react";
import { ScrollView, View, Text, TouchableOpacity } from "react-native";
import { Icon, Badge } from "native-base";
import Image from "react-native-scalable-image";
import style from "./style";

class Inbox extends Component {
  state = {
    inboxData: [],
  };

  loadInboxData = async () => {
    let inboxData = [
      {
        name: "Marrie Sutton",
        imageUri: require("../../assets/images/inbox/1.png"),
        notification: 1,
        message: "How do I start working out at the gym?",
        time: "5:45 PM",
        active: true,
      },
      {
        name: "Kenneth Drake",
        imageUri: require("../../assets/images/inbox/2.png"),
        notification: 1,
        message: "sure, … 😂",
        time: "11:13 AM",
        active: false,
      },
      {
        name: "Madge Santos",
        imageUri: require("../../assets/images/inbox/3.png"),
        notification: 2,
        message: "Sounds perfect to me.",
        active: false,
        time: "10:45 AM",
      },
      {
        name: "MDuane McDonald",
        imageUri: require("../../assets/images/inbox/4.png"),
        notification: 0,
        message: "The cost is too high. Can you ple.…",
        active: false,
        time: "8 : 06 AM",
      },
      {
        name: "Etta Parsons",
        imageUri: require("../../assets/images/inbox/5.png"),
        notification: 0,
        message: "an eight-week training prgress…",
        active: false,
        time: "Yesterday",
      },
      {
        name: "Lucile Marshail",
        imageUri: require("../../assets/images/inbox/6.png"),
        notification: 0,
        message: "Some people find it hard to find the right perfume….",
        active: false,
        time: "Yesterday",
      },
    ];

    this.setState({ inboxData: inboxData });
  };

  static navigationOptions = {
    headerShown: false,
  };

  componentDidMount = async () => {
    await this.loadInboxData();
  };

  render() {
    const { inboxData } = this.state;

    const Message = inboxData.map((data, index) => {
      return (
        <TouchableOpacity
          key={index}
           onPress={()=> this.props.navigation.navigate("Chat")}
          style={[
            style.flexRow,
            style.listItem,
            {
              borderBottomColor:
                index < inboxData.length - 1 ? "#D8D8D8" : "transparent",
            },
          ]}
        >
          <View style={style.imgOuterContainer}>
            <View style={style.imageContainer}>
              <Image source={data.imageUri} resizeMode="cover" />
              <Icon
                name="circle"
                type="FontAwesome"
                style={[
                  style.statusIcon,
                  {
                    color: data.active ? "#09DE06" : "transparent",
                  },
                ]}
              />
            </View>
          </View>
          <View style={style.textOuterContainer}>
            <Text style={style.nameText}>{data.name} </Text>
            <View>
              <Text style={style.message}>{data.message}</Text>
            </View>
          </View>

          <View style={[style.notificationContainer]}>
            {data.notification ? (
              <Badge style={style.badge}>
                <Text style={style.badgeText}>{data.notification}</Text>
              </Badge>
            ) : null}
            <Text style={style.time}>{data.time}</Text>
          </View>
        </TouchableOpacity>
      );
    });

    return (
      <ScrollView>
        <View style={style.container}>
          <View style={style.flexRow}>
            <View style={style.iconContainer}>
              <Icon name="arrowleft" type="AntDesign" style={style.icon} />
            </View>
            <View style={style.flex4}>
              <View style={style.headerContainer}>
                <Text style={style.headerText}>PT</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={style.listContainer}>{Message}</View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default Inbox;
