import React from 'react'
import {View, Text} from 'react-native'
import style from './style'

const SendMessage = (props) => {
    return(
       <View style={style.sendMessageOuterContainer}>
           <View style={style.sendMessageInnerContainer}>
                <Text style={style.sendMessageText}>{props.message}</Text>
           </View>
           <View>
                <Text style={style.sendMessageTime}>{props.time}</Text>
           </View>
       </View>
    )
}
export default SendMessage