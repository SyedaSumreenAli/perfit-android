import {StyleSheet} from 'react-native'
import { height, width } from 'react-native-dimension'

const style = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"white",
        alignItems:"center",
        // paddingTop:height(8),
        paddingBottom:height(10)
    },
    flexRow: { 
        flexDirection: "row",
       
    },
    iconContainer: {
      flex: 2,
    // backgroundColor:"orange",
      height: height(12),
      alignItems:"center",
        
      justifyContent: "center",
      
    },
    arrow: {
        marginLeft:width(4),
         width: width(30),
         alignSelf: "center",
        alignItems:"flex-start",
        // fontSize: 35,
        // backgroundColor:"green"
    },
    flex5: {
      flex: 4,
      alignItems: "flex-start",
      justifyContent: "center",
    },
  
    headerText: {
    //    backgroundColor:"red",
      flexDirection: "column",
      color: "#424242",
      fontSize: 20,
      textTransform: "uppercase",
      fontFamily: "Montserrat-SemiBold",
     
    },
    imageContainer:{
       borderRadius:100,
       overflow:"hidden",
       marginTop:height(4)
    },
    imgEditButton:{
        backgroundColor:"rgba(0,0,0,0.7)",
        position:"absolute",
        bottom:0,
        width:width(30),
       justifyContent:"center"
    },
    editText:{
        color:"#ffffff",
        fontSize:12,
        fontFamily:"Montserrat-SemiBold"
    },
    profileName:{
        color:"#333942",
        fontFamily:"Montserrat-Bold",
        fontSize:16,
        paddingTop:height(1)
    },
    list:{
        //  backgroundColor:"yellow",
        flex:1,
        width:width(90),
        alignItems:"flex-start"
    },
    icon:{
        fontSize:18,
        color:"#B2B2B2",
        marginRight:width(4),
        flexDirection:'column'
    },
    iconImg:{
        marginRight:width(2)
    },
    listText:{
        color:"#333942",
        fontFamily:"Montserrat-SemiBold",
        fontSize:16,
    },
    width90:{
        width:width(90)
    },
    flexRow:{
        flexDirection:"row",
    },

    listSubText:{
         alignSelf:"flex-start",
         fontFamily:"Montserrat-Medium",
         fontSize:16,
         color:"#333942"
       
    },
    subListContainer:{
        marginLeft:width(10),
        marginTop:height(2)
        //  backgroundColor:"yellow"
    },
    boldText:{
        fontFamily:"Montserrat-Bold",
        fontSize:14
    }
    
    
})
export default style