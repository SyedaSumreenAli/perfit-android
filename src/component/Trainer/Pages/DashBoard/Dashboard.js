import React, { Component } from 'react'
import {ScrollView, View, Image, TouchableOpacity} from 'react-native'
import {Text,Icon, Button,Content, List, ListItem} from 'native-base'
import dashboardUser from '../../../../assets/images/trainer/trainer.png'
import smartPayment from '../../../../assets/images/trainer/smartpayment.png'
import style from './style'
class TrainerDashboard extends Component{
    state={
        username:"Jorge olson",
        email:"Jorgeolson@live.com",
        contactNumber:"00-010069952040",
        userImage:dashboardUser,
        newMember:3,
        inProgress:4,
        Completed:5,
        total:"$450",
        received:"$250",
        inway:"$200"

    }
    render(){
        const {username,userImage, email, contactNumber, newMember, inProgress, Completed, total, received, inway} = this.state
        return(
            <ScrollView>
            <View style={style.container}>
                <View style={style.flexRow}>
                <TouchableOpacity 
                    style={style.iconContainer}
                    onPress={()=>{this.props.navigation.replace("Customer")}}
                    >
                   <View style={style.arrow}>
                        <Image 
                        // width={width(30)}
                        source={require('../../../../assets/images/trainer/arrow.png')}/>
                    </View>
                    </TouchableOpacity>
                    <View style={style.flex5}>
                    <View style={style.headerContainer}>
                        <Text style={style.headerText}>Dashboard</Text>
                    </View>
                    </View>
                </View>
                <View style={style.imageContainer}>
                <Image source={userImage}/>
                <Button transparent style={style.imgEditButton}>
                    <Text style={style.editText}>Edit</Text>
                </Button>
                </View>
                <View>
                    <Text style={style.profileName}>{username}</Text>
                </View>
                <View style={style.list}>
                <Content>
                    <List>
                        <ListItem>
                        <Icon style={style.icon}  type="FontAwesome" name="envelope-o"/>
                            <Text style={[style.listText, style.width90]} >{email}</Text>
                        </ListItem>
                        <ListItem>
                        <Icon style={style.icon} type="Feather" name="phone"/>   
                        <Text style={[style.listText, style.width90]} >{contactNumber}</Text>
                        </ListItem>
                        <ListItem>
                            <View>
                                <View style={style.flexRow}>
                                    <Icon style={[style.icon]} type="SimpleLineIcons" name="user"/> 
                                    <Text style={[style.listText, style.width90]}> Subscribers</Text>
                                </View>
                                <View style={style.subListContainer}>
                                    <View>
                                        <View style={style.flexRow}>
                                        <Text style={style.listSubText} >New Members:</Text>  
                                        <Text style={style.boldText}>  {newMember} </Text>
                                        </View>
                                    </View>
                                    <View>
                                        <View style={style.flexRow}>
                                        <Text style={style.listSubText} >InProgress: </Text>  
                                        <Text style={style.boldText}> {inProgress} </Text>
                                        </View>
                                    </View>
                                    <View>
                                      <View style={style.flexRow}>
                                        <Text style={style.listSubText} >Completed:</Text>   
                                        <Text style={style.boldText}> {Completed} </Text>
                                      </View>
                                    </View>
                                </View>
                              
                            </View>
                     
                            
                        </ListItem>
                        <ListItem last>
                        <View>
                                <View style={style.flexRow}>
                                <Image style={style.iconImg} source={smartPayment}/>
                                    <Text style={[style.listText]}> Payments</Text>
                                </View>
                                <View style={style.subListContainer}>
                                    <View>
                                        <View style={style.flexRow}>
                                        <Text style={style.listSubText} >Total: </Text>  
                                        <Text style={style.boldText}>  {total} </Text>
                                        </View>
                                    </View>
                                    <View>
                                        <View style={style.flexRow}>
                                        <Text style={style.listSubText} >Received: </Text>  
                                        <Text style={style.boldText}> {received} </Text>
                                        </View>
                                    </View>
                                    <View>
                                      <View style={style.flexRow}>
                                        <Text style={style.listSubText} >Inway: </Text>   
                                        <Text style={style.boldText}> {inway} </Text>
                                      </View>
                                    </View>
                                </View>
                              
                            </View>
                        </ListItem>
                    </List>
                </Content>
                </View>

              
            </View>
            </ScrollView>
        )
    }
}
export default TrainerDashboard