import { StyleSheet } from "react-native";
import { width, height, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    // backgroundColor:"yellow"
    backgroundColor: "white",
    flex: 1,
    justifyContent: "space-evenly",
    paddingBottom: height(10),
    // alignContent:"space-around"
  },
  flexRow: { flexDirection: "row" },
  iconContainer: {
    flex: 3,
    //  backgroundColor:"orange",
    height: height(15),
    justifyContent: "center",
  },
  icon: {
    //  backgroundColor:"green",
    width: width(30),
    alignSelf: "center",
    alignItems:"flex-start",
    fontSize: 35,
  },
  flex4: {
    flex: 4,
    alignItems: "flex-start",
    justifyContent: "center",
  },

  headerText: {
    // backgroundColor:"red",
    flexDirection: "column",
    color: "#424242",
    fontSize: 20,
    fontFamily: "Montserrat-SemiBold",
  },

  list: {
    backgroundColor: "#F6F6F6",
    // height:height(12),
  },
  imgOuterContainer: {
    //  backgroundColor:"yellow",
    flex: 2.2,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  textOuterContainer: {
    //  backgroundColor:"skyblue",
    flex: 7,
    justifyContent: "center",
  },

  imageContainer: {
    position: "absolute",
    borderColor: "#6D7278",
    borderWidth: width(0.2),
    borderRadius: 50,
  },
  nameText: {
    fontFamily: "Montserrat-SemiBold",
    color: "#000000",
    textTransform: "capitalize",
    fontSize: totalSize(2),
  },
  time: {
    color: "#161F3D",
    fontFamily: "Montserrat-Regular",
    fontSize: totalSize(1.2),
  },

  listItem: {
    // borderBottomColor:"#D8D8D8",
    borderWidth: width(0.1),
    borderColor: "transparent",
    justifyContent: "center",
    paddingVertical: height(3),
    flex: 1,
    height: height(12),
    width: width(90),
    alignSelf: "center",
  },

  statusIcon: {
    fontSize: totalSize(1.8),
    // backgroundColor:"yellow",
    justifyContent: "flex-end",
    alignSelf: "flex-end",
    marginTop: height(6),
    color: "#09DE06",
    position: "absolute",
  },
  message: {
    color: "#161F3D",
    fontFamily: "Montserrat-Regular",
    fontSize: totalSize(1.6),
  },
  notificationContainer: {
    justifyContent: "space-around",
    alignItems: "center",
  },
  badge: {
    backgroundColor: "#EBA70F",
    alignSelf: "center",
    justifyContent: "center",
    width: width(5),
    height: width(5),
  },
  badgeText: {
    color: "#ffffff",
    fontSize: totalSize(1.2),
    alignSelf: "center",
  },
});

export default style;
