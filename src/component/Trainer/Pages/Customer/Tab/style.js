import {StyleSheet} from 'react-native'
import {width , height, totalSize} from 'react-native-dimension'


const style = StyleSheet.create({
  tabStyle:{
    backgroundColor:"white",
    height:height(10),
    borderBottomWidth: 0
}  ,

activeTabStyle:{
    backgroundColor:"white",
     height:height(10)
},
activeTabTextStyle:{
    color:"#000",
    fontFamily:"Montserrat-SemiBold",
    fontSize: totalSize(2)
},
tabTextStyle:{
  color:"#000",
    fontFamily:"Montserrat-SemiBold",
    fontSize: totalSize(2)
},
tabBarUnderlineStyle:{
 height:height(1),
  backgroundColor:"#FCB345",
  // borderColor:"white",
  // borderWidth: 0
  //  marginTop:height(3)
},
tabContainerStyle:{
   height:height(10),
  justifyContent:"flex-end",
  alignItems:"flex-end",
  backgroundColor:"red",
  elevation:0.4,
  // marginBottom:height(6),
   borderBottomColor:"#ECE9E9",
   borderWidth:width(1)
 
},

tabContainer:{
  position:"absolute",
  backgroundColor:"white",
  borderWidth:width(0)
 
}

})

export default style