import React, { Component } from 'react';
import {View, Text, ScrollView, ToucableOpacity} from 'react-native';
import { Container, Header,Badge, Content, Tab, Tabs } from 'native-base';
import MyCustomer from '../MyCustomer/MyCustomer'
import Request from '../Request/Request'
import style from './style'
import { height } from 'react-native-dimension';


 class TabBar extends Component {
    static navigationOptions = {
        headerShown: false,
      };
   
     

  render() {
    return (
      <Container style={style.tabContainer}>
        
          <Tabs 
          tabContainerStyle={style.tabContainerStyle}
           tabBarUnderlineStyle={style.tabBarUnderlineStyle} 
           >
            <Tab heading="MY CUSTOMERS"
              tabStyle={style.tabStyle}
               activeTabStyle={style.activeTabStyle}  
               textStyle={style.tabTextStyle}
                activeTextStyle={style.activeTabTextStyle }
                >

                <MyCustomer />
            </Tab>
          
            <Tab heading="REQUESTS" 
              tabStyle={style.tabStyle}
               activeTabStyle={style.activeTabStyle} 
                textStyle={style.tabTextStyle} activeTextStyle={style.activeTabTextStyle}>
                {/* <Dietitian/> */}
                {/* <Badge style={{
                  position:"absolute",
                  alignSelf:"flex-end",
                  marginTop:-height(10)
                }} >
                  <Text>2</Text>
                </Badge> */}
                <Request />
            </Tab>
          </Tabs>
       </Container>
   
    
    );
  }
}

export default TabBar