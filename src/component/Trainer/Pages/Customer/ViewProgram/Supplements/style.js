import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    container:{
         flex:1,
          marginTop:height(2),
         height:height(100),
        // backgroundColor:"skyblue"
      },
    weekSlider:{
        backgroundColor:"#FDC14A",
        flexDirection:"row",
        justifyContent:"space-around",
        alignItems:"center",
        height:height(6)
    },
    whiteText:{
        color:"white",
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.5)
    },
    daysContainer:{
        backgroundColor:"#F7F5F5",
        height:height(10),
        paddingHorizontal:width(12),
        flexDirection:"row",
        justifyContent:"space-around",
        alignItems:"center"
    },
    dayButton:{
        backgroundColor:"transparent",
        borderRadius:50,
        width:width(10),
        height:width(10),
        justifyContent:"center",
        alignItems:"center"
    },
    dayText:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.6)
    },
    currentDay:{
        alignSelf:"center",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.4)
    },
    currentDayContainer:{
        //   flex:1,
    //  backgroundColor:"yellow",
        justifyContent:"center",
        alignItems:"center",
        height:height(10)
    },

    h2Container:{
        // backgroundColor:"yellow",
        //   flex:1,
          height:height(8),
        //   flex:1,
         justifyContent:"flex-end",
         paddingBottom:height(1.5),
         borderWidth:width(0.2),
         borderBottomColor:"#D8D8D8",
         borderLeftColor:"transparent",
         borderRightColor:"transparent",
         borderTopColor:"transparent"
    },
    exerciseContainer:{
        //   backgroundColor:"red",
         height:height(12),
        //  flex:4,
        flexDirection:"row",
        // position:"absolute"
         alignItems:"center"
       
    },
exerciseContainer1:{
        //    position:"absolute",
        //    backgroundColor:"orange",
           height:height(80),
           marginBottom:height(20),
        //  justifyContent:"flex-start",
        width:width(90),
        alignSelf:"center",
         flex:1,
         marginTop:-height(83)
    },
    exImgContainer:{
          flex:2,
        // backgroundColor:"red",   
        alignItems:"center",
        // paddingTop:height(1),
        // justifyContent:"flex-end",
        //  backgroundColor:"yellow",
    },
    youtubeIconContainer:{
        position:"absolute",
        height:width(15),
        overflow:"hidden",
        justifyContent:"center",
        alignItems:"center"
    },
    
    youtubeIcon:{
        color:"red", 
        alignSelf:"center"
    },
    checkIconContainer:{
        position:"absolute",
        height:width(6),
        // backgroundColor:"yellow",
        width:width(10),
        // opacity:0.5,
        overflow:"hidden",
        justifyContent:"center",
        alignItems:"flex-start"
    },
    
    checkIcon:{
        color:"white",
        fontSize:totalSize(2)
         
        
    },
    textContainer:{
        flex:8,
        
         height:height(12),
        //  paddingTop:-height(2),
        justifyContent:"center",
         alignItems:"center",
        flexDirection:"row",
          borderWidth:width(0.2),
         borderColor:"transparent",
         marginTop:-height(2),
        //  backgroundColor:"orange"
        //  borderBottomColor:"#d8d8d8",
        //    paddingBottom:height(4),
        //   marginBottom:height(4)
    },
    exDescriptionContainer:{
         flex:4,  
         marginTop:-height(1)
        //  backgroundColor:"red"
    },
    h1:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.6)
    },
    exSwtichContainer:{
         flex:2,
         alignItems:"flex-end",
         justifyContent:"flex-start",
         marginTop:-height(6)
        //  borderWidth:width(0.2),
        //  borderColor:"transparent",
        //  borderBottomColor:"#d8d8d8"
        //  height:height(10),
        //  backgroundColor:"skyblue"
    },

    h2:{
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(2)
    },
    p:{
        color:"#828282",
        fontFamily:"Montserrat-Regular",
        fontSize:totalSize(1.2)
    },
     weightText:{
         color:"#FFAA00",
         fontFamily:"Montserrat-Medium",
         fontSize:totalSize(1.2)
     }
})

export default style