import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    container:{
         flex:1,
          marginTop:height(2),
         height:height(100),
        // backgroundColor:"skyblue"
      },
    weekSlider:{
        backgroundColor:"#FDC14A",
        flexDirection:"row",
        justifyContent:"space-around",
        alignItems:"center",
        height:height(6)
    },
    whiteText:{
        color:"white",
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.5)
    },
    daysContainer:{
        backgroundColor:"#F7F5F5",
        height:height(10),
        paddingHorizontal:width(12),
        flexDirection:"row",
        justifyContent:"space-around",
        alignItems:"center"
    },
    dayButton:{
        backgroundColor:"transparent",
        borderRadius:50,
        width:width(10),
        height:width(10),
        justifyContent:"center",
        alignItems:"center"
    },
    dayText:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.6)
    },
    currentDay:{
        alignSelf:"center",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.4)
    },
    currentDayContainer:{
        //    flex:1,
    //  backgroundColor:"yellow",
        justifyContent:"center",
        alignItems:"center",
         height:height(10)
    },

    h2Container:{
        // backgroundColor:"yellow",
        //   flex:1,
        //   height:height(8),
          flex:1,
         justifyContent:"flex-end",
         paddingBottom:height(1.5),
         borderWidth:width(0.2),
         borderBottomColor:"#D8D8D8",
         borderLeftColor:"transparent",
         borderRightColor:"transparent",
         borderTopColor:"transparent"
    },
    mealContainer:{
    //    backgroundColor:"red",
        //  height:height(12),
           flex:1,
        flexDirection:"row",
        // position:"absolute"
         alignItems:"center",
         justifyContent:"center"
       
    },
    exerciseContainer1:{
        //    position:"absolute",
        //    backgroundColor:"orange", 
           marginBottom:height(50),
        //  justifyContent:"flex-start",
        width:width(90),
        alignSelf:"center",
         flex:1,
         marginTop:-height(80)
    },
    exImgContainer:{
          flex:2,
        // backgroundColor:"red",   
        alignItems:"center",
        // backgroundColor:"yellow",
    },
    youtubeIconContainer:{
        position:"absolute",
        height:width(15),
        overflow:"hidden",
        justifyContent:"center",
        alignItems:"center"
    },
    
    youtubeIcon:{
        color:"red", 
        alignSelf:"center"
    },
    checkIconContainer:{
        position:"absolute",
        height:width(6),
        // backgroundColor:"yellow",
        width:width(10),
        // opacity:0.5,
        overflow:"hidden",
        justifyContent:"center",
        alignItems:"flex-start"
    },
    
    checkIcon:{
        color:"white",
        fontSize:totalSize(2)
         
        
    },
    textContainer:{
        flex:6,
        //  height:height(8),
        paddingTop:height(2),
        justifyContent:"center",
        alignItems:"center",
        flexDirection:"row",
          borderWidth:width(0.2),
         borderColor:"transparent",
        //  borderBottomColor:"#d8d8d8",
          paddingBottom:height(2),
          marginBottom:height(2)
    },
    exDescriptionContainer:{
         flex:3,
        
        // backgroundColor:"yellow"
    },
    exSwtichContainer:{
         flex:2,
         alignItems:"flex-end",
        //  borderWidth:width(0.2),
        //  borderColor:"transparent",
        //  borderBottomColor:"#d8d8d8"
        //  height:height(10),
        //  backgroundColor:"skyblue"
    },

    h2:{
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(2)
    },
    h3:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2)
    },
    p:{
        fontSize:totalSize(1.6),
        fontFamily:"Montserrat-Regular",
        color:"#828282"
    }
})

export default style