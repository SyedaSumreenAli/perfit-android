import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    body:{
        backgroundColor:"#fff",
        height:height(100)
        // flex:1
    },
    crossIconContainer:{
        position:"absolute",
        // backgroundColor:"red",
        width:width(100),
        height:height(10),
        justifyContent:"flex-end",
        alignItems:"flex-end",
        paddingRight:width(2)
    },
    content:{
        backgroundColor:"#fff",
        width:width(90),
        alignSelf:"center",
        borderRadius:width(4),
        paddingHorizontal:width(5),
        paddingBottom:height(4),
        paddingTop:height(4),
        //  height:height(100),
        // position:"absolute",
         justifyContent:"flex-end"
    },
    flexRow:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    badge:{
        backgroundColor:"#FCB345",
        justifyContent:"center",
        height:height(3.5)
        
    },
    badgeText:{
        fontFamily:"Montserrat-Medium",
        color:"#fff",
        paddingHorizontal:width(2)
    },
    h1:{
       fontFamily:"Montserrat-Bold",
       color:"#000",
       fontSize:totalSize(2.5),
       marginBottom:height(3)
    },
    h2:{
        fontFamily:"Montserrat-SemiBold",
        color:"#000",
        fontSize:totalSize(2) ,
        marginBottom:height(2),
        marginTop:height(2)
     },
     descriptionText:{
         color:"#828282",
         fontFamily:"Montserrat-Regular",
         fontSize:totalSize(1.8),
         width:width(75)
        //  marginBottom:height(4)
     },
     card:{
         backgroundColor:"#f2f2f2",
         padding:width(2),
         borderRadius:width(2)
     },
     p:{
         fontFamily:"Montserrat-Medium",
         color:"#000",
         fontSize:totalSize(1.6)

     },
    contentContainer:{
        // backgroundColor:"red",
        position:"absolute",
        justifyContent:"flex-end",
         height:height(100),
         overflow:"visible",
        width:width(100)
    }
})

export default style