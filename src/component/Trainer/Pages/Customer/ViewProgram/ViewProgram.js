import React, {Component} from 'react'
import {View, Text, TouchableOpacity,Image} from 'react-native'
import {Icon, Thumbnail, Button} from 'native-base'
//import Image from 'react-native-scalable-image'
import Tab from './Tab/Tab'
import style from './style'
import { height, width } from 'react-native-dimension'

class ViewProgram extends Component{

    state={
        userImage:require('../../../../../assets/images/trainer/customer/1.png'),
        username:"Etta Parson",
    }

    static navigationOptions = {
        headerShown: false,
      };


    render(){
   const{ userImage, username}= this.state
    return(
        <View style={style.container}>
             
                  {/******* HEADER COMPONENT START ******* */}
                  <View style={style.headerContainer}> 
                    <View  style={[style.flex1, style.center]}>
                        <TouchableOpacity
                            onPress={()=>{this.props.navigation.goBack()}}
                        >
                           <Image source={require('../../../../../assets/images/trainer/arrow.png')}/>
                        </TouchableOpacity>
                    </View>
                    <View style={[style.flex5, style.center]}>
                        <Text style={style.headerText}> my customers </Text>
                    </View>
                    <View style={[style.flex1,style.flexEnd, style.pr2]}>
                        <TouchableOpacity
                             onPress={()=>{this.props.navigation.goBack()}}
                        >
                            <Image source={require('../../../../../assets/images/trainer/menu.png')}/>
                           
                        </TouchableOpacity>
                    </View>
                </View>
                    {/******* HEADER COMPONENT END ******* */}

                    {/******** CUSTOMER INFO CARD START *******/}

                <View style={style.infoContainer}>
                    <View style={style.ci1}>
                        <Thumbnail  source={userImage} style={style. imageContainer}/>
                        {/* <View style={style.progressTextContainer}>
                            <Text style={style.progressText}>Progress</Text>
                        </View> */}
                   </View>
                   <View style={style.ci2}>
                       <View style={style.r1}>
                            <Text style={style.name}>{username}</Text>
                            
                            <View style={[style.flexRow, style.dateContainer]}>
                               
                                <View style={[style.borderRight]}>
                                    <View>
                                        <Text style={style.smallMedium}>Start Date</Text>
                                    </View>
                                    <View style={style.flexRow}>
                                        <Icon style={style.dateIcon} name="calendar" type="Fontisto"/>
                                        <Text style={style.dateText}>12/04/2020</Text>
                                    </View>
                                </View>
                                <View>
                                    <View>
                                        <Text style={style.smallMedium}>End Date</Text>
                                    </View>
                                    <View style={style.flexRow}>
                                        <Icon style={style.dateIcon} name="calendar" type="Fontisto"/>
                                        <Text style={style.dateText}>12/04/2020</Text>
                                    </View>
                                </View>

                            </View>
                    <View>
                        <View style={style.progressContainer}>
                            {/* <Text style={style.h4Medium}> Payment Status:<Text style={style.paymentStatus}>  Done</Text> </Text> */}
                            <View style={style.progressBar}>
                                <View style={[style.progress, { width:width(30)}]}></View>
                            </View>
                        </View>

                    </View>

                       </View>
                       <View style={style.r2}>
                           <Button iconLeft light style={style.msgButton}>
                               <Icon style={style.btnIcon}  name="email" type="Fontisto"/>
                               <Text style={style.btnText}>Message</Text>
                           </Button>
                           <Button iconLeft light style={style.startBtn}>
                           <Icon style={style.btnIcon}  name="phone-call" type="Feather"/>
                               <Text style={style.btnText}>Call</Text>
                           </Button>
                       </View>
                   </View>

                </View>

                    {/******** CUSTOMER INFO CARD END *******/}


        
            <View style={style.tabContainer}>
                <Tab/>
            </View>
        </View>
    )
          
    }
}

export default ViewProgram