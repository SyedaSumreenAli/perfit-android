import {StyleSheet} from 'react-native'
import {height, width, totalSize} from 'react-native-dimension'
const style = StyleSheet.create({
    container:{
        
        // paddingBottom:height(10),
         flex:1,
        alignItems:"center",
        width:width(100),
        // backgroundColor:"skyblue",
        alignSelf:"center",
        // height:height(100),
        backgroundColor:"white",
        paddingBottom:height(10)
    },
    headerContainer:{
        flexDirection:"row",
         backgroundColor:"#F8F8F8",
        //  flex:1,
        // backgroundColor:"white",
        alignItems:"center",
        height:height(12),
        justifyContent:'space-around',
        paddingHorizontal:width(3),
        alignSelf:"center"
    },
    flex1:{
        flex:1,
        //  backgroundColor:"gray"
    },
    center:{
        justifyContent:"center",
        alignItems:"center"
    },
    flexEnd:{
        alignItems:"flex-end"
    },
    flex2:{
        flex:2,
         // backgroundColor:"red"    
    },
    flex5:{
        flex:5,
         // backgroundColor:"red"    
    },
    userContainer:{
        //   flex:1,
        // height:height(40),
        // backgroundColor:"skyblue"
    },
    flexRow:{
        flexDirection:"row"
    },

    headerText:{
        color:"#424242",
        // fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(2.4),
        textTransform:"capitalize"
    },
    pr2:{
        paddingRight:width(2)
    },

    /*** Linear Gradient Button Start */

    linearGradient: {
        //  flex: 1,
        // paddingLeft: 45,
        // paddingRight: 45,
        // opacity: 0.9,
        paddingHorizontal:width(4),
        borderRadius: 25,
      },
      buttonText: {
        fontSize: totalSize(2.2),
        fontFamily: "Montserrat-Bold",
        textAlign: "center",
        margin: 10,
        color: "#ffffff",
        paddingHorizontal:width(3),
        textTransform:"uppercase"
      },
      /*** Linear Gradient Button End */



    imageContainer:{
        // borderRadius:100,
        // overflow:"hidden"
     },
 
    h1:{
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.2),
        color:"#000"
    },
    headingContainer:{
        marginTop:height(4)
    },
    h4:{
        textAlign:"center",
        fontFamily:"Montserrat-SemiBold",
        marginTop:height(1),
        fontSize:totalSize(1.8)
    },
    chartContainer:{
        width:width(95),
        flex:1,
        // backgroundColor:"yellow",
       alignItems:"center"
    },
    // flexRow:{
        // flexDirection:"row",
        // justifyContent:"space-between",
        // flex:1,
        // paddingVertical:height(5)

    // },
    c1:{
        
         alignItems:"center",
         justifyContent:"center",
         flex:1,
    },
    semiBold:{
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(1.5),
        textTransform:"uppercase"
    },
    medium:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.5),
        alignSelf:"center"
    },
    mediumSmall:{
        fontSize:totalSize(1.8),
        fontFamily:"Montserrat-Medium",
        textAlign:"center",
        alignSelf:"center",
        justifyContent:"flex-start",
        marginTop:-height(2)
    },
    boldLarge:{
        alignSelf:"center",
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(3.2)
    },
    infoContainer:{
        backgroundColor:"#f8f8f8",
        width:width(100),
        // flex:1,
        height:height(28),

        flexDirection:"row",
        justifyContent:"center",
        // alignItems:"center"
        //  paddingBottom:height(10)
    },
    ci1:{
        //  backgroundColor:"gray",
        flex:1,

        alignItems:"center",     
    },
    ci2:{
        flex:3,
        
    },
    name:{
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.6),
        paddingBottom:height(1)
    },
    status:{
        fontSize:totalSize(2),
        fontFamily:"Montserrat-SemiBold",
        color:"#AAAAAA"
    },
    msgButton:{
        backgroundColor:"#FCB345",
        justifyContent:"center",
        width:width(32),
        height:height(4),
        alignItems:"center",
        borderRadius:width(20),
        paddingRight:width(4),
     marginTop:height(1)
    },
    btnText:{
        color:"#fff",
        fontSize:totalSize(1.6)
    },
    btnIcon:{
        fontSize:totalSize(1.6),
        marginRight:width(1)
    },
    startBtn:{
        backgroundColor:"#E04725",
        justifyContent:"center",
        // width:width(30),
        height:height(4),
        alignItems:"center",
        borderRadius:width(20),
        paddingRight:width(4),
        marginLeft:width(2),
        marginTop:height(1) 
    },
    r2:{
        flexDirection:"row",
         flex:1,
         justifyContent:"flex-end",
         alignItems:"flex-start",
         paddingRight:width(6),
        // alignItems:"center",
        //   backgroundColor:"yellow"
    },

    h1Medium:{
        fontSize:totalSize(2.4),
        fontFamily:"Montserrat-Medium",
        // alignSelf:"center"
    },
    smallMedium:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.6),
        paddingBottom:height(1),
        // backgroundColor:"gray",
        // width:width(30)
    },
    borderRight:{
       
        borderWidth:width(0.2),
        borderLeftColor:"transparent",
        borderTopColor:"transparent",
        borderBottomColor:"transparent",
        borderRightColor:"#939495",
        paddingRight:width(1),
        marginRight:width(5)
    },
    dateIcon:{
        color:"#939495",
        fontSize:totalSize(1.6),
         paddingRight:width(2),
          marginTop:height(0.2),
    },
    dateText:{
        // marginTop:-height(5.5),
        color:"#939495",
        fontSize:totalSize(1.6),
       
        // backgroundColor:"red"   
    },
    p3:{
        padding:width(1),
        paddingRight:width(10),
        // backgroundColor:"green"
    },
    p3nl:{
        paddingRight:width(10),
        paddingVertical:width(1),
        // backgroundColor:"red"

    },

    progressBar:{
        backgroundColor:"#D8D8D8",
        height:height(1.3),
        borderRadius:4,
        marginRight:width(5),
        marginTop:height(1),
         
    },
    progressContainer:{
        // backgroundColor:"yellow",
        //   flex:2,
        overflow:"hidden",
         marginTop:height(2),
        marginBottom:height(2),
        width:width(74)
    },
    progress:{
        backgroundColor:"#FCB345",
        height:height(1.3), 
        borderRadius:4
    },
    h4Medium:{
        fontFamily:"Montserrat-Medium",
        color:"#26315F",
        fontSize:totalSize(2)
    },
    dateContainer:{
        // backgroundColor:"yellow",
        //  width:width(30),
        // //   height:height(10),
        //   paddingTop:-height(4),  
        //   justifyContent:"flex-start",
        //   alignItems:"flex-start",
        //   paddingBottom:height(6)
       
    },
    paymentStatus:{
        color:"#10CA88",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(2.2),
      
    },
    progressTextContainer:{
        // backgroundColor:"yellow",
        justifyContent:"flex-end",
        height:height(12)
    },
    progressText:{
        fontFamily:"Montserrat-Medium",
        color:"#161F3D",
        fontSize:totalSize(2)
    },

    tabContainer:{
        position:"absolute",
        width:width(100),
        marginTop:height(40)  ,  }


})

export default style