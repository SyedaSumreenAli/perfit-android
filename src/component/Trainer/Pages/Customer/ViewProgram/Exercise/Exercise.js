import React ,{Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import Image from 'react-native-scalable-image'
import {Icon, Button, Item} from "native-base"
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import ToggleSwitch from 'toggle-switch-react-native'
import { width } from 'react-native-dimension'
import {withNavigation} from 'react-navigation'

class Exercise extends Component{
    state={
        days:[],
        exercisePlan:[],
        currentDay:"Monday"
    }
    loadDays = () => {
        let days=[
            {
                day:"Sunday",
                label:"S",
            },
            {
                day:"Monday",
                label:"M",
            },
            {
                day:"Tuesday",
                label:"T",
            },
            {
                day:"Wednesday",
                label:"W",
            },
            {
                day:"thursday",
                label:"T",
            },
            {
                day:"Friday",
                label:"F",
            },
            {
                day:"Saturday",
                label:"S",
            },
           
        ];

        this.setState({ days })
    }

    loadExercisePlan = () => {
        let exercisePlan = [
            {
                title:" Morning Plan",
                exercises:[
                   { name:"Chest – Barbell Bench Press – 4 sets of 8 reps.",
                    imageUri: require('../../../../../../assets/images/exercises/1.png'),
                    status: true
                   },
                   { name:"Back – Lat-pulldowns – 4 sets of 10 reps.",
                    imageUri:require('../../../../../../assets/images/exercises/2.png'),
                    status: false
                   },
                   { name:"Legs – Leg Extensions – 4 sets of 10 reps.",
                    imageUri:require('../../../../../../assets/images/exercises/3.png'),
                    status: false
                   },
            ]
            },
            {
                title:" Evening Plan",
                exercises:[
                   { name:"Chest – Barbell Bench Press – 4 sets of 8 reps.",
                    imageUri: require('../../../../../../assets/images/exercises/1.png'),
                    status: true
                   },
                   { name:"Back – Lat-pulldowns – 4 sets of 10 reps.",
                    imageUri:require('../../../../../../assets/images/exercises/2.png'),
                    status: false
                   },
                   { name:"Legs – Leg Extensions – 4 sets of 10 reps.",
                    imageUri:require('../../../../../../assets/images/exercises/3.png'),
                    status: false
                   },
            ]
            }
        ]

        this.setState({
           exercisePlan
        })
    }


  
    renderDays=()=>{
        const {days, currentDay} = this.state

         let day =  days.map((day, index) =>{
                  return  <TouchableOpacity 
                            key={index}
                            style={[style.dayButton,{
                                backgroundColor: currentDay== day.day ? "#979797" : "transparent",
                            }]}
                            
                            onPress={ () => { this.handleCurrentDay(day.day)}}
                            >
                        <Text  style={[style.dayText, {
                            color: currentDay == day.day ? "white": "#000"
                        }]}>{day.label}</Text>
                    </TouchableOpacity>
            })

            return day
        
    }

    renderExercisePlan = () => {
        const {exercisePlan} = this.state
        
        console.log(exercisePlan)
        let exercises = exercisePlan.map(item=>{
            console.log(item.title)
            if(item.title ==''||null)
                return ''
            else{ 
                 return <View>
                <View style={style.h2Container}>
                    <Text style={style.h2}>{item.title}</Text>
                </View>
                { item.exercises.map(sub => {
                   return(
                     <View style={style.exerciseContainer}>
                        <TouchableOpacity style={style.exImgContainer}
                            onPress={()=>{this.props.navigation.navigate("TrainerExerciseVideo")}}
                        >
                            <Image source={sub.imageUri} width={width(15)}/>
                            <View style={style.youtubeIconContainer}>
                                <Icon name="youtube" type="AntDesign" style={style.youtubeIcon}/>

                            </View>
                        </TouchableOpacity>
                        <View style={style.textContainer}>
                        <View style={style.exDescriptionContainer}>
                            <Text>{sub.name}</Text>
                        </View>
                        <View style={style.exSwtichContainer}>
                         
                        <ToggleSwitch
                            isOn={sub.status}
                            onColor="#2FD475"
                            offColor="#B2B2B2"
                            size="medium"
                            style={{width:width(20)}}
                            onToggle={isOn => console.log("changed to : ", isOn)}
                            />
                           <View style={style.checkIconContainer}>
                                <Icon name="check" type="AntDesign" style={style.checkIcon}/>

                            </View>  
                         </View>   
                        
                        </View>
                    </View> )
                }
            )}
               </View>
              
            
        }})

        return exercises

    }

   componentDidMount= async()=>{
       return( 
          await  this.loadDays(),
          await  this.loadExercisePlan()
       )
    }

    handleCurrentDay= (day) => {
        const {currentDay} = this.state
        this.setState({currentDay: day})
    }
 
    render(){
        const {currentDay} = this.state
        return(
            <ScrollView>
            <View style={style.container}>
                <View style={style.weekSlider}>
                    <View>
                        <Icon style={style.whiteText} name="left" type="AntDesign"/>
                    </View>
                    <View>
                        <Text  style={style.whiteText} > Week 1</Text>
                    </View>
                    <View>
                        <Icon  style={style.whiteText}  name="right" type="AntDesign"/>
                    </View>
                </View>

                <View style={style.daysContainer}>
                    {this.renderDays()}     
                </View>
                
            </View>
            <View style={style.exerciseContainer1}>
                
              {this.renderExercisePlan()}
            </View>
            </ScrollView>
        )
    }
}

export default withNavigation(Exercise) 