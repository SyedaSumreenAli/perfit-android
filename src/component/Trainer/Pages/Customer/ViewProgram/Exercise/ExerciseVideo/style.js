import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    body:{
        backgroundColor:"#000",
        flex:1
    },
    header:{
        flexDirection:"row",
        justifyContent:"space-between",
        marginHorizontal:width(6),
        marginTop:height(4)
    },
    icon:{
        color:"#fff"
    },
    text:{
        color:"#fff",
        fontFamily:"Montserrat-Medium",
        textAlign:"center",
        fontSize:totalSize(1.8)
    }
    
});

export default style