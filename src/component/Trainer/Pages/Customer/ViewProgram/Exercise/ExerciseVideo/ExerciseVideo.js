import React, { Component } from 'react'
import {View, TouchableOpacity, Text} from 'react-native'

import {Icon} from 'native-base'
import Image from 'react-native-scalable-image'
// import Video from 'react-native-af-video-player'
import style from './style'
import { width, height } from 'react-native-dimension'


class ExerciseVideo extends Component{

    static navigationOptions = {
      headerShown: false
    }
  
    // static navigationOptions = ({ navigation }) => {
    //     const { state } = navigation
    //     // Setup the header and tabBarVisible status
    //     const header = state.params && (state.params.fullscreen ? undefined : null)
    //     const tabBarVisible = state.params ? state.params.fullscreen : true
    //     return {
    //       // For stack navigators, you can hide the header bar like so
    //       header,
    //       // For the tab navigators, you can hide the tab bar like so
    //       tabBarVisible,
    //     }
    // }
    // onFullScreen(status) {
    //     // Set the params to pass in fullscreen status to navigationOptions
    //     this.props.navigation.setParams({
    //       fullscreen: !status
    //     })
    //   }
    
    //   onMorePress() {
    //     Alert.alert(
    //       'Boom',
    //       'This is an action call!',
    //       [{ text: 'Aw yeah!' }]
    //     )
    //   }
    render(){
        // const url = 'https://your-url.com/video.mp4'
        // const logo = 'https://your-url.com/logo.png'
        // const placeholder = 'https://your-url.com/placeholder.png'
        // const title = 'My video title'
        return(
            <View style={style.body}>
                <View style={style.header}>
                   <TouchableOpacity>
                       <Image source={require('../../../../../../../assets/images/exercises/fullscreen-exit.png')}/>
                   </TouchableOpacity>
                   <TouchableOpacity 
                      onPress={()=>{this.props.navigation.goBack()}}
                   >
                       <Image source={require('../../../../../../../assets/images/exercises/close.png')}/>
                   </TouchableOpacity>
                </View> 
               <View style={{ 
                 backgroundColor:"#f2f2f2",
                 width:width(90), 
                 height:height(35),
                 alignSelf:"center",
                 justifyContent:"center",
                 marginTop:height(5),
                 marginBottom:height(8),
                 alignItems:"center"}}>
                 <Text> Video component will display here</Text>
               {/* <Video
                    autoPlay
                    url={url}
                    title={title}
                    logo={logo}
                    placeholder={placeholder}
                    onMorePress={() => this.onMorePress()}
                    onFullScreen={status => this.onFullScreen(status)}
                    fullScreenOnly
                    /> */}
               </View>
              
                <Text style={style.text}>Chest – Barbell Bench Press – 4 sets of 8 reps.</Text>
            </View>
        )
    }
}
export default ExerciseVideo