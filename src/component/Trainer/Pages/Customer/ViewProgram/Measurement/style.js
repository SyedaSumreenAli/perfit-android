import { StyleSheet } from "react-native";
import { width, height, totalSize } from "react-native-dimension";

const style = StyleSheet.create({
  container: {
    // flex: 1,
     height:height(100)+height(20),
    // backgroundColor:"red",
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  imgContainer:{
    //   flex:1,
    marginVertical:height(4),
      justifyContent:"center"
  },
  linearGradient: {
    //  flex: 1,
    // paddingLeft: 45,
    // paddingRight: 45,
    // opacity: 0.9,
    paddingHorizontal:width(4),
    borderRadius: 25,
  },
  buttonText: {
    fontSize: totalSize(2),
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    textTransform:"uppercase"
  },

})

export default style