import {StyleSheet} from "react-native"
import {height, width, totalSize} from "react-native-dimension"


const style = StyleSheet.create({
    container:{
         backgroundColor:"white",
        flex:1,
        // paddingTop:height(4),
        width:width(100),
        //  height:height(100),
        alignSelf:"center",
        alignItems:"center",
        //  position:"absolute"
    },
    h1:{
        fontFamily:"Montserrat-Bold",
        fontSize:totalSize(2.4)
    },
    h1Container:{height:height(15)},
    imgContainer:{
        justifyContent:"center",
        alignItems:"center",
          position:"absolute",
        // flex:1,
        marginTop:height(10),
        //  backgroundColor:"orange",
        width:width(100),
        // height:height(100)
    },
    form:{
        flex:4,
        width:width(90),
    //   marginTop:height(10)
        
    },
    flexRow:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    r1:{
        height:height(3.5),
        borderWidth:width(0.2),
        justifyContent:"center",
         alignItems:"center",
        borderRadius:20, 
        borderColor:"#d8d8d8",      
        // backgroundColor:"skyblue",
        width:width(20),
        flex:1,
        alignSelf:"flex-end",
        marginTop:-height(4),
        marginRight:height(6),  
       
      
    },
    r2:{
         width:width(24),
        height:height(3.5),
        borderWidth:width(0.2),
         alignItems:"center",
         justifyContent:"center",
        borderRadius:20, 
        borderColor:"#d8d8d8",      
        //   backgroundColor:"yellow",
        marginTop:height(3)
    },
       
    r3:{
         width:width(18),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"center",
        justifyContent:"center",
        borderRadius:20, 
        borderColor:"#d8d8d8",    
        marginTop:height(1),  
        //   backgroundColor:"blue"
    },
    r4:{
         width:width(24),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"flex-start",
        justifyContent:"flex-start",
        textAlign:"left",
        borderRadius:20, 
        borderColor:"#d8d8d8",      
        //    backgroundColor:"orange",
        // alignSelf:"flex-end",
          marginTop:height(1),
        //   marginLeft:-width(2),
          zIndex:6

    },
    r5:{
         width:width(24),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"flex-start",
        justifyContent:"flex-start",
        borderRadius:20, 
        borderColor:"#d8d8d8",   
        // marginTop:height(-6),
       
        //   backgroundColor:"gray"
    },
    r6:{
        
         width:width(26),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"flex-start",
        justifyContent:"flex-start",
        borderRadius:20, 
        borderColor:"#d8d8d8", 
        marginTop:-height(2), 
        // backgroundColor:"skyblue"
    },
    r7:{
         width:width(26),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"flex-start",
        justifyContent:"center",
        borderRadius:20, 
        borderColor:"#d8d8d8",      
        //  backgroundColor:"yellow" 
    },
    r8:{
         width:width(18),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"stretch",
        borderRadius:20, 
        borderColor:"#d8d8d8",  
         marginTop:height(1)  , 
         marginLeft:height(3),  
        //   backgroundColor:"blue"
    },
    r9:{
         width:width(18),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"stretch",
        borderRadius:20, 
        borderColor:"#d8d8d8", 
        alignSelf:"flex-end",
        marginRight:height(4),    
        //  backgroundColor:"orange"
    },
    r10:{
         width:width(22),
        height:height(3.5),
        borderWidth:width(0.2),
        alignItems:"stretch",
        borderRadius:20, 
        borderColor:"#d8d8d8", 
        marginTop:-height(4),
        marginLeft:height(4),  
        //   backgroundColor:"gray"
    },
    r11:{
        width:width(22),
       height:height(3.5),
       borderWidth:width(0.2),
       alignItems:"stretch",
       borderRadius:20, 
       marginTop:-height(2),
       borderColor:"#d8d8d8",
       marginRight:height(4),      
       //  backgroundColor:"gray"
   },
   r12:{
    width:width(22),
   height:height(3.5),
   borderWidth:width(0.2),
   alignItems:"stretch",
   borderRadius:20, 
   borderColor:"#d8d8d8",    
    marginTop:-height(2),
    marginLeft:height(5), 
   //  backgroundColor:"gray"
},
r13:{
    width:width(22),
   height:height(3.5),
   borderWidth:width(0.2),
   alignItems:"stretch",
   borderRadius:20, 
   borderColor:"#d8d8d8",  
   marginTop:-height(2),
   marginRight:height(5),      
   //  backgroundColor:"gray"
},
r14:{
    width:width(22),
   height:height(3.5),
   borderWidth:width(0.2),
   alignSelf:"center",
   borderRadius:20, 
   borderColor:"#d8d8d8", 
   marginTop:height(12)    

   //  backgroundColor:"gray"
},

    picker:{
        height:height(3),  
        borderColor:"white",
        alignItems:"center",
        justifyContent:"center",
        zIndex:4
      
        // backgroundColor:"white",
    },
    pickerItem:{
        fontSize:totalSize(1.4),
        fontFamily:"Montserrat-Bold",
        zIndex:4,
        alignSelf:"center",
       
    },
    forearm:{
        marginTop:height(3)
    },
    thigh:{
        marginTop:height(6)
    },
    calf:{
        marginTop:height(9)
    },
    button:{
        backgroundColor:"#D8D8D8",
        justifyContent:"center",
        height:height(5),
        width:width(60),
        borderWidth:width(0.2),
        alignSelf:"center",
        borderRadius:20, 
        borderColor:"#d8d8d8", 
        marginTop:height(3)
    },
    buttonMedium:{
        backgroundColor:"#D8D8D8",
        justifyContent:"center",
        height:height(4),
         width:width(40),
        borderWidth:width(0.2),
        alignSelf:"center",
        borderRadius:20, 
        borderColor:"#d8d8d8",
        marginBottom:height(4) 
        // marginTop:height(6),
    },
    buttonText:{
      fontFamily:"Montserrat-Regular",
      textTransform:"lowercase"
    },
    line:{
        color:"#D8D8D8"
    },
    headContainer:{
        // backgroundColor:"yellow",
       flex:4,
       justifyContent:"center",
       alignItems:"flex-end",

    },
    headIndicator:{
         width:width(14),
         overflow:"hidden",
         height:height(3),
        //  backgroundColor:"pink"
    },
    shoulderContainer:{
        // backgroundColor:"gray",
        width:width(14),
        height:height(3),
        overflow:"hidden"
    },
    shoulderIndicator:{
        //  backgroundColor:"pink",
        flex:1,
        justifyContent:"flex-end",
        overflow:"hidden"
    },
    chestContainer:{
        // backgroundColor:"gray",
          width:width(40),
         height:height(3),
         overflow:"hidden"
    },
    chestIndicator:{
        //  backgroundColor:"orange",
        flex:1,
        justifyContent:"flex-end",
        overflow:"hidden"
    },
    BisepIndicator:{
        // backgroundColor:"skyblue",
        justifyContent:"center",
        // flex:1,
        overflow:"hidden"
    },
    BisepContainer:{
        width:width(12),
         height:height(3),
         overflow:"hidden"
    },
    leftForearmIndicator:{
        //   backgroundColor:"skyblue",
        justifyContent:"center",
        // flex:1,
        overflow:"hidden",
        
    },
    leftForearmContainer:{
        width:width(12),
        // height:height(3),
        // overflow:"hidden",
        marginTop:-height(6),
    },
    rightForearmIndicator:{
        //    backgroundColor:"skyblue",
        justifyContent:"center",
        //  flex:1,
        overflow:"hidden",
        
    },
    rightForearmContainer:{
        width:width(12),
         height:height(3),
         overflow:"hidden",
        // marginTop:height(6),
    },
    waistIndicator:{
            // backgroundColor:"skyblue",
         justifyContent:"center",
          flex:1,
        overflow:"hidden",
        
    },
    waistContainer:{
         width:width(12),
         height:height(3),
          overflow:"hidden",
        //   backgroundColor:"yellow"
        // marginTop:height(6),
    },
    hipIndicator:{
    //  backgroundColor:"skyblue",
     justifyContent:"center",
     alignItems:"flex-end",
      flex:1,
    overflow:"hidden",
    
    },
    hipContainer:{
        width:width(10),
        height:height(3),
        overflow:"hidden",
        //   backgroundColor:"yellow"
        // marginTop:height(6),
    },
    leftThighIndicator:{
            backgroundColor:"skyblue",
        //  justifyContent:"center",
         flex:1,
        //  overflow:"hidden",
        
    },
    leftThighContainer:{
        width:width(7),
         height:height(3),
         overflow:"hidden",
        // marginTop:height(6),
        marginTop:-height(4),
        // backgroundColor:"yellow",
    },
    rightThighIndicator:{
        //    backgroundColor:"skyblue",
        //  justifyContent:"center",
        //   flex:1,
        //  overflow:"hidden",
        
    },
    rightThighContainer:{
        width:width(7),
         height:height(3),
         overflow:"hidden",
         flex:1,
        //  backgroundColor:"yellow",
        marginTop:-height(2),
    },
    leftCalfIndicator:{
        //    backgroundColor:"skyblue",
        //  justifyContent:"center",
        //   flex:1,
        //  overflow:"hidden",
        
    },
    leftCalfContainer:{
        width:width(7),
         height:height(3),
         overflow:"hidden",
         flex:1,
        //  backgroundColor:"yellow",
        marginTop:-height(2),
    },
    rightCalfIndicator:{
        //    backgroundColor:"skyblue",
        //  justifyContent:"center",
        //   flex:1,
        //  overflow:"hidden",
        
    },
    rightCalfContainer:{
        width:width(6),
         height:height(3),
         overflow:"hidden",
         flex:1,
        //  backgroundColor:"yellow",
        marginTop:-height(2),
    },

    crossContainer:{
        // /  flex:1,
        // backgroundColor:"yellow",
        height:height(10),
        width:width(100),
        zIndex:4,
        alignItems:"flex-end",
        justifyContent:"flex-start"
    },
    crossText:{
        fontFamily:"Montserrat-Light",
        fontSize:totalSize(6),
        alignItems:"flex-end",
        alignSelf:"flex-end",
        zIndex:4,
        paddingRight:width(2),
        paddingTop:height(3)
        
    },
    linearGradient: {
        //  flex: 1,
        // paddingLeft: 45,
        // paddingRight: 45,
        // opacity: 0.9,
        // paddingHorizontal:width(4),
        borderRadius: 25,
        width:width(40),
        alignSelf:"center",
        marginTop:height(2)
      },
      buttonText: {
        fontSize: totalSize(2),
        fontFamily: "Montserrat-Bold",
        textAlign: "center",
         marginVertical: height(1),
        color: "#ffffff",
        textTransform:"uppercase"
      },

})

export default style