import React, { Component } from 'react';
import {View, Text, ScrollView, ToucableOpacity} from 'react-native';
import { Container, Header, Content, Tab, Tabs , ScrollableTab} from 'native-base';
import Exercise from '../Exercise/Exercise'
import MealPlan from '../MealPlan/MealPlan'
import Supplements from '../Supplements/Supplements'
import Measurement from '../Measurement/Measurement'
import style from './style'


 class TabBar extends Component {
    // static navigationOptions = {
    //     headerShown: false,
    //   };
   

            
  render() {
    return (
      <Container style={style.tabContainer}>
        
        <Tabs
         renderTabBar={ ()=> <ScrollableTab 
            style={{ backgroundColor:"white"}}
            tabContainerStyle={{ borderBottomWidth: 0, }}
            underlineStyle={style.underlineStyle} />
         }>
          
            
            <Tab heading="EXERCISES"  
                tabStyle={style.tabStyle}
                activeTabStyle={style.activeTabStyle} 
                textStyle={style.tabTextStyle} 
                activeTextStyle={style.activeTabTextStyle}
              >
                <Exercise/>
            </Tab>
            <Tab heading="MEAL PLAN"  
                tabStyle={style.tabStyle}
                activeTabStyle={style.activeTabStyle} 
                textStyle={style.tabTextStyle} 
                activeTextStyle={style.activeTabTextStyle}
              >
                <MealPlan/>
            </Tab>
            <Tab heading="MEASUREMENTS"  
                tabStyle={style.tabStyle}
                activeTabStyle={style.activeTabStyle} 
                textStyle={style.tabTextStyle} 
                activeTextStyle={style.activeTabTextStyle}
              >
                <Measurement/>
            </Tab> 
             <Tab heading="SUPPLEMENTS"  
                tabStyle={style.tabStyle}
                activeTabStyle={style.activeTabStyle} 
                textStyle={style.tabTextStyle} 
                activeTextStyle={style.activeTabTextStyle}
              >
                <Supplements />
            </Tab>
          

           
          </Tabs>
       </Container>
   
    
    );
  }
}

export default TabBar