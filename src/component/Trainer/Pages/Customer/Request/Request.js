import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import style from './style'
import { ScrollView } from 'react-native-gesture-handler';

class Request extends Component {
    state = {
        request:[]
    }

    loadRequest = () =>{
        let data = [
            {
                name:"Etta Parsons",
                msg:"How can I join the training",
                imgUri:require('../../../../../assets/images/trainer/customer/1.png')
            },
            {
                name:"Duane McDonald",
                msg:"Can I be  stronger than ever before with these intense yet-inspiring motivational quotes that are sure to take your fitness to the next level.",
                imgUri:require('../../../../../assets/images/trainer/customer/2.png')
            },
            {
                name:"Etta Parsons",
                msg:"How can I join the training",
                imgUri:require('../../../../../assets/images/trainer/customer/3.png')

            }
        ];
        this.setState({request:data})
    }
    componentDidMount = () =>{
         this.loadRequest()
    }
  render() {
      const {request} =this.state
      console.log(request)
      
      let requestList = request.map((item, index)=>{

        return (
            <ListItem avatar 
            style={[
                style.ListItem,
                {
                    borderBottomColor:  request.length-1 == index ?"transparent" : "#d8d8d8"
                }
             ] }>
              <Left style={style.avatarContainer}>
                <Thumbnail source={item.imgUri} />
              </Left>
              <Body style={style.Body}>
                <Text>{item.name}</Text>
                <Text note>{item.msg}</Text>
              </Body>
            </ListItem>
        )

      })

    return (
        <ScrollView>
            <Container style={style.Container}>
                <Content>
                <List>
                    {requestList}
                </List>
                </Content>
            </Container>
      </ScrollView>
    );
  }
}

export default Request