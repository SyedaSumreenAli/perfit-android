import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
     Container:{
         width:width(90),
         alignSelf:"center"
     },
      ListItem:{
          borderWidth:width(0.2),
          width:width(100),
          borderColor:"transparent",
          borderBottomColor:"#d8d8d8"
      },
      Body:{
          borderBottomColor:"transparent"
      },
      avatarContainer:{
          justifyContent:"center",
        //   backgroundColor:"yellow",
          alignItems:"center",
          paddingBottom:height(2)
      }

})

export default style