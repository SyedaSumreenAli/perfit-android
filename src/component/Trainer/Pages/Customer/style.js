import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'

const style = StyleSheet.create({
    body:{
        backgroundColor:"white",
         flex:1,
        width:width(100)
    },
    headerContainer:{
        flexDirection:"row",
        // backgroundColor:"yellow",
        // flex:1,
        backgroundColor:"white",
        alignItems:"center",
        height:height(12),
        justifyContent:'space-around'
    },
   
    flexRow:{
        flexDirection:"row"
    },
    flexColumn:{
        flexDirection:"column"
    },
    flex1:{
        flex:1,
        //  backgroundColor:"gray"
    },
    center:{
        justifyContent:"center",
        alignItems:"center"
    },
    flexEnd:{
        alignItems:"flex-end"
    },
    flex2:{
        flex:2,
         // backgroundColor:"red"    
    },
    flex5:{
        flex:5,
         // backgroundColor:"red"    
    },
    flex6:{
        flex:6,
         // backgroundColor:"red"    
    },
    flex7:{
        flex:7,
         // backgroundColor:"red"    
    },
    flex8:{
        flex:8,
         // backgroundColor:"red"    
    },
    flex9:{
        flex:9,
        //   backgroundColor:"red"    
    },
    flex4:{
        flex:4,
        //   backgroundColor:"red"    
    },
    h1:{
        color:"#424242",
        fontFamily:"Montserrat-SemiBold",
        fontSize:totalSize(2.4),
        textTransform:"capitalize"
    },
    h2:{
        fontFamily:"Montserrat-Medium",
        color:"#000",
        fontSize:totalSize(3)
    },
    pr2:{
        paddingRight:width(2)
    },
    pl1:{
        paddingLeft:width(1)
    },
    pl2:{
        paddingLeft:width(2)
    },
    pl3:{
        paddingLeft:width(3)
    },
    pb1:{
        paddingBottom:height(1)
    },
    pb5:{
        paddingBottom:height(5),
        // backgroundColor:"pink"
    },
    pb3:{
        paddingBottom:height(3),
        // backgroundColor:"pink"
    },
    pb2:{
        paddingBottom:height(2),
        // backgroundColor:"pink"
    },
    pv1:{
        paddingVertical:height(1)
    },
    pv2:{
        paddingVertical:height(3)
    },
    lg6:{
        height:height(7)
    },
    userInnerContainer:{
        //  backgroundColor:"yellow",
        justifyContent:"center",
        alignItems:"center",
        marginLeft:height(5),
        marginBottom:height(4)
    },
    iconStar:{
        color:"#FDC900"
    },
    
    fs16:{
        fontSize:totalSize(1.6)
    },
    fs18:{
        fontSize:totalSize(1.8)
    },
    fs2:{
        fontSize:totalSize(2),
    },
    fs24:{
        fontSize:totalSize(4),
    },
    mb_04:{
      marginBottom:-height(0.4)  
    },
    button:{
        backgroundColor:"#FCB345",
        borderTopLeftRadius:width(20),
        borderBottomLeftRadius:width(20),
        marginTop:-height(8)
    },
    reviewText:{
        fontFamily:"Montserrat-Medium",
        textDecorationLine:"underline"
    }
    


}) 

export default style