import React, { Component } from 'react'
import {View, Text, Dimensions, TouchableOpacity} from 'react-native'
import {Button, Thumbnail, Icon} from 'native-base'
import {width, height} from 'react-native-dimension'
import Image from 'react-native-scalable-image'
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import { LineChart, Grid, XAxis, YAxis} from 'react-native-svg-charts'
import Measurement from "../Measurement/Measurement"
import { Circle, Path } from 'react-native-svg'
import LinearGradient from "react-native-linear-gradient";
// import CircularProgress from './ProgressCircle/CircularProgress'
import ProgressCircle from 'react-native-progress-circle'


class NewCustomer extends Component{

    state={
        userImage:require('../../../../../assets/images/trainer/customer/2.png'),
        username:"William Wood",
        email:"Ahmedmoftah@live.com",
        progressPercent:40,
        weightLoss:98
    }

    render(){
        const{ userImage, username, email, progressPercent, weightLoss}= this.state
        const data = [50, 30, 40 , 45]
        const axesSvg = { fontSize: 10, fill: 'grey' };
        const verticalContentInset = { top: 10, bottom: 10 }
        const xAxisHeight = 30
        
        const Decorator = ({ x, y, data }) => {
            return data.map((value, index) => (
                <Circle
                    key={ index }
                    cx={ x(index) }
                    cy={ y(value) }
                    r={ 4 }
                    stroke={ '#FCB345' }
                    strokeWidth={width(1.6)}
                    fill={ '#FCB345' }
                />
            ))
        }
    return(
        
        <ScrollView>
        <View style={style.container}>

                  {/******* HEADER COMPONENT START ******* */}
                  <View style={style.headerContainer}> 
                    <View  style={[style.flex1, style.center]}>
                        <TouchableOpacity
                            onPress={()=>{this.props.navigation.goBack()}}
                        >
                           <Image source={require('../../../../../assets/images/trainer/arrow.png')}/>
                        </TouchableOpacity>
                    </View>
                    <View style={[style.flex5, style.center]}>
                        <Text style={style.headerText}> my customers </Text>
                    </View>
                    <View style={[style.flex1,style.flexEnd, style.pr2]}>
                        <TouchableOpacity
                             onPress={()=>{this.props.navigation.navigate("TrainerDashboard")}}
                        >
                            <Image source={require('../../../../../assets/images/trainer/menu.png')}/>
                           
                        </TouchableOpacity>
                    </View>
                </View>
                    {/******* HEADER COMPONENT END ******* */}

                    {/******** CUSTOMER INFO CARD START *******/}

                <View style={style.infoContainer}>
                    <View style={style.ci1}>
                        <Thumbnail large source={userImage} style={style. imageContainer}/>
                   </View>
                   <View style={style.ci2}>
                       <View style={style.r1}>
                            <Text style={style.name}>{username}</Text>
                            <Text style={style.status}>New Member</Text>
                       </View>
                       <View style={style.r2}>
                           <Button iconLeft light style={style.msgButton}>
                               <Icon style={style.btnIcon}  name="email" type="Fontisto"/>
                               <Text style={style.btnText}>Message</Text>
                           </Button>
                           <Button style={style.startBtn}>
                               <Text style={style.btnText}>Start Plan</Text>
                           </Button>
                       </View>
                   </View>

                </View>

                    {/******** CUSTOMER INFO CARD END *******/}

              
                <View>
                    
                    <View style={style.headingContainer}>
                        <Text style={style.h1}>Weight Loss Progress</Text>
                        <Text style={style.h4}>{progressPercent}% Completed</Text>
                    </View>
                </View>
                <View style={style.flexRow}>
                    <View style={style.c1}>
                        <Text style={style.semiBold}>Start</Text>
                        <Text style={style.medium}>110 KG</Text>
                    </View>
                    <View  style={style.c2}>
                    <ProgressCircle
                            percent={0}
                            radius={width(18)}
                            borderWidth={8}
                            color="#FCB345"
                            shadowColor="#D8D8D8"
                            bgColor="#fff"
                        ><View style={style.centerText}>
                            <Text style={style.mediumSmall}>current</Text>
                            <Text style={style.boldLarge}>101 KG</Text>
                        </View>
                            
                        </ProgressCircle>
                    </View>
                    <View style={style.c1}>
                    <Text style={style.semiBold} >Target</Text>
                        <Text style={style.medium}>85 KG</Text>
                    </View>
                </View>
                <View style={{ height: height(50),   width:width(90), padding: 20, flexDirection: 'row' }}>
                <YAxis
                    data={data}
                    style={{ marginBottom: xAxisHeight }}
                    contentInset={verticalContentInset}
                    svg={axesSvg}
                />
                <View style={{ flex: 1, marginLeft: 10,  }}>
                    <LineChart
                        style={{ flex: 1 }}
                        data={data}
                        contentInset={verticalContentInset}
                        svg={{ stroke: '#FCB345', strokeWidth:width(1.2) }}
                    >
                        <Grid/>
                        <Decorator/>
                    </LineChart>
                    <XAxis
                        style={{ marginHorizontal: -10, height: xAxisHeight }}
                        data={data}
                        formatLabel={(value, index) => index}
                        contentInset={{ left: 10, right: 30 }}
                        svg={axesSvg}
                    />
                </View>
            </View>
              
               
       
            <TouchableOpacity transparent 
            onPress={()=>{this.props.navigation.navigate("UpdateMeasurement")}}
            >
            <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 4 }}
                colors={["#efcc64", "#FFAA2F"]}
                style={style.linearGradient}
            >
                <Text style={style.buttonText}>View Program</Text>
            </LinearGradient>
            </TouchableOpacity>

               
             
               
             
        </View>
        </ScrollView>
    )}
        
}
export default NewCustomer