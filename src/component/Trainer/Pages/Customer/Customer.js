import React, { Component }  from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import Image from 'react-native-scalable-image'
import {Icon, Button} from 'native-base'
import TabBar from './Tab/Tab'
import style from './style'
class Customer extends Component{
    state={
        trainer:{
            name:'Jorge Olson',
            imgUri:require('../../../../assets/images/trainer/trainer.png'),
            rating: 5,
            reviews:20
        }
    }

    // loadTrainerData = () =>{
    //    let data={
    //         name:'Jorge Olson',
    //         imgUri:'../../../../assets/images/trainer/trainer.png',
    //         rating: 5,
    //         reviews:20
    //     }

    //     this.setState({ trainer: data })
    // }

    // componentDidMount = async() =>{
    //  await this.loadTrainerData()
    //    console.log(this.state.trainer)
    // }

    render(){
        const {name, imgUri, rating, reviews} = this.state.trainer
        return(
            <View style={style.body}>
                    {/******* HEADER COMPONENT START ******* */}
                <View style={style.headerContainer}> 
                    <View  style={[style.flex1, style.center]}>
                        <TouchableOpacity
                            onPress={()=>{this.props.navigation.goBack()}}
                        >
                           <Image source={require('../../../../assets/images/trainer/arrow.png')}/>
                        </TouchableOpacity>
                    </View>
                    <View style={[style.flex5, style.center]}>
                        <Text style={style.h1}> my customers </Text>
                    </View>
                    <View style={[style.flex1,style.flexEnd, style.pr2]}>
                        <TouchableOpacity
                             onPress={()=>{this.props.navigation.navigate("TrainerDashboard")}}
                        >
                            <Image source={require('../../../../assets/images/trainer/menu.png')}/>
                           
                        </TouchableOpacity>
                    </View>
                </View>
                    {/******* HEADER COMPONENT END ******* */}
                    
                    {/******* USER COMPONENT START ******* */}
                <View>
                    <View style={style.userContainer}>
                        <View style={style.flexRow}>
                            <View style={[
                                style.flex9, 
                                style.flexColumn,
                                 style.center,
                                //  style.pl3
                                // style.flexEnd
                                 ]}>
                            <View style={style.userInnerContainer}>
                                <View style={style.pb2}>
                                    <Image source={imgUri}/>
                                </View>
                                <View style={style.pb2}>
                                    <Text style={style.h2}>{name}</Text>
                                </View>
                                <View style={[style.flexRow]}>
                                    <View style={[style.flexRow, style.flexEnd, style.pr2]}>
                                        <Icon name="star" type="AntDesign"  style={[style.iconStar ,style.fs16]}/>
                                        <Icon name="star" type="AntDesign"  style={[style.iconStar ,style.fs16]}/>
                                        <Icon name="star" type="AntDesign"  style={[style.iconStar ,style.fs16]}/>
                                        <Icon name="star" type="AntDesign"  style={[style.iconStar ,style.fs16]}/>
                                        <Icon name="star" type="AntDesign"  style={[style.iconStar ,style.fs16]}/>
                                    </View>
                                    <TouchableOpacity
                                        onPress={()=>{this.props.navigation.navigate("TrainerReviews")}}
                                    >
                                        <Text style={[
                                            style.fs16,
                                            style.flexEnd,
                                            style.mb_04,
                                            style.reviewText
                                        ]}>Review({reviews})</Text>
                                    </TouchableOpacity>
                            </View>

                                </View>
                                

                            </View>
                            <View style={style.flex2, style.center}>
                                <Button style={[style.button, style.pl3, style.lg6]}
                                       onPress={()=>{this.props.navigation.navigate("TrainerInbox")}}
                                >
                                    <Icon  style={style.fs24} name="email" type="Fontisto"/>
                                </Button>
                            </View>
                        </View>
                    </View>

                </View>
                   {/******* USER COMPONENT END ******* */}
                   {/******* TAB COMPONENT START ******* */}

                <View>
                    <TabBar/>
                </View>
                    
                   {/******* TAB COMPONENT END ******* */}
            </View>

        );
    }
}

export default Customer