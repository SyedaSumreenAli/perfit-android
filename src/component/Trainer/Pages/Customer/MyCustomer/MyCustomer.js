import React, { Component } from 'react'
import {Badge,Thumbnail,Content, Button, Icon } from 'native-base'
import{View,Text , TouchableOpacity} from 'react-native'
import {width, height} from 'react-native-dimension'
import style from './style'
import { ScrollView } from 'react-native-gesture-handler'
import {withNavigation} from 'react-navigation'

class MyCustomer extends Component{
    state={
        customer:[]
    }
    loadCustomer = async() =>{
         let data = [
           {
                name:"Etta Parsons",
                imgUri: require('../../../../../assets/images/trainer/customer/1.png'),
                plan:"1 Month",
                startDate:"12/04/2020",
                endDate:"12/05/2020",
                status:"in progress"
           },
           {
                name:"William Wood",
                imgUri: require('../../../../../assets/images/trainer/customer/2.png'),
                plan:"1 Month",
                status:"new member"
            },
            {
                name:"Violet Santos",
                imgUri: require('../../../../../assets/images/trainer/customer/3.png'),
                plan:"1 Month",
                startDate:"12/04/2020",
                endDate:"12/05/2020",
                status:"completed"
           },
         ]

         this.setState({ customer: data})

    }
    // renderCustomerCard=()=>{
    //     const {customer} = this.state
     
    //     customer.map((item,index)=>{
    //         console.log(item.name)
    //         return <View style={{
    //              position:"relative",
    //              backgroundColor:"red",
    //               justifyContent:"flex-end",
    //               alignItems:"flex-end",
    //                width:width(100),
    //                height:height(12)
    //             }}>
    //             <Text>{item.name}</Text>

    //         </View>
    //     })
    
        
    // }
    handlePage = (status) => {
        console.log(status)
        if (status == "in progress")
          return this.props.navigation.navigate("CustomerDetails")
        if ( status == "new member")
            return this.props.navigation.navigate("NewCustomer")
       
    }

    componentDidMount=() =>{
         this.loadCustomer()
    }
    render(){
        const {customer} = this.state
        
        let  rendercustomer=  customer.map((item,index)=>{
             
                return (
                    <TouchableOpacity style={style.card}
                        onPress={()=>{this.handlePage(item.status)}}
                    >
                       
                        <View style={style.thumbnailContainer}>
                            <View style={style.thumbnailInnerContainer}>
                            <Thumbnail source={item.imgUri}/>
                            </View>             
                        </View>

                        <View style={style.description}>
                            <View style={style.r1}>
                                <View style={style.c1}>
                                    <View>
                                        <Text style={style.name}>{item.name}</Text></View>
                                            <View style={style.pt2}><Text style={style.plan}>Plan: 
                                            <Text style={style.planText}>  {item.plan}</Text> 
                                        </Text>
                                    </View>
                                </View>
                                <View style={style.c2}>
                             
                                        <Badge style={[style.center1,
                                        {
                                            backgroundColor: item.status == "in progress" ? "#10CA88" :
                                            item.status == " new member" ? "#FCB345" :
                                            item.status == "completed" ? "#E04725" :"#FCB345"
                                        }]}>
                                            <Text style={style.status}>{item.status}</Text>
                                        </Badge>
                              
                                </View>
                            </View>
                            {item.status =="new member" ?
                            
                            <View style={style.ButtonContainer}>
                                <Button style={style.Button}>
                                    <Text style={style.ButtonText}>  Start Plan </Text>
                                </Button>
                            </View> :
                            <View style={style.r2}>
                              <View style={style.startDate}>
                                    <View><Text style={style.dateText}>Start Date</Text></View>
                                    <View style={style.flexRow}>
                                        <Icon style={style.dateIcon}  name="calendar" type="EvilIcons"/>
                                        <Text style={style.date}>{item.startDate}</Text>
                                    </View>
                                </View>
                                <View  style={style.endDate}>
                                    <View><Text>End Date</Text></View>
                                    <View style={style.flexRow}>
                                        <Icon style={style.dateIcon} name="calendar" type="EvilIcons"/>
                                        <Text style={style.date}>{item.endDate}</Text>
                                    </View>
                                </View>
                                  
                            </View>}
                        </View>
                        <View style={[style.lineContainer,{
                               backgroundColor: item.status == "in progress" ? "#10CA88" :
                               item.status == " new member" ? "#FCB345" :
                               item.status == "completed" ? "#E04725" :"#FCB345"
                        }]}/>
                    </TouchableOpacity>
                )
            })
        








        return(
            <ScrollView
             scrollToOverflowEnabled={true}
             >
                <View 
                style={style.body}>
                    {rendercustomer}
                </View>
            </ScrollView>
        )
    }
}

export default withNavigation( MyCustomer )