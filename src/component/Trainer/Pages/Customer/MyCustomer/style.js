import {StyleSheet} from 'react-native'
import {width, height, totalSize} from 'react-native-dimension'


const style= StyleSheet.create({
    body:{
        color:"#fff",
        height:height(100)+height(20),
        marginTop:height(2)
    },
    card:{
        elevation:6,
        backgroundColor:"white",
        width:width(90),
        height:height(20),
        marginVertical:height(2),
        borderRadius:width(4),
        flexDirection:"row",
        // flex:1,
         alignSelf:"center",
         overflow:"hidden"
        //  justifyContent:"center"
    },
    thumbnailContainer:{
        flex:1,
        // backgroundColor:"yellow",
         justifyContent:"flex-end",
         alignItems:"center"
    },
    thumbnailInnerContainer :{
        height:height(18),
        // backgroundColor:"gray",
        justifyContent:"flex-start",
        // alignItem:"flex-end"
    },
    description:{
      flex:3,
    //    backgroundColor:"pink"
    },
    lineContainer:{
        // flex:1,
        // backgroundColor:"red",
        width:width(1.5),
        height:height(10),
        alignSelf:"center",
        borderTopLeftRadius:width(4),
        borderBottomLeftRadius:width(4)
    },
    r1:{
        // backgroundColor:"orange",
        flex:1,
        flexDirection:"row",
        alignItems:"center"
    },
    r2:{
        // backgroundColor:"yellow",
        flex:1,
        flexDirection:"row"
    },
    flexRow:{
        flexDirection:"row"
    },
    c2:{
        // alignSelf:"center",
        // justifyContent:"flex-end",
    //   backgroundColor:"blue",
        flex:1
    },
    c1:{
        flex:1,
        // backgroundColor:"skyblue"
    },
    center1:{
        // backgroundColor:"red",
        justifyContent:"center",
        alignItems:"center",
        alignSelf:"center",
        borderRadius:width(2),
        height:height(3),
      width:width(28),
      marginTop:-width(4)
    },
    name:{
       fontFamily:"Monserrat-SemiBold",
       color:"#000",

       fontSize:totalSize(2.4) 
    },
    plan:{
        color:"#161F3D",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.6)
    },
    planText:{
        color:"#ACADAE",
        fontFamily:"Montserrat-Medium",
    },
    pt2:{
        paddingTop:width(2)
    },
    dateText:{
        color:"#000",
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.6)
    },
    date:{
        color:"#939495",
        fontFamily:"Montserrat-Medium"
    },
    dateIcon:{
        fontSize:totalSize(2.1),
        paddingTop:height(0.5)
    },
    startDate:{
        // backgroundColor:"pink",
        paddingRight:width(3),
        justifyContent:"flex-start",
        alignItems:"flex-start"
    },
    ButtonContainer:{
        justifyContent:"space-around",
        flex:1
        // alignSelf:"center"
    },
    Button:{
        backgroundColor:"#f2f2f2",
        // borderWidth:width(0.1),
        // borderColor:"#979797",
        alignItems:"center",
        justifyContent:"center",
        width:width(26),
        height:height(4.5),
        borderRadius:width(1)
    },
    ButtonText:{
        color:"#333333",
        fontFamily:"Montserrat-Medium",
        paddingRight:width(1),
        fontSize:totalSize(1.8)

    },
    status:{
        fontFamily:"Montserrat-Medium",
        fontSize:totalSize(1.4),
        color:"#fff",
        textTransform:"uppercase"
    }



})

export default style