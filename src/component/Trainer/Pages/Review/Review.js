import React, { Component } from "react";
import { ScrollView, View, Text, TouchableOpacity } from "react-native";
import {
  Icon,
  Button,
  Content,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Thumbnail,
} from "native-base";
import Image from "react-native-scalable-image";
import style from "./style";

const trainerData = [
  {
    key: 0,
    name: "Madge Santos",
    rating: "3",
    imageUri: require("../../../../assets/images/reviews/1.png"),
    description:
      "With all of the styling tool options available in today’s market, it can be very confusing to choose the best curling iron for your hair. I mean, let’s face it; most curling irons claim to be the best buy, but are they really? So, what should you really look for in a curling iron and what should you avoid? \n\nWhen choosing the best curling iron for your hair, you should discount everything that companies say about their product in order to market it. I mean, they have a vested interest in selling more curling irons, so who would believe what they had to say anyway? But you should pay attention to what is written on the packaging in the small letters.",
  },
  {
    key: 1,
    name: "Etta Parsons",
    rating: "5",
    imageUri: require("../../../../assets/images/reviews/2.png"),
    description:
      "Without a doubt there is something very relaxing and pleasurable about cooking.",
  },
  {
    key: 2,
    name: "Madgi Santos",
    rating: "5",
    imageUri: require("../../../../assets/images/reviews/1.png"),
    description:
      "healthy first means that you have decided to eat healthy. Cooking on a grill can be a great way to reduce fats on while adding ",
  },
  {
    key: 3,
    name: "Etta Parson",
    rating: "2",
    imageUri: require("../../../../assets/images/reviews/2.png"),
    description:
      "chemical changes in the outer layers of flesh foods. To avoid these dangerous chemical formations we must avoid inhaling.",
  },
  {
    key: 4,
    name: "Madge Santos",
    rating: "3",
    imageUri: require("../../../../assets/images/reviews/1.png"),
    description:
      " the smoke and avoid the black char on the outside of charcoal cooked food caused by high heat and/or overcooking. ",
  },
  {
    key: 5,
    name: "Etta Parsons",
    rating: "5",
    imageUri: require("../../../../assets/images/reviews/2.png"),
    description:
      "Without a doubt there is something very relaxing and pleasurable about cooking.",
  },
];
class TrainerReview extends Component {
  state = {
    showLine: 2,
    clickedId: "",
  };

  toggleDescriptionLength = (key) => {
    this.setState(
      {
        showLine: this.state.showLine == 2 ? null : 2,
      },
      () => {
        console.log(this.state.showLine);
      }
    );
  };

  static navigationOptions = {
    headerShown: false,
  };

  render() {
    let counter = 0;
    const renderList = trainerData.map((data, index) => {
      return (
        <TouchableOpacity
          key={data.key}
          onPress={() => this.toggleDescriptionLength(data.key)}
          style={[style.flexRow, style.listItem]}
        >
          <View style={style.imgOuterContainer}>
            <View style={style.imageContainer}>
              <Image source={data.imageUri} resizeMode="cover" />
            </View>
          </View>
          <View style={style.textOuterContainer}>
            <Text style={style.nameText}>{data.name}</Text>
            <View style={style.flexRow}>
              <View style={style.flexRow}>
                <Icon style={style.starIcon} name="star" type="AntDesign" />
                <Icon style={style.starIcon} name="star" type="AntDesign" />
                <Icon style={style.starIcon} name="star" type="AntDesign" />
                <Icon style={style.starIcon} name="star" type="AntDesign" />
                <Icon style={style.starIcon} name="star" type="AntDesign" />
              </View>
            </View>
            <View style={style.descriptionContainer}>
              <Text
                key={data.key}
                style={style.description}
                numberOfLines={this.state.showLine}
              >
                {data.description}
              </Text>
              {/* <Description showLines={this.state.showLine} description={data.description}/> */}
            </View>
          </View>
        </TouchableOpacity>
      );
    });

    return (
      <ScrollView>
        <View style={style.container}>
          <View style={style.flexRow}>
          <TouchableOpacity 
            style={style.iconContainer}
            onPress={()=>{this.props.navigation.replace("Customer")}}
            >
              <View style={style.icon}>
                <Image 
                  // width={width(30)}
                source={require('../../../../assets/images/trainer/arrow.png')}/>
              </View>
            </TouchableOpacity>
            <View style={style.flex5}>
              <View style={style.headerContainer}>
                <Text style={style.headerText}>Reviews</Text>
              </View>
            </View>
          </View>
          <View style={style.listContainer}>{renderList}</View>
        </View>
      </ScrollView>
    );
  }
}

export default TrainerReview;
