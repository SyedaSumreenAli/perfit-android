import { StyleSheet } from "react-native";
import { width, height } from "react-native-dimension";

const style = StyleSheet.create({
 trainerbg:{
    //    height:height(10),
    flex:1,
    // flex:5,flexDirection:"column"
 },

 trainerbgImage:{
    //  height:100
    // width:1500
 },
 container:{
    position:"absolute",
    //  backgroundColor:"red",
    //  width:80,
    //  height:60,
     flex:1,
     width:width(100),
     justifyContent:"center",
     alignItems:"center",
     zIndex:2
 },
 logoContainer:{
     paddingTop:height(4),
     justifyContent:"center",
     alignItems:"center"
 },
 logo:{
    width:130,
    height:200
  },
 
 h2:{
     textTransform:"uppercase",
     fontFamily:"Montserrat-SemiBold",
     fontSize:17.68,
     textAlign:"center",
     color:"#ffffff"
 },
 h1:{
    // textTransform:"uppercase",
    fontFamily:"Montserrat-SemiBold",
    fontSize:27.08,
    textAlign:"center",
    color:"#ffffff"
},
 loginContainer:{
     backgroundColor:'rgba(0,0,0,0.3)',
     width:width(90),
     marginTop:height(4),
     borderRadius:25,
     paddingTop:height(4),
     paddingHorizontal:width(6),
     paddingBottom:height(8)
    
 },
 formContainer:{
    marginTop:height(6)
 },
 icon:{ 
     color:"#FDBE51",
     fontSize:20,
     marginRight:width(2)
    },
formText:{
    fontFamily:"Montserrat-Medium",
    fontSize:16.9,
    color:"#C1C1C1"
},
linearGradient: {
    flex: 1,
    paddingLeft: 45,
    paddingRight: 45,
    opacity: 0.9,
    borderRadius: 25,
  },
  loginButtonContainer:{
    marginTop:height(8)
  },
  buttonText: {
    fontSize: 19.23,
    fontFamily: "Montserrat-Light",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
  },
  contactUsButton:{
      backgroundColor:"#000000",
      borderRadius:50,
      marginTop:height(15),
      paddingHorizontal:width(10),
      height:height(4)
  },
  contactUsButtonText:{
      color:"#ffffff",
      fontFamily:"Montserrat-Bold",
      textTransform:"uppercase"
  }
});

export default style;
