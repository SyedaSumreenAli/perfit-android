import React, { Component } from "react";
import { ScrollView, View, Text} from "react-native";
import logo from "../../../../assets/images/logo.png";
import trainerbg from "../../../../assets/images/trainer/trainerbg.png";
import { Button, Content, Form, Item, Input } from "native-base";
import { Dimensions } from 'react-native';
import Image from 'react-native-scalable-image';
import LinearGradient from "react-native-linear-gradient";
import style from "./style";
import Icon from "react-native-vector-icons/FontAwesome";

class TrainerLogin extends Component {
  constructor(props){
    super(props)
    {
      this.state = {
        errMsg : 'Invalid email address, please enter again'
      }
    }
  }
  
  handleSubmit=()=>{
    alert('click')
    this.props.navigation.navigate("Customer")
  }
  render() {
    const {email, password,errMsg} = this.state
    return (
        <ScrollView>
        <View>
            <View style={style.trainerbg}>
            <Image
                width={Dimensions.get('window').width} 
             
                source={trainerbg}
            />
            {/* <Image source={trainerbg}/> */}
            </View>
            <View style={style.container}>
              <View style={style.logoContainer} >
                  <Image source={logo} width={170}/>
              </View>
              <View style={style.loginContainer}>
                    <View>
                        <Text style={style.h2}>login Area</Text>
                        <Text style={style.h1}>Personal Trainer</Text>
                    </View>

                    <View style={style.formContainer}>
                        <Content>
                            <Form>
                                <Item>
                                    <Icon  type="FontAwesome" name="user-o" style={style.icon}/>
                                    <Input style={style.formText} placeholder="Email"/>
                                </Item>
                                <Item>
                                    <Icon  type="FontAwesome" name="key" style={style.icon}/>
                                    <Input style={style.formText} placeholder="Password"/>
                                </Item>
                            </Form>
                        </Content>
                        <View style={style.loginButtonContainer}>
                        <Button transparent onPress={this.handleSubmit}>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 4 }}
                            colors={["#efcc64", "#FFAA2F"]}
                            style={style.linearGradient}
                        >
                            <Text style={style.buttonText}>Login</Text>
                        </LinearGradient>
                        </Button>
                        </View>
                        
                    </View>

                    <View>
                    </View>
                    
              </View>
              <Button style={style.contactUsButton}>
                            <Text style={style.contactUsButtonText}>Contact Us</Text>
                        </Button>
            </View>

        </View>
        </ScrollView>
        
    );
  }
}

export default TrainerLogin;
