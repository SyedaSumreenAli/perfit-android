import React, { Component } from "react";
import { StyleSheet, View, StatusBar } from "react-native";
import Routes from "../MainRoutes/Routes";

import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import AllReducers from '../redux/reducers'

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
const store = createStoreWithMiddleware(AllReducers)


const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 20 : StatusBar.currentHeight;

export default class MainApp extends Component {
 
  render() {
    

    return (
      <Provider store={store}>
          <Routes/>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});
