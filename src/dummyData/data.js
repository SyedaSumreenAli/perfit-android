export const trainerData = [
    {
      name: "jorge olson",
      category: "weight lose",
      review: "20",
      rating: "3",
      imageUri: require("../../../../assets/images/data/1.png"),
    },
    {
      name: "Bella Mathis",
      category: "gain muscles",
      review: "20",
      rating: "5",
      imageUri: require("../../../../assets/images/data/2.png"),
    },
    {
      name: "Bertha Bass",
      category: "weight lose",
      review: "20",
      rating: "5",
      imageUri: require("../../../../assets/images/data/3.png"),
    },
    {
      name: "jorge Clarke",
      category: "weight lose",
      review: "20",
      rating: "2",
      imageUri: require("../../../../assets/images/data/4.png"),
    },
    {
      name: "Bill Mathis",
      category: "weight lose",
      review: "20",
      rating: "3",
      imageUri: require("../../../../assets/images/data/5.png"),
    },
    {
      name: "Gavin Vaughn",
      category: "weight lose",
      review: "20",
      rating: "5",
      imageUri: require("../../../../assets/images/data/6.png"),
    },
    {
      name: "Inez Howel",
      category: "weight lose",
      review: "20",
      rating: "5",
      imageUri: require("../../../../assets/images/data/7.png"),
    },
  ];
  