import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import TabNavigator from "./Tab";
import Splash from "../component/Splash/Splash";
import Login from "../component/Auth/Login/Login";
import Signup from "../component/Auth/Signup/Signup";
import ForgetPassword from "../component/Auth/ForgetPassword/ForgetPassword";
import ContactUs from "../component/Pages/ContactUs/ContactUs";


import YourDetails from "../component/Pages/YourDetails/YourDetails";
import SetYourGoals from "../component/Pages/SetYouGoals/SetYourGoals";


import TrainerLogin from "../component/Trainer/Auth/Login/Login";
import TrainerDashboard from "../component/Trainer/Pages/DashBoard/Dashboard";
import Customer from '../component/Trainer/Pages/Customer/Customer';
import TrainerReviews from "../component/Trainer/Pages/Review/Review";
import TrainerContact from "../component/Trainer/Pages/Contact/Contact";
import TrainerInbox from "../component/Trainer/Pages/Inbox/Inbox";
import CustomerDetails from "../component/Trainer/Pages/Customer/CustomerDetails/CustomerDetails";
import NewCustomer from "../component/Trainer/Pages/Customer/newCustomer/newCustomer";
import ViewProgram from "../component/Trainer/Pages/Customer/ViewProgram/ViewProgram";
import Recipe from "../component/Trainer/Pages/Customer/ViewProgram/Recipe/Recipe";
import ExerciseVideo from "../component/Trainer/Pages/Customer/ViewProgram/Exercise/ExerciseVideo/ExerciseVideo"


const AppNavigator = createStackNavigator(
  {
    Splash,
    Login,
    Signup,
    ForgetPassword,
    ContactUs,
    YourDetails,
    SetYourGoals,
    Tab: TabNavigator,

    /******  TRAINER ROUTES  *******/
    TrainerLogin,
    TrainerDashboard,
    Customer,
    TrainerContact,
    TrainerInbox,
    TrainerReviews,
    CustomerDetails,
    NewCustomer,
    ViewProgram,
    TrainerExerciseVideo: ExerciseVideo,
    TrainerRecipe: Recipe
  },
  {
    headerShown: false,
    headerMode: "none",
    // initialRouteName: "Tab",
   initialRouteName: "Splash",
  }
);

export default createAppContainer(AppNavigator);
