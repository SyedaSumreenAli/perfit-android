import React, { Component } from "react";

import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { height, width } from "react-native-dimension";
import Image from "react-native-scalable-image";

import SelectionMain from "../component/Pages/SelectionPage/Selection";
import TrainerList from "../component/Pages/List/TrainerList/TrainerList";


import PTPage from "../component/Pages/PTPage/PTPage";
import DTPage from "../component/Pages/PTPage/DTPage";


import PackageDetails from "../component/Pages/PackageDetails/PackageDetails";
import Supscription from "../component/Pages/Subscription/Subscription"

import Dashboard from "../component/Pages/Dashboard/Dashboard";
import StrengthTraining from "../component/Pages/StrengthTraining/StrengthTraining";
import ExerciseVideo from "../component/Pages/StrengthTraining/Exercise/ExerciseVideo/ExerciseVideo";
import UpdateMeasurement from "../component/Pages/StrengthTraining/Measurement/UpdateMeasurement/UpdateMeasurement";
import Recipe from "../component/Pages/StrengthTraining/Recipe/Recipe";
import AddReview from "../component/Pages/AddReview/AddReview";

import Review from "../component/Pages/Review/Review";
import Inbox from "../component/Inbox/Inbox";
import Chat from "../component/Pages/Chat/Chat";

import CustomerServiceChat from "../component/Pages/CustomerService/CustomerServiceChat"

import messagesIcon from "../assets/images/icons/messages.png";
import subscriptionIcon from "../assets/images/icons/subscription.png";
import dashboardIcon from "../assets/images/icons/dashboard.png";
import phoneIcon from "../assets/images/icons/phone.png";
import { Icon } from "native-base";

import DietitiansList from '../component/Pages/List/DietitiansList/DietitiansList'

const Pages = createStackNavigator(
  {
    SelectionMain,

    DietitiansList,
    DTPage,


    TrainerList,
  
    

    
    PTPage,
    
    PackageDetails,
    Review,


  },
  {
    initialRouteName: "SelectionMain",
  }
);
const MessageStack = createStackNavigator(
  {
    Chat,
    Inbox,
  },
  {
    initialRouteName: "Inbox",

    navigationOptions: {
      headerVisible: false,
    }

  }
);
const DashboardStack = createStackNavigator(
  {
    Dashboard,
    StrengthTraining,
    UpdateMeasurement,
    ExerciseVideo,
    Recipe,
    AddReview
  },
  {
    initialRouteName: "Dashboard",
  }
);

const TabNavigator = createBottomTabNavigator(
  {
    Messages: {
      screen: MessageStack,
      navigationOptions: {
        tabBarLabel: "Messages",
        headerShown: false,
        tabBarIcon: ({ tintColor }) => (
          <Image source={messagesIcon} width={30} />
        ),
      },
    },
    Home: {
      screen: Pages,
      navigationOptions: {
        tabBarLabel: "Home",
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="home"
            type="Entypo"
            style={{
              color: "#949494"
            }}
          />
        ),
      },
    },

    Dashboard: {
      screen: DashboardStack,
      navigationOptions: {
        tabBarLabel: "Dashboard",
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <Image source={dashboardIcon} width={30} />
        ),
      },
    },
    ContactUs: {
      screen: CustomerServiceChat,
      navigationOptions: {
        header: null,
        tabBarLabel: "Contact Us",
        tabBarIcon: ({ tintColor }) => <Image source={phoneIcon} width={30} />,
      },
    },
  },

  {
    initialRouteName: "Home",
    tabBarOptions: {
      inactiveTintColor: "gray",
      activeTintColor: "white",
      labelStyle: {
        fontSize: 12,
        fontWeight: "bold",
      },
      style: {
        backgroundColor: "#000000",
        //backgroundColor:'yellow',
        borderTopWidth: 0,
        //   borderTopColor:'yellow',
        height: height(10),
        // paddingVertical:5,
      },
    },
  }
);

export default createAppContainer(TabNavigator);