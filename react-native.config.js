
module.exports = {
    project: {
      ios: {},
      android: {}, // grouped into "project"
    },
    assets: [
      "./src/assets/fonts/",
      "./src/theme"
  ], // stays the same
  };